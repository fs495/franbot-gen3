# -*- coding: utf-8 -*-

# 参考
# - iPhone向けpixiv API by moyashi
#   http://pastie.org/735195
# - pxv: A pixiv API wrapper for Java
#   http://sourceforge.jp/projects/pxv/

module Pixiv
    class API
	BASE = "http://spapi.pixiv.net/iphone/"
	SESSVAR = "PHPSESSID"

	def initialize()
	    @client = HTTPClient.new()
	    @sess_id = nil
	end

	#----------------------------------------------------------------------

	def login(id, password)
	    url = "#{BASE}login.php?mode=login&pixiv_id=#{id}&pass=#{password}&skip=0"
	    res = @client.get(url)
	    if res.status == 302 &&
		    res.header['Location'][0] =~ /\?#{SESSVAR}=(.*)$/
		@sess_id = $1
		@sess_par = "#{SESSVAR}=#{@sess_id}"
	    end
	    return login?()
	end

	def login?
	    return @sess_id != nil
	end

	# システムの稼働状態
	def status()
	    url = "#{BASE}maintenance.php?software-version=1.0"
	    res = @client.get(url)
	    return res.status == 200
	end

	# ログインユーザのプロフィール
	def profile()
	    url = "#{BASE}profile.php?dummy=0&#{@sess_par}"
	    return @client.get_content(url)
	end

	#----------------------------------------------------------------------
	# 新着

	# みんなの新着
	def new_illust_count()
	    return __count("new_illust")
	end
	def new_illusts(page)
	    return __illusts("new_illust", page)
	end

	# マイピクの新着
	def mypixiv_new_illust_count()
	    return __count("mypixiv_new_illust")
	end
	def mypixiv_new_illusts(page)
	    return __illusts("mypixiv_new_illust", page)
	end

	# お気に入りユーザの新着イラスト
	def bookmark_user_new_illust_count()
	    return __count("bookmark_user_new_illust")
	end
	def bookmark_user_new_illusts(page)
	    return __illusts("bookmark_user_new_illust", page)
	end

	# デイリーランキング
	def daily_ranking_count()
	    return __count("ranking", "mode=day")
	end
	def daily_rankings(page)
	    return __illusts("ranking", page, "mode=day")
	end

	# ウィークリーランキング
	def weekly_ranking_count()
	    return __count("ranking", "mode=week")
	end
	def weekly_rankings(page)
	    return __illusts("ranking", page, "mode=week")
	end

	# マンスリーランキング
	def monthly_ranking_count()
	    return __count("ranking", "mode=month")
	end
	def monthly_rankings(page)
	    return __illusts("ranking", page, "mode=month")
	end

	#----------------------------------------------------------------------
	# イラスト検索・ユーザ検索

	# modeはs_tagまたはs_tc
	def search_illust_count(mode, word)
	    return __count("search", "s_mode=#{mode}&word=#{escape(word)}")
	end
	def search_illusts(mode, word, page)
	    return __illusts("search", page, "s_mode=#{mode}&word=#{escape(word)}")
	end

	def search_user_count(name)
	    return __count("search_user", "nick=#{escape(name)}")
	end
	def search_users(name, page)
	    return __users("search_user", page, "nick=#{escape(name)}")
	end

	#----------------------------------------------------------------------
	# マイページ

	# イラスト
	def member_illust_count(user_id)
	    return __count("member_illust", "id=#{user_id}")
	end
	def member_illusts(user_id, page)
	    return __illusts("member_illust", page, "id=#{user_id}")
	end

	# マイピク
	def mypixiv_all_count(user_id)
	    return __count("mypixiv_all", "id=#{user_id}")
	end
	def mypixiv_all_users(user_id)
	    return __users("mypixiv_all", page, "id=#{user_id}")
	end

	# 公開お気に入り
	def public_bookmark_user_count(user_id)
	    return __count("bookmark_user_all", "rest=show&id=#{user_id}")
	end
	def public_bookmark_users(user_id)
	    return __users("bookmark_user_all", page, "rest=show&id=#{user_id}")
	end

	# 非公開お気に入り
	def private_bookmark_user_count(user_id)
	    return __count("bookmark_user_all", "rest=hide&id=#{user_id}")
	end
	def private_bookmark_users(user_id)
	    return __users("bookmark_user_all", page, "rest=hide&id=#{user_id}")
	end

	# 公開ブックマーク
	def public_bookmark_count(user_id)
	    return __count("bookmark", "id=#{user_id}")
	end
	def public_bookmarks(user_id, page)
	    return __illusts("bookmark", page, "id=#{user_id}")
	end

	# 非公開ブックマーク
	def private_bookmark_count(user_id)
	    return __count("bookmark", "id=#{user_id}&rest=hide")
	end
	def private_bookmarks(user_id, page)
	    return __illusts("bookmark", page, "id=#{user_id}&rest=hide")
	end

	#----------------------------------------------------------------------

	DUMMY_PARAM = 'dummy=0'

	def __count(type, param = DUMMY_PARAM)
	    url = "#{BASE}#{type}.php?#{param}&#{@sess_par}&c_mode=count"
	    return @client.get_content(url).to_i
	end

	def __illusts(type, page, param = DUMMY_PARAM)
	    url = "#{BASE}#{type}.php?#{param}&#{@sess_par}&p=#{page}"
	    body = @client.get_content(url)
	    data = Pixiv::CSVParser.parse(body)
	    return data.map{|x| Image.new(self, x)}
	end

	def __users(type, page, param)
	    url = "#{BASE}#{type}.php?#{param}&#{@sess_par}&p=#{page}"
	    body = @client.get_content(url)
	    data = Pixiv::CSVParser.parse(body)
	    return data.map{|x| User.new(self, x)}
	end

	def escape(str) # stoled from CGI.escape
	    return str.gsub(/([^ a-zA-Z0-9_.-]+)/n) {
		'%' + $1.unpack('H2' * $1.bytesize).join('%').upcase
	    }.tr(' ', '+')
	end
    end

    #----------------------------------------------------------------------

    class Image
	attr_reader :api

	attr_reader :illust_id
	attr_reader :author_id
	attr_reader :suffix
	attr_reader :title
	attr_reader :server
	attr_reader :author_name
	attr_reader :mobile_thumbnail_url
	attr_reader :mobile_url
	attr_reader :date
	attr_reader :tags
	attr_reader :tool
	attr_reader :eval_count
	attr_reader :eval_point
	attr_reader :views
	attr_reader :comment
	attr_reader :comment_count
	attr_reader :bookmark_users
	attr_reader :account_name

	def initialize(api, data)
	    raise ArgumentError.new if data.size <= 18
	    @api = api
	    @illust_id = data[0][0].to_i
	    @author_id = data[1][0].to_i
	    @suffix = data[2][0]
	    @title = data[3][0]
	    @server = sprintf("%02d", data[4][0].to_i)
	    @author_name = data[5][0]
	    @mobile_thumbnail_url = data[6][0]
	    @mobile_url = data[9][0]
	    @date = data[12][0]
	    @tags = data[13]
	    @tool = data[14]
	    @eval_count = data[15][0].to_i
	    @eval_point = data[16][0].to_i
	    @views = data[17][0].to_i
	    @comment = data[18][0]
	    @bookmark_users = data[22][0].to_i
	    @comment_count = data[23][0].to_i
	    @account_name = data[24][0]
	end

	def page_url
	    return "http://www.pixiv.net/member_illust.php?mode=medium&illust_id=#{@illust_id}"
	end

	def page_url_short
	    return "http://p.tl/i/#{@illust_id}"
	end

	def illust_url(type = :original)
	    case type
	    when :original
		return "http://img#{@server}.pixiv.net/img/#{@account_name}/#{@illust_id}.#{@suffix}"
	    when :thumbnail
		return "http://img#{@server}.pixiv.net/img/#{@account_name}/#{@illust_id}_s.#{@suffix}"
	    when :mobile
		return @mobile_url
	    when :mobile_thumbnail
		return @mobile_thumbnail_url
	    end
	end
    end

    #----------------------------------------------------------------------

    class User
	attr_reader :api

	attr_reader :user_id
	attr_reader :name
	attr_reader :mobile_thumbnail_url
	attr_reader :account_name

	def initialize(api, data)
	    raise ArgumentError.new if data.size < 1
	    @api = api
	    @user_id = data[1][0].to_i
	    @name = data[5][0]
	    @mobile_thumbnail_url = data[6][0]
	    @account_name = data[24][0]
	end
    end

    #----------------------------------------------------------------------

    class CSVFormatError < StandardError
    end

    # Pixivが返すCSV(風データ)のパーザ
    # - 行セパレータはLFのみ。データ中の改行はCR+LF
    # - カンマ区切り。要素がリストの場合はスペース区切り
    class CSVParser
	attr_reader :records

	def initialize()
	    @records = []
	end

	def parse_lines(s)
	    @records.clear
	    fields, data, atom = [], [], ''
	    state = :pre_quote
	    s = s.chomp() + "\n"

	    s.each_byte do |ch|
		err_desc = "#{s}, #{state}, in #{ch.chr.inspect}"
		case state
		when :pre_quote
		    case ch
		    when ?"
			state = :in_quote
		    when ?\s
			# スキップ
		    when ?,
			fields << data.dup
			data.clear
		    when ?\n
			fields << data.dup
			data.clear
			@records << fields.dup
			fields.clear
		    else
			raise CSVFormatError.new(err_desc)
		    end

		when :in_quote
		    case ch
		    when ?"
			state = :post_quote
		    #when ?\\
			#state = :escaped
		    when ?\r
			state = :cr
		    else
			atom << ch
		    end

		when :post_quote
		    case ch
		    when ?"
			atom << ?"
			state = :in_quote
		    when ?\s
			data << atom
			atom = ''
			state = :pre_quote
		    when ?,
			data << atom
			atom = ''
			fields << data.dup
			data.clear
			state = :pre_quote
		    when ?\n
			data << atom
			atom = ''
			fields << data.dup
			data.clear
			@records << fields.dup
			fields.clear
			state = :pre_quote
		    else
			raise CSVFormatError.new(err_desc)
		    end

		when :escaped
		    atom << ch
		    state = :in_quote

		when :cr
		    case ch
		    when ?\n
			atom << ch
			state = :in_quote
		    else
			raise CSVFormatError.new(err_desc)
		    end
		end
	    end
	    return @records
	end

	def self.parse(s)
	    parser = CSVParser.new()
	    return parser.parse_lines(s)
	end
    end
end


if $0 == __FILE__
    require 'rubygems'
    require 'httpclient'
    require 'pp'
    require 'private/pixiv-secret'

    case ARGV[0]
    when '-api-test'
	api = Pixiv::API.new()
	pp api.login(PIXIV_USER, PIXIV_PASSWD)
	pp api.status()
	pp api.profile()
	pp api.bookmark_user_new_illusts(1)
	pp api.daily_rankings(1)
	pp api.search_illusts('s_tag', 'まどマギ', 1)
	pp api.search_users('ひつじ', 1)
    when '-parse-test'
	s = '"a1","b1","c1","d1"' + "\n" +
	    '"a2-1" "a2-2",,"""c""2""","d2' + "\r\n" + 'd2"'
	pp Pixiv::CSVParser.parse(s)
    when '-dump'
	while s = STDIN.gets()
	    pp Pixiv::CSVParser.parse(s)
	end
    end
end
