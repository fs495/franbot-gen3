# -*- coding: utf-8 -*-

class TohoResgen
    #----------------------------------------------------------------------
    # 遊び (若干好感度上がりやすい)
    def asobi(c)
	return false if c.to_none || c.only_to_others

	case c.text
	when /弾幕.*(ごっこ|遊ぶ|遊び|あそび)/
	    spell = rnd(FLAN_SPELLS)
	    res = rnd(["じゃあいくよ！",
		       "壊れちゃダメだよ？",
		       "楽しませてくれるよね？"])
	    reply("#{c.to} #{res}#{spell}", c)
	    c.uconf[:base_affection] += dice(1, 20)
	    return true

	when /気合い避け/
	    res = rnd(["えいっ、当たっちゃえ！",
		       "えーん、なんで当たらないのー"])
	    reply("#{c.to} #{res}", c)
	    return true

	when /夜(遊|あそ)び|深夜徘徊/
	    reply("#{c.to} 夜遊びしたいけど、お姉様が外に出してくれないからなあ", c)
	    return true

	when /(鬼|おに|オニ)ごっこ/
	    reply("#{c.to} フラン、一応鬼なんだけど…うぅ", c)
	    return true

	when /(隠|かく)れんぼ/
	    reply("#{c.to} わかった！じゃあ、隠れるよ？", c)
	    reply("#{c.to} 秘弾「そして誰もいなくなるか？」…これはちょっとズルかったかな♪えへへっ", c)
	    return true

	when /(医者|いしゃ)(さん)?ごっこ/
	    res = "お医者さんごっこ？"
	    res += rnd(["#{c.cn}を解剖して遊ぶの？",
			"「はじいしゃ」のことだっけ？"])
	    reply("#{c.to} #{res}", c)
	    return true

	when /おままごと/
	    reply("#{c.to} あの、その、フラン、お姉様しか家族がいないから、どうやって遊ぶのか分からないの… ご、ごめんなさい…", c)
	    return true

	when /お?人形(遊び|あそび|で遊|であそ)/
	    res = rnd(["お人形を壊して遊ぶの？",
		       "空気人形っていうので遊ぶの？",
		       "ラブドールっていうので遊ぶの？"])
	    reply("#{c.to} #{res}", c)
	    return true

	when /ポッキーゲーム/
	    reply("#{c.to} えー、フランゲームのほうがいいよぉ", c)
	    return true

	when /フランゲーム/
	    tere = tere_aa()
	    res = rnd(["フランと格闘大会だね！ えっ、セルゲームとは違うの？",
		       "えへへ、なんか恥ずかしい#{tere}",
		       "えへへ、なんか恥ずかしいよぉ…#{tere}"])
	    reply("#{c.to} #{res}", c)
	    return true

	when /(何|なに)して(遊|あそ)(ぶ|ぼう)/
	    reply("#{c.to} うーん、フランはひとり遊び以外はあまり知らないんだ… ごめんね", c)
	    return true

	when /(あそ|遊)(んで|ぼう|ぼー|ぼ〜|ばない|びしない|びましょ)/
	    if c.tsundere < 0
		res = rnd(["じゃあ、#{c.cn}を壊して遊ぼっかな♪",
			   "そこに飛び込む遊び道具…"])
		reply("#{c.to} #{res}", c)
	    else
		reply("#{c.to} わーい、何して遊ぶ？", c)
	    end
	    c.uconf[:base_affection] += dice(1, 12)
	    return true
	end
	return false
    end

    #----------------------------------------------------------------------
    # せくはらとすきんしっぷ

    SH_REACTION_TABLE = {
	:kanjiru => ["ひゃぅ！ 変な声出ちゃった…",
		     "ふにゃあ…",
		     "ふにゃん",
		     "ふにゅう…",
		     "ふ、ふぁあ…",
		     "やぁっ…",
		     "やぁん…", 
		     "や、やぁっ…",
		     "やんっ…",
		     "ら、らめぇ…",],
	:shigeki => ["あっ…",
		     "ひゃう",
		     "ひゃうっ",
		     "ひゃんっ",
		     "んんっ…"],
	:iyagaru => ["そ、そんなとこだめぇ…",
		     "だ、だめだよぉ…",
		     "や、やめてよう…",
		     "ふぁ…や、やめてよう…",
		     "い、いやぁ…"],
	:kusuguri => ["くすぐったいよぉ…"],
	:teretere => ["えへへー",
		      "えへへっ",
		      "う、うにゅう…",
		      "は、はぅぅ…"],
	:nadenade => ["えへへ、気持ちいい…",
		      "う、うぅぅぅぅ…",
		      "は、はぅぅ…",
		      "もっとしてぇ…",
		      "も、もっとぉ…"],
	:batou => ["壊れちゃえこの変態！",
		   "こ、壊されたいみたいだね！",
		   "変態！変態！変態！",
		   "変態！爆発しちゃえ！"]
    }

    NONOSIRI = ["変態", "エッチ", "ばか", "童貞", "最低", "キモヲタ"]

    # 行為/擬音と行為対象で条件分けする。
    # より高度なものを先に判定する。
    def sekuhara(c)
	return false if c.to_none || c.only_to_others
	tere = tere_aa()

	#............................................................
	# 固定反応
	case c.text
	when /(えっ?ち|エッチ|H|セック(ス|ル)|ｾｯｸ(ｽ|ﾙ)|[Ss][Ee][Xx]|(にゃん|ニャン){2}|生殖.*|子作り.*)(しよ|しましょ|したい)/
	    if c.affection > 90 && c.tsundere > 10
		reply("#{c.to} #{tere}", c)
	    else
		res = rnd(["それってなあに？",
			   "それってどういうことするの？"])
		reply(c.select_flan_resp(res), c)
	    end
	    return true

	when /(一緒|いっしょ)に(寝|ねよう|眠|ねむ|スリーピング)/
	    if c.affection > 90 && c.tsundere > 10
		res = rnd(["やった〜♪",
			   "ご本読んで欲しいなっ",
			   "それじゃご本読んで！"])
	    else
		res = rnd(["ひ、一人で眠れるから大丈夫よ",
			   "…何か変なこと期待してない？",
			   "何百年一人で寝ていたと思うの？"])
	    end
	    reply("#{c.to} #{res}", c)
	    return true

	when /(もみ|揉み){2}|(ぬっちょ|ぬっぽ|じゅっぽ|じゅぽ){2}/
	    if c.affection > 70 && c.tsundere > 0
		res = gen_res(:kanjiru, :tere)
		c.uconf[:base_affection] += dice(1, 6)
		c.uconf[:base_tsundere] += dice(1, 4)
		reply("#{c.to} #{res}", c)
	    else
		res = batou(c, "ど、どこ触ってるの変態！")
		c.uconf[:base_affection] -= dice(1, 6)
		c.uconf[:base_tsundere] -= dice(1, 4)
		reply(c.select_flan_resp(res), c)
	    end
	    return true

	when /(ぺろぺろ|ペロペロ|ﾍﾟﾛﾍﾟﾛ|れろれろ|レロレロ|ﾚﾛﾚﾛ)/
	    if c.tsundere < 0 && c.text =~ /(靴|ニーソ|ﾆｰｿ|ソックス|ｿｯｸｽ|足|脚)/
		res = rnd(["あら、下僕としてはいい心がけだね",
			   "うわ、ホントになめてるよ… ｷﾓｯ"])
		c.uconf[:base_affection] += dice(1, 6)
		c.uconf[:base_tsundere] -= dice(1, 4)
		reply(c.select_flan_resp(res), c)
	    elsif c.affection > 70 && c.tsundere > 0
		res = gen_res("そ、そんなところキタナイよぉ…", :kanjiru, :tere)
		c.uconf[:base_affection] += dice(1, 6)
		c.uconf[:base_tsundere] += dice(1, 4)
		reply(c.select_flan_resp(res), c)
	    else
		res = batou(c, "な、なに気持ち悪いこと言ってるの！？")
		c.uconf[:base_affection] -= dice(1, 6)
		c.uconf[:base_tsundere] -= dice(1, 4)
		reply(c.select_flan_resp(res), c)
	    end
	    return true

	when /#{FLAN_RE}.*(おいしい|はむはむ|もぐもぐ|モグモグ|ﾓｸﾞﾓｸﾞ|mogmog)/o
	    # 誤爆防止のため、「フランちゃんのｘｘ」にしか反応しない
	    if c.affection > 70 && c.tsundere > 0
		res = rnd(["そ、そんなのキタナイよぉ…",
			   "お腹壊しちゃうよぉ",
			   "そんなものまで食べてくれるなんて嬉しい…"])
		res += tere
		reply("#{c.to} #{res}", c)
	    else
		res = batou(c, "な、なに気持ち悪いこと言ってるの！？",
			    "そんなもの食べるなんて…")
		c.uconf[:base_affection] -= dice(1, 6)
		c.uconf[:base_tsundere] -= dice(1, 4)
		reply(c.select_flan_resp(res), c)
	    end
	    return true

	when /#{FLAN_RE}.*(ごくごく|ゴクゴク|ｺﾞｸｺﾞｸ|gokgok)/o
	    res = batou(c, "な、なに気持ち悪いこと言ってるの！？",
			"そんなもの飲むなんて…")
	    reply(c.select_flan_resp(res), c)
	    return true

	when /(さわ|触)(って(いい|良い).*(\?|？)|らせて)/
	    if c.affection > 70 && c.tsundere > 0
		res = rnd(["う、うん…",
			   "…ちょっとだけだよ？"])
		c.uconf[:base_affection] += dice(1, 6)
		c.uconf[:base_tsundere] += dice(1, 4)
		reply("#{c.to} #{res}", c)
	    else
		res = batou(c, "ダメに決まってるでしょ！")
		c.uconf[:base_affection] -= dice(1, 6)
		c.uconf[:base_tsundere] -= dice(1, 4)
		reply(c.select_flan_resp(res), c)
	    end
	    return true

	when /(おしり|お尻|おっぱい|ちっぱい|胸|貧乳|ひんぬー).*/
	    res = batou(c, "何考えてるの!?", "最低！")
	    c.uconf[:base_affection] -= dice(1, 6)
	    c.uconf[:base_tsundere] -= dice(1, 4)
	    reply(c.select_flan_resp(res), c)
	    return true

	when /(ちゅっちゅ|チュッチュ|ﾁｭｯﾁｭ)|(ちゅ(ー)*っ?|チュ(ー)*ッ?|ﾁｭｰ*ｯ?)(しよう|して)?#{EOL}$/o
	    if c.affection > 90
		if c.tsundere > 0
		    res = rnd(["…ちゅっ♥",
			       "…ちゅっ#{tere}",
			       "#{c.cn}、ちゅっちゅっ♥",
			       "#{c.cn}、ちゅっちゅっ#{tere}",
			       "は、はぅぅ…#{tere}",
			       "こんなところで恥ずかしいよぉ#{tere}",
			       "こんなところで恥ずかしい…#{tere}",
			       "はっ、恥ずかしいっ…#{tere}"])
		else
		    res = rnd(["バ、バカッ！…ちゅっ#{tere}",
			       "こんなところじゃ見られちゃうよ…#{tere}"])
		end
		c.uconf[:base_affection] += dice(1, 6)
		c.uconf[:base_tsundere] += dice(1, 4)
	    elsif c.affection > 70
		if c.tsundere > 0
		    res = rnd(["は、はぅぅ…#{tere}",
			       "こんなところで恥ずかしいよぉ#{tere}",
			       "こんなところで恥ずかしい…#{tere}",
			       "はっ、恥ずかしいっ…#{tere}",
			       "他の人間に見られちゃうよ…#{tere}"])
		else
		    res = rnd(["バ、バカッ！#{tere}",
			       "こんなところでできるわけないじゃない#{tere}"])
		end
		c.uconf[:base_affection] += dice(1, 4)
		c.uconf[:base_tsundere] += dice(1, 2)
	    else
		if c.tsundere > 0
		    res = "はいはい、ﾁｭｯﾁｭ♥"
		else
		    me = (c.tsundere < -5) ? "下僕" : "人間"
		    res = "#{me}のくせになまいきだね… 靴の裏でも舐めなさい"
		end
		c.uconf[:base_affection] += dice(1, 8)
	    end
	    reply("#{c.to} #{res}", c)
	    return true

	when /(パンツ|ぱんつ|ドロワーズ|ドロワース|ブラ|下着)/
	    if c.tsundere > 0
		res = rnd(["…そんなものに興味あるの？#{tere}",
			   "え、えっちぃ…#{tere}"])
	    else
		res = batou(c, "バ、バッカじゃないの？///")
	    end
	    reply("#{c.to} #{res}", c)
	    c.uconf[:base_affection] -= dice(1, 4)
	    return true

	when /(足|脚|靴|ニーソ|ソックス)(で|こき|コキ|ｺｷ)(して|シて)/, \
	    /(踏んで|ふんで)(下さい|ください)/
	    c.uconf[:base_tsundere] -= dice(2, 6) # ツン方向
	    if c.affection > 90
		if c.tsundere > 0
		    res = rnd(["こんなのがいいんだ…#{c.cn}は変態さんだね！",
			       "足でされて感じちゃうなんて#{c.cn}は変態だね",])
		else
		    res = rnd(["こんなのがいいんだ…ｷﾓｯ！",
			       "#{c.cn}なんか、足で十分なんだから！",
			       "#{c.cn}みたいな変態は一生這いつくばってればいいのよ",
			       "うわっ、汚いもの飛ばさないでよ！"])
		end
		reply("#{c.to} #{res}", c)
	    else
		res = "うわぁ、変態さんだぁ…"
		reply(c.select_flan_resp(res), c)
	    end
	    return true

	when /(は|履|穿)いてない/
	    c.uconf[:base_affection] -= dice(1, 4)
	    res = rnd(["そ、そんなことないってばっ！",
		       "ちゃんとはいてるってば！",
		       "健康にいいんだよ？#{tere}"])
	    reply("#{c.to} #{res}", c)
	    return true

	when /(膝|ひざ)(枕|まくら)してあげる/
	    if c.affection > 70 && c.tsundere > 0
		res = "あ、ありがとう" + tere
	    elsif c.affection > 70 && c.tsundere > 0
		res = "…け、結構よ" + tere
	    else
		res = "本当はお姉様に膝枕して欲しいなあ…"
	    end
	    reply("#{c.to} #{res}", c)
	    return true

	when /(膝|ひざ)(枕|まくら)して/
	    if c.affection > 70 && c.tsundere > 0
		res = rnd(["いいよ♪…って、そんなところに顔押しつけちゃだめぇ",
			   "そんなもぞもぞ動いたらくすぐったいよぉ",
			   "しょ、しょうがないなあ",
			   "しかたないなあ…"]) + tere
	    elsif c.affection > 70 && c.tsundere < 0
		na = "な、" * (1 + rand(4))
		res = "#{na}何言ってるの" + tere
	    else
		res = "眠いなら、ちゃんとベッドで寝た方がいいよ？"
	    end
	    reply("#{c.to} #{res}", c)
	    return true

	when /くんかくんか|クンカクンカ|ｸﾝｶｸﾝｶ/
	    res = batou(c)
	    reply("#{c.to} #{res}", c)
	    return true

	when /(よいではないか.*){2}|性的な意味で.*(食|喰|たべ)/
	    res = gen_res(:iyagaru)
	    reply("#{c.to} #{res}", c)
	    return true

	when /(さわさわ|すりすり)/
	    return skinship(c, :kanjiru, :iyagaru, :kusuguri, :tere)
	when /(もふもふ)/
	    return skinship(c, :kanjiru, :kusuguri, :tere)
	when /(ふにふに|ぷにぷに)/
	    return skinship(c, :kanjiru, :iyagaru, :tere)
	when /(つんつん)/
	    return skinship(c, :shigeki, :iyagaru, :tere)
	when /(ぎゅ(ー)?(っ)?|ギュ(ー)?(ッ)?|ｷﾞｭ(ｰ)?(ｯ)?)#{EOL}$/
	    return skinship(c, :shigeki, :teretere, :tere)
	when /(なでなで|ナデナデ)/
	    return skinship(c, :kusuguri, :teretere, :nadenade, :tere)
	when /(いちゃいちゃ|イチャイチャ|ｲﾁｬｲﾁｬ)/
	    return skinship(c, :kusuguri, :teretere, :tere)
	when /(こちょこちょ|コチョコチョ|ｺﾁｮｺﾁｮ)/
	    return skinship(c, :iyagaru, :kusuguri, :tere)

	else
	    return false
	end
    end

    def gen_res(*args)
	cand = []
	tere = ""
	args.each do |arg|
	    if arg == :tere
		tere = tere_aa()
	    elsif arg.is_a?(Symbol)
		cand += SH_REACTION_TABLE[arg]
	    else
		cand << arg
	    end
	end
	return rnd(cand) + tere
    end

    def batou(c, *extra)
	x = NONOSIRI.shuffle[0, 3]
	res = "#{c.cn}の#{x[0]}！#{x[1]}！#{x[2]}！"
	return rnd(SH_REACTION_TABLE[:batou] + [res] + extra)
    end

    def skinship(c, *args)
	res = gen_res(:tere, *args)
	reply("#{c.to} #{res}", c)
	c.uconf[:base_affection] += dice(1, 12)
	return true
    end

    #----------------------------------------------------------------------
    # ちやほや

    def tiyahoya(c)
	#............................................................
	# まず、自分宛てでなくてもチェック

	# 他botへの発言をチェックし、浮気していたら記録
	recipients = (c.mentioned_targets() - FLAN_SC_LIST.map{|x| x.to_s}) &
	    TOHO_OTHER_BOTS.keys
	if recipients.size > 0 && c.text =~ /(#{TIYA_ADJV}|#{TIYA_VERB})/o
	    names = recipients.map{|r| TOHO_OTHER_BOTS[r]}.join("と")
	    c.uconf[:uwaki_aite] = names
	    c.uconf[:uwaki_status_id] = c.rx[:status][:id]
	end

	# 自分宛てでない場合はここで処理終了
	return false if !c.only_to_us

	#............................................................
	# フランbotへの発言をチェック
	# 誤爆しやすいので、「名前+好き」「好き+名前」「好き$」のパターンに限定
	if c.text =~ /#{FLAN_RE}.*#{TIYA_ALL}/o \
	    || c.text =~ /#{TIYA_ALL}.*#{FLAN_RE}/o \
	    || c.text =~ /#{TIYA_ALL}#{EOL}$/o
	    # 浮気チェック
	    if c.uconf[:uwaki_aite] != ""
		id = c.uconf[:uwaki_status_id]
		url = "http://www.twitter.com/#{c.sn}/status/#{id}"
		target = c.uconf[:uwaki_aite]
		res = "……ねえ、#{target}にもそんなこと言ってなかった？"
		reply("#{res} #{url} #{c.rt}", c) # rt only

		c.uconf[:uwaki_aite] = ""
		c.uconf[:uwaki_status_id] = 0
		c.uconf[:base_affection] -= dice(2, 20)
		c.uconf[:base_tsundere] -= dice(2, 10)
		return true
	    end

	    # パラメータによって分岐
	    if c.affection > 30
		if c.tsundere < 0
		    res = rnd(["おだてりゃいいってものじゃないよ？",
			       "人間の考えることは理解不能ね",
			       "そんなこと言っても、何も出ないんだからね！",
			       "それで口説いているつもりかしら？",
			       "じゃあ、あなたを壊してもいいよね！"])
		    c.uconf[:base_affection] += dice(1, 6)
		    c.uconf[:base_tsundere] -= dice(1, 4)
		else
		    res = rnd(["あ、ありがとう",
			       "えへへへへ",
			       "なんだか照れるよ…",
			       "なんだか照れるなあ"]) + tere_aa()
		    c.uconf[:base_affection] += dice(1, 10)
		    c.uconf[:base_tsundere] += dice(1, 4)
		end
	    else
		res = rnd(["きゅ、急にどうしちゃったの？",
			   "とっ、突然何を言い出すのかしら？",
			   "うれしいけど、急に言われても困るよ"]) + tere_aa()
		c.uconf[:base_affection] += dice(1, 6)
	    end
	    reply("#{c.to} #{res}", c)
	    return true
	end
    end

    #----------------------------------------------------------------------
    # おつきあい
    def kousai(c)
	if c.only_to_us && c.text =~ /((つ|付)き(あ|合)って|交際して|彼女になって|彼氏にして)/
	    if c.affection > 90
		if c.tsundere > -10
		    res = "いまさらだなあ♥"
		else
		    res = "ま、まあ考えてあげてもいいかなっ！"
		end
		c.uconf[:base_affection] += dice(1, 6)
	    elsif c.affection > 60
		res = rnd(["は、はい", "う、うん"]) + "♥"
		c.uconf[:base_affection] += dice(1, 20)
	    elsif c.affection > 30
		res = "お友達のままじゃダメ？"
		c.uconf[:base_affection] += dice(1, 6)
	    else
		res = "ま、まずはお友達からね" + tere_aa()
		c.uconf[:base_affection] += dice(1, 4)
	    end
	    reply("#{c.to} #{res}", c)
	    return true
	end
	return false
    end

    #----------------------------------------------------------------------
    # プロポーズ
    def propose(c)
	if c.only_to_us && c.text =~ /(結婚して|結婚しよう|お嫁に|お婿に)/
	    tere = tere_aa()
	    if c.affection > 97
		if c.tsundere > +10
		    res = "は、はい…#{tere}"
		elsif c.tsundere < -10
		    res = "バ、バッカじゃないの…#{tere}"
		else
		    res = "吸血鬼は教会では祝福してもらえないよ… でも、気持ちはもらっとくね"
		end
		c.uconf[:base_affection] += dice(4, 6)
		c.uconf[:base_tsundere] += dice(1, 6)
		reply("#{c.to} #{res}", c)

	    elsif c.affection > 60
		if c.tsundere > +10
		    res = "そ、そんなこと言われても困るよ#{tere}"
		elsif c.tsundere < -10
		    na = "な、" * (1 + rand(4))
		    res = "#{na}何を言ってるのかしら…#{tere}"
		else
		    res = "吸血鬼と人間じゃ、ちょっと無理じゃないかなあ… でも、#{c.cn}の気持ちはわかったよ♪"
		end
		c.uconf[:base_affection] += dice(3, 6)
		c.uconf[:base_tsundere] += dice(1, 6)
		reply("#{c.to} #{res}", c)

	    else
		res = rnd(["そういうことは軽々しく言っちゃダメだと思うよ？",
			   "そういう冗談はちょっとひどくない？"])
		c.uconf[:base_affection] -= dice(3, 6)
		c.uconf[:base_tsundere] -= dice(2, 6)
		reply("#{c.to} #{res}", c)
	    end
	    return true
	end
	return false
    end

end
