# -*- coding: utf-8 -*-

require 'core-context'
require 'core-resgen'

class SampleResgen < ResgenProcess
    # timelineおよびmentionを処理し、必要に応じて応答する
    # botあてのリプライはtimelineとしてもmentionとしても処理される
    # このシステムではmentionではフォロー依頼・リムーブ依頼にしか使わない
    # ことにする
    def generate_response(rx)
	# 入力に含まれるユーザのTwitter情報をキャッシュしたい場合はここで処理

	# 応答生成処理本体
	actors = @bot_list
	c = Context.new()
	c.setup_common(rx)
	begin
	    c.setup_subc(actors, self)
	    set_default_poster_candidates(c.get_default_poster_candidates())

	    if !rx[:processed_as_mention] && rx[:mention_for].size > 0
		respond_as_mention(c)
		rx[:processed_as_mention] = true
	    end

	    if !rx[:processed_as_timeline] && rx[:timeline_for].size > 0
		respond_as_timeline(c)
		rx[:processed_as_timeline] = true
	    end
	end
    end

    #..................................................

    # mentionとして反応する
    # フォロー要求かリムーブ要求の時だけ
    def respond_as_mention(c)
	rt = "RT @#{c.sn}: #{c.orig_text}"
	if follow_request?(c)
	    if c.rel[:all_actors_follow_user]
		reply("すでに相互フォローです #{rt}", c)
	    else
		c.rel[:indeces_not_following_user].each do |index|
		    @inout[index].puts("add_friend #{c.sn}")
		end
		s = (c.rel[:actors_not_following_user].map do |bot|
			 '@' + bot.to_s
		     end).join(', ')
		reply("フォローしました(#{s}) #{rt}", c)
	    end

	    if !c.rel[:user_follows_all_actors]
		# ユーザがすべてのbotをフォローしていない場合は警告
		s = (c.rel[:actors_not_followed_by_user].map do |bot|
			 '@' + bot.to_s
		     end).join(', ')
		reply("まだ#{s}をフォローしていないようです", c)
	    end

	elsif remove_request?(c)
	    if c.rel[:any_actors_follow_user]
		c.rel[:indeces_following_user].each do |index|
		    @inout[index].puts("remove_friend #{c.sn}")
		end
		s = (c.rel[:actors_following_user].map do |bot|
			 '@' + bot.to_s
		     end).join(', ')
		reply("フォローを外しました(#{s}) #{rt}", c)
	    else
		reply("botからはフォローしてません #{rt}", c)
	    end
	end
    end

    def follow_request?(c)
	return c.text =~ /^(follow|フォロー)して/
    end

    def remove_request?(c)
	return c.text =~ /^(remove|リムーブ)して/
    end

    #..................................................

    IGNORINGS = []
    SPECIALS = []

    def respond_as_timeline(c)
	# ツイート元によって分岐
	# - 自クラスタ
	# - 無視するアカウント
	# - 特別対応するアカウント
	# - 一般アカウント
	if c.rel[:actors].include?(c.sn.to_sym)
	    # 自クラスタのメンバから
	elsif IGNORINGS.include?(c.sn)
	    # 無視するアカウントから
	elsif SPECIALS.include?(c.sn)
	    # 特別対応するアカウントから
	else
	    # 一般アカウントから
	    res = c.text.gsub(/@/, '{at}')
	    reply("test: #{res}", c)
	end
    end
end

o = SampleResgen.new()
o.start()
