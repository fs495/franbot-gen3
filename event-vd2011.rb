# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

require 'GD'
require 'core-inout'

class TohoResgen
    VALENTINES_DAY = Time.local(2011, 2, 14)

    COLORS = {
	:red =>		[255,   0,   0],
	:pink =>	[255, 179, 204],
	:yellow =>	[255, 255,   0],
	:white =>	[255, 255, 255],
	:skyblue =>	[ 51, 204, 230],
    }

    PIC_TAB = [# URL・ファイル名・作者・文字色
	       ['http://www.flickr.com/photos/cup_prof/4335861843/',
		'4335861843_bf1dec5dd9.jpg', 'cupprof', :pink],
	       ['http://www.flickr.com/photos/eszter/68153223/',
		'68153223_270d6e369d_o.jpg', 'eszter', :pink],
	       ['http://www.flickr.com/photos/427/2508045734/',
		'2508045734_a9a4ccd78e.jpg', '427', :red],
	       ['http://www.flickr.com/photos/quintanaroo/489358037/',
		'489358037_646e5aa407.jpg', 'QuintanaRoo', :red],
	       ['http://www.flickr.com/photos/quintanaroo/404148118/',
		'404148118_e2968abf6e.jpg', 'Quintanaroo', :red],
	       ['http://www.flickr.com/photos/bunchofpants/34840919/',
		'34840919_1851c6ed02.jpg', 'bunchofpants', :red],
	       ['http://www.flickr.com/photos/emilywaltonjones/1112839370/',
		'1112839370_c3c564a7d7_b.jpg', 'emilywjones', :pink],
	       ['http://www.flickr.com/photos/megpi/8567753/',
		'8567753_e6d40e8387_o.jpg', 'megpi', :skyblue],
	       ['http://www.flickr.com/photos/lynac/525957708/',
		'525957708_f965f92c8f.jpg', 'lynac', :white],
	       ['http://www.flickr.com/photos/tracyhunter/110294234/',
		'110294234_6f3695a78f_o.jpg', 'Tracy Hunter', :pink],
	       ['http://www.flickr.com/photos/ajagendorf25/3280407786/',
		'3280407786_70f0278ffc.jpg', 'ajagendorf25', :skyblue],
	       ['http://www.flickr.com/photos/quintanaroo/2242732240/',
		'2242732240_fe4d03b6a4_o.jpg', 'Quintanaroo', :pink],
	       ['http://www.flickr.com/photos/stephbond/2859151108/',
		'2859151108_39c950428d_b.jpg', 'stephbond', :red],
	      ]

    HEART_FILE = 'etc/image_orig/heart.png'
    TTF_FILE = 'etc/image_orig/uzura.ttf'
    TTF_PT = 22

    #----------------------------------------------------------------------

    def create_image(opt)
	# オープンと情報取得
	img = GD::Image.new_from_jpeg(opt[:orig_file])
	color = img.colorAllocate(*COLORS[opt[:color]])
	bounds = img.bounds
	img_w, img_h = bounds[0], bounds[1]

	# ハートマークで枠を書く
	heart = GD::Image.new_from_png(HEART_FILE)
	bounds = heart.bounds
	cw, ch = bounds[0], bounds[1]
	nw, nh = img_w / cw, img_h / ch
	off_w, off_h = (img_w - nw * cw) / 2, (img_h - nh * ch) / 2

	(0 ... nh).each do |idx_h|
	    [0, nw - 1].each do |idx_w|
		x, y = off_w + idx_w * cw, off_h + idx_h * ch
		heart.copy(img, x, y, 0, 0, 32, 32)
	    end
	end
	(1 ... nw-1).each do |idx_w|
	    [0, nh - 1].each do |idx_h|
		x, y = off_w + idx_w * cw, off_h + idx_h * ch
		heart.copy(img, x, y, 0, 0, 32, 32)
	    end
	end

	# メッセージ
	x, y = off_w + cw * 1.2, off_h + ch * 1.2
	draw_string(img, color, x, y, :tl, opt[:user_name])

	opt[:messages].reverse.each_with_index do |s, index|
	    x = off_w + cw * (nw - 1.2)
	    y = off_h + ch * (nh - index - 1.2)
	    draw_string(img, color, x, y, :br, s)
	end

	# ファイルを出力する
	open(opt[:dest_file], "w+") do |file|
	    img.jpeg(file, 95)
	end
    end

    def draw_string(img, color, x, y, orientation, s)
	ox, oy = nil, nil
	r = GD::Image.stringTTF(color, TTF_FILE, TTF_PT, 0, 0, 0, s)[1]
	case orientation
	when :bl; ox, oy = r[0], r[1]
	when :br; ox, oy = r[2], r[3]
	when :tr; ox, oy = r[4], r[5]
	when :tl; ox, oy = r[6], r[7]
	end
	img.stringTTF(color, TTF_FILE, TTF_PT, 0, x - ox, y - oy, s)
    end

    def self.test_create_image()
	opt = {
	    :orig_file => 'image_orig/4335861843_bf1dec5dd9.jpg',
	    :color => :pink,
	    :dest_file => 'foo.jpg',
	    :user_name => '私だけの大事なお兄様',
	    :messages => ['お兄様', 'いつもありがとう！'],
	}
	o = TohoResgen.new()
	o.create_image(opt)
    end

    #----------------------------------------------------------------------

    def check_event(c)
	return false if !c.only_to_us
	return false if c.text !~ /(チョコ|ﾁｮｺ|Choco).*(頂戴|ちょうだい|欲しい|ほしい|please|プリーズ|ﾌﾟﾘｰｽﾞ|ぷりーず|下さい|ください)/
	#return false if c.text !~ /^vd2011/

	# テスト用: お世話係でなければ反応しない
	# return false if c.sn != 'fs495'

	# バレンタインデー当日でないと反応しない
	curr = Time.now()
	return false if !(curr.month == 2 && curr.mday == 14 &&
			  (curr.hour > 0 || curr.min > 15))

	# 優先度の設定
	prob = 33 + c.uconf[:base_affection] / 3 + 
	    (curr - VALENTINES_DAY) * 33 / 24 / 3600
	prob = 100 if [57295594, 61733434, 81332487, 82628825,
		       110821893, 124468615].include?(c.u[:id])
	prob = 100
	if rand(100) >= prob
	    if c.cconf[:vd2011_skip] == nil
		c.cconf[:vd2011_skip] = true
		reply("#{c.to} えっ、チョコレート欲しいの…？", c)
	    end
	    return true
	end

	img_msgs = ["いつもフランと", "あそんでくれてありがとう", "大好き///"]
	res_msg = rnd(["えっと…これ…#{tere_aa()} っ",
		       "えーと…はい…#{tere_aa()} っ",
		       "ど、どうぞ♥ #{tere_aa()} っ",
		       "あとで見ておいてねっ#{tere_aa()} っ"])

	if c.cconf[:vd2011_done]
	    reply("#{c.to} いっこだけだよぉ… #{c.cconf[:vd2011_url]}", c)
	    return true
	end

	begin
	    # 画像の生成
	    pic = rnd(PIC_TAB)
	    gop = {
		:orig_file => 'etc/image_orig/' + pic[1],
		:dest_file => 'etc/image_dest/' + c.u[:id].to_s + '.jpg',
		:color => pic[3],
		:user_name => c.cn,
		:messages => img_msgs,
	    }
	    create_image(gop)
	    @logger.info("vd2011: created #{gop[:dest_file]}")

	    # アップロード
	    uop = {
		:message => "Using photo by #{pic[2]} under Creative Commons License. See original page for detail statement: #{pic[0]}",
		:file => gop[:dest_file],
		:api_key => 'fe26a28782b2fbc1512b81cfbea0e6b9',
	    }

	    bot = rnd(SUBCLUSTER[:flan_subc])
	    file = sprintf(BOT_CONFIG_FILE, bot)
	    store = ConfigStore::open_store(file)
	    store.transaction(true) do
		cfg = store[:bot_config]
		uop[:consumer_key] = cfg.consumer_key
		uop[:consumer_secret] = cfg.consumer_secret
		uop[:access_token] = cfg.access_token
		uop[:access_token_secret] = cfg.access_token_secret
	    end
	    r = Twitpic.post(uop)
	    raise "upload failed" if r.nil? || r.body.nil?

	    # アップロード結果を格納してTLにポスト
	    raise "url not found: #{r.body.content}" if r.body.content !~ %r[<url>(.*)</url>]
	    c.cconf[:vd2011_url] = $1
	    c.cconf[:vd2011_done] = true
	    @logger.info("uploaded: #{c.cconf[:vd2011_url]}")
	    reply("#{c.to} #{res_msg} #{c.cconf[:vd2011_url]}", c)
	    return true
	rescue Timeout::Error, StandardError => ex
	    s = ex.message
	    ex.backtrace.each{|bt| s += "\n\t" + bt.to_s}
	    @logger.error("vd2011: #{s}")
	end
    end

    def start_vd_chat(c)
	curr = Time.now
	if curr.hour == 0 && curr.min > 2 && @vd2011_dialog == nil && false
	    a = [[:flan_subc, "うう、お姉様気に入ってくれるかな…"],
		 [:remy_subc, "@@ 私がどうかしたの？"],
		 [:flan_subc, "@@ ドキッ！"],
		 [:remy_subc, "@@ なんかあやしい…"],
		 [:flan_subc, "@@ お姉様、これっ＞＜"],
		 [:remy_subc, "@@ これは… もしかしてチョコレート？"],
		 [:flan_subc, "@@ 2月14日はチョコレート渡すって聞いたの… すきな相手にｺﾞﾆｮｺﾞﾆｮ…"],
		 [:remy_subc, "@@ うーん、この紅い悪魔に聖人にちなんだものを渡すとは…"],
		 [:flan_subc, "@@ も、もしかして気に入らなかった？"],
		 [:remy_subc, "@@ ううん、そんなことないよ。嬉しいわ。ありがとうフラン///"],
		 [:flan_subc, "@@ えへへへ…(///ω///)"],
		 [:remy_subc, "@@ 恥ずかしい妹ね… それはそうと、渡す相手はどうのこうのってさっき言ってなかった？私なんかで良かったの？"],
		 [:flan_subc, "@@ そ、それは… なんでもないよーだ♪"],
		 [:remy_subc, "@@ あ、待ちなさいったらフラン///"],
		]
	    register_chatseq(a, SUBCLUSTER)
	    @vd2011_dialog = :done
	end
    end
end

#TohoResgen.test_create_image()
