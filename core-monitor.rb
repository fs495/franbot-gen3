# -*- coding: utf-8 -*-

require 'yaml/store'
require 'logger'
require 'socket'
require 'core-consts'
require 'core-stores'
require 'core-subprocess'

class BotMonitor < Subprocess
    include Commondef

    def initialize
	super(:monitor)
	open_logger(Logger::INFO)
    end

    def start(argv)
	if argv.size > 0
	    # 引数がある場合は特別処理してすぐに終了
	    special_run(argv)
	    exit(0)
	else
	    # TODO: 自動再起動処理
	    prepare()
	    run()
	end
    end

    # 引数が指定された特殊な起動時の処理
    def special_run(argv)
	case argv[0]
	when '-sample-cluster-config'
	    Cluster::save_sample_config()

	when '-sample-bot-config'
	    system('ruby', 'core-inout.rb', '-save-sample-config')

	when '-discard-pended'
	    bot_list = Cluster::load_config().bot_list
	    bot_list.each_with_index do |bot, index|
		system('ruby', 'core-inout.rb', '-discard-pended', index.to_s)
	    end

	else
	    STDERR.print "#{argv[0]}: unknown option\n"
	    exit(1)
	end
    end

    # クラスターの通常起動処理
    def prepare()
	# ログ
	@logger.info("##### preparing cluster #####")
	@cluster_conf = Cluster::load_config()

	# 自動再起動の抑止状態ならプログラムを終了する
	if analyze_boot()
	    @logger.info("shutting down...")
	    exit(0)
	end

	# 自分のpidをファイルに記録
	open(File::join(VAR_DIR, "pid_monitor"), "w") {|io|
	    io.puts($$)
	}
	@logger.info("monitor pid is #{$$}")

	# 入力データのロックファイルを削除
	input_lock = File::join(VAR_DIR, GLOBAL_INPUT_LOCK)
	FileUtils::rm_f(input_lock)

	# サーバーとしてのポートをオープン
	tries = 0
	begin
	    @gensock = TCPServer.open(HOSTNAME, @cluster_conf.port_number)
	rescue Errno::EADDRINUSE
	    raise if tries > 20
	    @logger.info("port open failed... retrying (count #{tries})")
	    tries += 1
	    sleep(3)
	    retry
	end
    end

    def analyze_boot()
	# シャットダウンログを確認し、短時間で何度も再起動している場合は
	# 自動再起動を抑止する
	return false # TODO
    end

    # メインループ
    #   以下の処理を行なう
    #   - 長すぎる過渡状態を強制的に終了
    #   - コマンドの配送
    #   - 死活監視
    #
    # 全体状態(global_state)
    #   - waiting_for_startup
    #   - established
    #   - waiting_for_close
    #   - aborting
    #
    # 以下のいずれかの条件が成立したとき、メインループの脱出処理を開始する
    #   - いずれかのプロセスが終了した
    #   - いずれかのコネクションが確立後にクローズされた
    #   - 子プロセス群の立ち上げに60秒以上かかった
    #
    # 以下の条件成立時にrun()内のループを終了する
    #   - すべての子プロセスが終了したことを確認できた
    #   - 子プロセスを終了させようとして60秒経過した
    def run()
	@logger.info("##### starting to run cluster #####")
	@global_state = :waiting_for_startup

	# 管理用の情報を設定
	@anon_sock_list = []
	@transition_time = Time.now
	@last_timeout = Time.now

	# 子プロセスを起動する
	ProcState::set_script_hash(@cluster_conf.script)
	@proc_list = {}
	@proc_list[:resgen] = ProcState.new(:resgen).spawn()
	@proc_list[:delivery] = ProcState.new(:delivery).spawn()
	@cluster_conf.bot_list.each_with_index do |bot, n|
	    @proc_list["inout#{n}".to_sym] = ProcState.new(:inout, n).spawn()
	end

	while true
	    # 過渡状態が異常に長く続いていないかチェック
	    check_transient_duration()
	    # 死んだ子プロセスがあれば回収処理
	    reap_child_processes()

	    # 強制終了か、終了処理中に全プロセスが死んだら、ループを抜ける
	    case @global_state
	    when :aborting
		@logger.error("### main loop aborted ###")
		break
	    when :waiting_for_close
		if @proc_list.values.all?{|proc| proc.dead?}
		    @logger.info("### main loop ended ###")
		    break
		end
	    end

	    # 監視対象のソケットの集合を決定し、
	    # コマンドが送受信可能かどうか判定する
	    rfd = [@gensock] + @anon_sock_list
	    wfd = []
	    @proc_list.each do |k, proc|
		rfd << proc.socket if proc.connected?
		wfd << proc.socket if proc.active? && !proc.txqueue.empty?
	    end
	    avails = select(rfd, wfd, nil, 5)

	    if avails == nil
		# アイドル時の処理
		timed_out()
	    else
		# 送受信可能になったソケットの処理
		avails[0].each do |sock|
		    read_socket(sock)
		end
		avails[1].each do |sock|
		    write_socket(sock)
		end
	    end
	end
    end

    # コネクション待ちがタイムアウトしたアイドル時の処理
    # - ログのローテーション
    def timed_out()
	rotate_logs()
    end

    # 過渡状態が長く続きすぎるのを防止する
    def check_transient_duration()
	curr = Time.now
	case @global_state
	when :waiting_for_startup
	    # 起動状態が長く続く場合は終了処理を開始する
	    if curr > @transition_time + 60
		@logger.error("children will not wake up")
		kill_children()
		return
	    end
	when :waiting_for_close
	    # 終了状態が長く続く場合は強制終了状態に移行させる
	    if curr > @transition_time + 60
		@logger.error("children will not die")
		@global_state = :aborting
		return
	    end
	end
    end

    # 死んでしまった子プロセスがあるか調べ、
    # 死んでたら回収するともに残りの子プロセスもkillする。
    def reap_child_processes()
	need_termination = false
	@proc_list.each do |k, proc|
	    next if proc.dead?

	    # 存在しないpidをwaitすると例外発生するが、ここではrescueしない
	    pid = Process.waitpid(proc.pid, Process::WNOHANG)
	    if pid != nil
		@logger.warn("#{proc.name} has dead")
		proc.mark_child_as_dead()
		need_termination = true
	    end
	end

	if need_termination
	    kill_children() # 残りの子プロセスもkillする
	end
    end

    # 子プロセスがaliveならkillし、waiting_for_close状態に遷移する。
    # (killしてもwaitするまではalive状態)
    def kill_children()
	@proc_list.each do |k, proc|
	    next if proc.dead?
	    begin
		@logger.info("killing #{proc.name} (pid #{proc.pid})")
		Process.kill(:INT, proc.pid)
	    rescue
		@logger.warn("failed to kill #{proc.name} (pid #{proc.pid})")
	    end
	end

	@global_state = :waiting_for_close
	@transition_time = Time.now
    end

    # 規定の時刻を超えたら、ログをローテートする
    def rotate_logs()
	curr = Time.now
	if @last_timeout.hour == (LOG_ROTATE_HOUR + 23) % 24 &&
		curr.hour == LOG_ROTATE_HOUR
	    level = @logger.level

	    # ログファイルのリネーム
	    base = File::join(VAR_DIR, LOG_FILE)
	    old = base + "." + @last_timeout.strftime("%Y%m%d")
	    @logger.info("closed for rotation")
	    @logger.close
	    begin
		File::rename(base, old)
	    rescue
	    end
	    open_logger(level)

	    # サブプロセスにログの再オープンを指示
	    @proc_list.each do |k, proc|
		proc.txqueue.push("reopen_logger")
	    end
	end
	@last_timeout = curr
    end

    # ソケットからコマンドを受信する
    def read_socket(sock)
	# gensockが受信可能になった時はacceptして、一時ソケットとして記憶
	if sock == @gensock
	    @logger.info("accepted new connection")
	    @anon_sock_list << @gensock.accept()
	    return
	end

	# 一時ソケットがクローズされたらプールから削除
	# 一時ソケットが受信可能になった時はセッションを開始する
	if @anon_sock_list.include?(sock)
	    if sock.eof?
		@logger.error("anonymous socket closed")
		@anon_sock_list.delete(sock)
	    else
		start_sesstion(sock)
	    end
	    return
	end

	# オープン済みソケットがクローズされたらシステム再起動開始
	# オープン済みソケットが受信可能になったらコマンド処理
	@proc_list.each do |k, proc|
	    if sock == proc.socket && proc.connected?
		if sock.eof?
		    @logger.info("#{proc.name} closed connection")
		    proc.set_socket(nil)
		    kill_children()
		else
		    buf = sock.gets().chomp()
		    analyze_command(proc, buf)
		end
		return
	    end
	end
    end

    # セッションを開始する
    def start_sesstion(sock)
	# 匿名コネクションからコマンドを取得
	cmd = sock.gets().chomp()
	if cmd =~ /init (.*)/
	    bot = $1.to_sym
	    if @proc_list.keys.include?(bot) 
		@logger.info("connection identified: #{bot}")
		@proc_list[bot].set_socket(sock)
		@proc_list[bot].set_last_ka_time()
	    else
		# 未知の自称名
		@logger.error("unknown init command: #{bot}")
	    end
	else
	    # 匿名接続からinit以外のコマンド
	    @logger.error("junk input from anonymous socket: #{cmd}")
	end
	@anon_sock_list.delete(sock)

	# すべてのセッションが開始されたらグローバル状態を変更
	if @proc_list.values.all?{|proc| proc.active?}
	    @logger.info("## cluster coming up ##")
	    @global_state = :established
	    @transition_time = Time.now
	    @proc_list.each do |k, proc|
		proc.socket.puts("init_ack")
	    end
	end
    end

    # コマンドを受け取り、宛先の送信キューに入れる
    def analyze_command(proc, s)
	proc.set_last_ka_time()

	if @global_state != :established
	    @logger.warn("#{proc.name} got message while unestablished")
	    kill_children()
	    return
	end

	if s !~ /<(.*?)>(.*)/
	    @logger.error("illegal command format: #{s}")
	    return
	end

	dest, payload = $1.to_sym, $2
	@logger.debug("analyze_command: #{proc.name}->#{dest}: #{payload}")

	if !@proc_list.has_key?(dest)
	    @logger.error("unknown destination: #{dest}")
	    return
	end

	@proc_list[dest].txqueue.push(payload)
    end

    # 送信キューからコマンドを取り出し、ソケットに送信する
    def write_socket(sock)
	@proc_list.each do |dest, proc|
	    next if proc.socket != sock
	    next if proc.txqueue.empty?
	    payload = proc.txqueue.shift()
	    @logger.debug("write_socket: ->#{dest}: #{payload}")
	    sock.puts(payload)
	end
    end

    #----------------------------------------------------------------------

    # サブプロセスを表すクラス
    # 以下の情報を管理する。
    #   - 子プロセスのプロセスID
    #   - 通信用ソケット
    #   - メッセージバッファ
    #   - keepalive管理用のタイマ
    #
    # 子プロセスの状態: alive <-> dead
    # 初期状態でdeadで、spawnされてalive、killされてdead
    #
    # 通信用ソケットの状態: connected <-> disconnected
    # 初期状態でdisconnected、セッションが開始されてconnected、
    # 切断されてdisconnected
    #
    # サブプロセスの状態(上記2つの状態から自動的に決定): active <-> inactive
    # 子プロセスがaliveかつ通信用ソケットがconnectedなら
    # サブプロセスはactiveであると判断
    #
    class ProcState
	include Commondef

	attr_reader :basename
	attr_reader :pid, :socket
	attr_reader :txqueue
	attr_reader :last_ka_time

	def initialize(basename, instance = nil)
	    @basename, @instance_nr = basename, instance
	    @pid, @socket = nil, nil
	    @txqueue = []
	end

	def self.set_script_hash(script)
	    @@script = script
	end

	def name()
	    return "#{@basename}#{@instance_nr}"
	end

	def active?()
	    # 子プロセス状態とソケット状態で自動的に決定される
	    return @pid != nil && @socket != nil
	end

	def set_last_ka_time(time = Time.now.to_i)
	    @last_ka_time = time
	end

	#----------------------------------------------------------------------
	# プロセス

	def spawn()
	    # スクリプト名を決定
	    script = "core-#{@basename}.rb"
	    if @@script.is_a?(Hash) && @@script[@basename] != nil
		script = @@script[@basename]
	    end

	    # Rubyプロセスを起動
	    args = [script]
	    args << @instance_nr.to_s if @instance_nr != nil
	    @pid = fork {
		exec('ruby', *args)
		raise "exec failed: args = #{args.join(',')}"
		exit(1)
	    }

	    # プロセスIDをファイルに記録
	    file = File::join(VAR_DIR, "pid_#{@basename}#{@instance_nr}")
	    open(file, "w") {|io|
		io.puts(@pid)
	    }
	    return self
	end

	def mark_child_as_dead()
	    @pid = nil
	end

	def dead?()
	    return @pid == nil
	end

	#----------------------------------------------------------------------
	# ソケット
	def set_socket(sock)
	    @socket = sock
	end

	def connected?()
	    return @socket != nil
	end
    end
end

monitor = BotMonitor.new()
monitor.start(ARGV)
