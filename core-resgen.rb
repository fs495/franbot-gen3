# -*- coding: utf-8 -*-

require 'logger'
require 'socket'
require 'timeout'
require 'yaml/store'

require 'core-consts'
require 'core-subprocess'
require 'core-stores'

# === サブクラスで使えるメンバ・メソッド ===
# @logger
# @bot_list
#	クラスタに属する全botのリスト
# @friends[bot], @followers[bot]
#	friendship情報
#
# reply(), post(), store_txdata()
# set_default_poster_candidates(actors)
#
class ResgenProcess < Subprocess
    include Commondef

    DEFAULT_POSTDATA_TTL = 600

    def initialize
	super(:resgen)
	open_logger(Logger::INFO)

	@cluster_conf = Cluster::load_config()
	@bot_list = @cluster_conf.bot_list
	@bot_to_index = {}
	@bot_list.each_with_index do |bot, index|
	    @bot_to_index[bot] = index
	end

	@fstore = VarStore::open_store(FRIENDSHIP_STORE_FILE)
	@friends, @followers = {}, {}
    end

    def prepare()
	connect_to_monitor(@cluster_conf.port_number, 5)

	# 現時点でのfriendship情報をロード
	friendship_updated()

	# 拡張IDの初期値設定
	@extid_count = Time.now.to_i * 100

	# 会話シーケンスのリストを初期化
	@chatseq_list = []
    end

    def read_socket(s)
	@logger.debug("received command: #{s}")

	case s
	when /req_resgen (.*)/
	    resgen($1)

	when /friendship_updated/
	    friendship_updated()
	when /ban_started (.*)/
	    ban_started($1.to_sym)
	when /ban_ended (.*)/
	    ban_ended($1.to_sym)
	when /post_done (.*) (\d*) (succeeded|failed) (.*)/
	    extid, status_id, result, bot = $1, $2, $3, $4
	    @logger.info("#{extid} post #{result} as #{bot} #{status_id}")
	    post_done($1, $2, $3.to_sym, $4.to_sym) if $4 != ''

	when /quit_process/
	    @logger.info("quitting process")
	    close_monitor()
	    sleep(1)
	    exit()
	when /reopen_logger/
	    reopen_logger()
	else
	    @logger.warn("invalid command: #{s.chomp}")
	end
    end

    #----------------------------------------------------------------------
    # 応答生成

    # 応答生成時に更新される入力データの要素
    ELEMS_UPDATED_ON_RESGEN = [:resgen_hint,
			       :processed_as_mention,
			       :processed_as_timeline]

    # 応答を生成する
    def resgen(extid)
	istore = InputStore::open_store(IN_ACTIVE, extid)
	istore.transaction do
	    rx, old = {}, {}
	    # 入力データの複製をrxに作成
	    istore.roots().each do |key|
		rx[key] = istore[key]
	    end
	    # もともとの値を保存しておく
	    ELEMS_UPDATED_ON_RESGEN.each do |key|
		old[key] = istore[key]
	    end

	    # 応答を生成する
	    generate_response(rx)

	    # 入力データが更新されていたら、変更分だけファイルに書き戻す
	    needs_commit = false
	    ELEMS_UPDATED_ON_RESGEN.each do |key|
		if old[key] != rx[key]
		    istore[key] = rx[key]
		    needs_commit = true
		end
	    end
	    istore.commit() if needs_commit
	end
    end

    # 応答を生成する実際の処理
    def generate_response(rx)
	# サブクラスでオーバーライドする
	# - 入力データrxは変化しない (ストアは排他制御されている)
	# - 管理のためにrx[:processed_as_*]とrx[:resgen_hint]を使ってよい
	#   これらの値は入力ストアに自動的に書き戻される
    end

    #----------------------------------------------------------------------
    # ポスト生成

    # 返信先のデフォルトを設定する
    def set_default_poster_candidates(actors)
	@default_postercand = actors
    end

    # StringまたはHashをポスト
    def post(maybe_text)
	data = maybe_text.is_a?(String) ? {:text => maybe_text} : maybe_text
	return store_txdata(data)
    end

    # post()と同じだが、ポスト元のbotを指定する
    def post_from(text, from)
	data = {
	    :text => text,
	    :postercand => [from],
	}
	return store_txdata(data)
    end

    # post()と同じだが、in-reply-toを付ける
    def reply(text, context)
	if !context.is_a?(Context)
	    @logger.error("context is #{context.class}")
	    return # 以前からの形式はエラーにする
	end

	data = {
	    :text => text,
	    :in_reply_to => context.rx[:status][:id],
	}
	return store_txdata(data)
    end

    # post()と同じだが、フォロワーがリプしたbotだけからリプを返す
    # サブクラスタ内の別のbotからリプさせると混乱しそうなリプにだけ使う
    def reply_targeted(text, context)
	targets = context.mentioned_targets() & context.actors
	if targets.empty?
	    @logger.error("reply from nobody: #{context.to_s}")
	    return
	end

	data = {
	    :text => text,
	    :in_reply_to => context.rx[:status][:id],
	    :postercand => [targets[0].to_sym],
	}
	return store_txdata(data)
    end

    # 出力データを配送依頼する
    def store_txdata(tx)
	# 必須フィールドのチェック
	raise "missing text" if !tx.has_key?(:text)

	# 入力してないデータがあれば補完する
	curr = Time.now
	tx[:nr_retries] ||= 0
	tx[:registered_at] ||= curr.to_f
	tx[:valid_after] ||= curr.to_i
	tx[:time_to_live] ||= DEFAULT_POSTDATA_TTL
	tx[:postercand] = @default_postercand unless tx.has_key?(:postercand)

	# 強制的に設定
	tx[:extid] = @extid_count.to_s
	tx[:delivery_log] = []
	@extid_count += 1

	# 出力ストアに格納して配送プロセスに通知する
	ostore = OutputStore::open_store(OUT_ACTIVE, tx[:extid])
	ostore.transaction do
	    tx.keys.each do |key|
		ostore[key] = tx[key]
	    end
	    ostore.commit()
	end
	if tx.has_key?(:in_reply_to)
	    extra = " in reply to #{tx[:in_reply_to]}"
	else
	    extra = ""
	end
	@logger.debug("#{tx[:extid]} scheduled to delivery #{extra}")
	sendmsg("req_delivery #{tx[:extid]}", "delivery")

	return tx
    end

    #----------------------------------------------------------------------
    # friendship管理

    # inoutプロセスがfriendship情報の更新を通知してきたときの処理。
    # ストアからメモリにロードしてくる。
    # 最初の起動時・bot追加時はまだファイルに格納されてない場合あり。
    def friendship_updated()
	@logger.info("friendship updated, reloading file store")
	@fstore.transaction(true) do
	    @bot_list.each do |bot|
		@friends[bot] = []
		if @fstore[:friends].is_a?(Hash)
		    @friends[bot] = @fstore[:friends][bot]
		end

		@followers[bot] = []
		if @fstore[:followers].is_a?(Hash)
		    @followers[bot] = @fstore[:followers][bot]
		end
	    end
	end

    end

    # ユーザとサブクラスタの関係を示すHashを得る
    # このHashは以下のメンバを持つ
    #  - 調査したサブクラスに属するbotのリスト
    #  - ユーザをフォローしているbotのリスト
    #  - ユーザをフォローしていないbotのリスト
    #  - ユーザがフォローしているbotのリスト
    #  - ユーザがフォローしていないbotのリスト
    #  - すべて、または一部のbotがユーザをフォローしているかのフラグ
    #  - ユーザがすべて、または一部のbotをフォローしているかのフラグ
    def analyze_relationship(user, actors)
	f = {
	    :actors => actors,
	    :actors_following_user => [],
	    :actors_not_following_user => [],
	    :actors_followed_by_user => [],
	    :actors_not_followed_by_user => [],
	    :all_actors_follow_user => true,
	    :any_actors_follow_user => false,
	    :user_follows_all_actors => true,
	    :user_follows_any_actors => false,
	}

	actors.each do |bot|
	    if @friends[bot].include?(user[:id])
		f[:actors_following_user] << bot
		f[:any_actors_follow_user] = true
	    else
		f[:actors_not_following_user] << bot
		f[:all_actors_follow_user] = false
	    end
	    if @followers[bot].include?(user[:id])
		f[:actors_followed_by_user] << bot
		f[:user_follows_any_actors] = true
	    else
		f[:actors_not_followed_by_user] << bot
		f[:user_follows_all_actors] = false
	    end
	end
	return f
    end

    #----------------------------------------------------------------------

    # 発言規制が開始されたときの処理
    def ban_started(bot)
	# サブクラスでオーバーライドする
    end

    # 発言規制が終了したときの処理
    def ban_ended(bot)
	# サブクラスでオーバーライドする
    end

    # 出力データのポスト処理が完了(成功・失敗含む)したときの処理
    def post_done(extid, status_id, result, bot)
	# サブクラスでオーバーライドする
    end

    #----------------------------------------------------------------------

    # 会話シーケンスを登録する
    # chatseq:: [サブクラスタ, 会話文]を要素とする配列
    #           ここでサブクラスタは、サブクラスタを表すシンボル、
    #           またはbotを表すシンボルの配列
    # subc_hash:: シンボルからbotの配列を表すHash
    def register_chatseq(chatseq, subc_hash)
	cand = chatseq[0][0]
	tx = {
	    :text => chatseq[0][1],
	    :postercand => cand.is_a?(Symbol) ? subc_hash[cand] : cand
	}
	store_txdata(tx)
	@chatseq_list << {
	    :chatseq => chatseq,
	    :count => 1,
	    :waiting_extid => tx[:extid],
	    :subc_hash => subc_hash,
	}
    end

    # 出力データがポストされたとき、現在進行中の会話シーケンスがあるか調べて
    # 該当するシーケンスのものであったら続きの発言を行なう
    # extid:: ポストしたツイートの拡張ID
    # status_id:: ポストしたツイートのステータスID
    # botnm:: 実際にツイートしたbot
    def continue_chatseq(extid, status_id, botnm)
	seqlist = @chatseq_list
	@chatseq_list = []

	seqlist.each do |seq|
	    # チャットシーケンスの対象ではない場合
	    if extid != seq[:waiting_extid]
		@chatseq_list << seq
		next
	    end

	    # チャットシーケンスにまだ残りがあるならポスト
	    cand = seq[:chatseq][seq[:count]][0]
	    cand = seq[:subc_hash][cand] if cand.is_a?(Symbol)
	    text = seq[:chatseq][seq[:count]][1]
	    tx = {
		:text => text.gsub(/@@/, "@#{botnm}"),
		:postercand => cand,
		:in_reply_to => status_id,
		:valid_after => Time.now.to_i + 10,
	    }
	    store_txdata(tx)

	    # シーケンスを次に進める
	    seq[:count] += 1
	    next if seq[:count] >= seq[:chatseq].size

	    # シーケンスを再登録
	    seq[:waiting_extid] = tx[:extid]
	    @chatseq_list << seq
	end
    end

    # 出力データのポストが失敗したときに会話シーケンスを打ち切る
    # extid:: ポストしようとしていたツイート
    # status_id:: 必ず0(未使用)
    # botnm:: 実際にツイートしたbot(未使用)
    def abort_chatseq(extid, status_id, botnm)
	seqlist = @chatseq_list
	@chatseq_list = []

	seqlist.each do |seq|
	    next if extid == seq[:waiting_extid]
	    @chatseq_list << seq
	end
    end
end
