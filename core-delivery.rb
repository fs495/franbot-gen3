# -*- coding: utf-8 -*-

require 'logger'
require 'socket'
require 'timeout'
require 'yaml/store'

require 'core-consts'
require 'core-subprocess'
require 'core-stores'

class DeliveryProcess < Subprocess
    include Commondef

    # botがban状態になったときに、banから解除されたかを試す間隔
    BAN_SKIPPING_TIME = 300

    # 全候補でポストできなかった時にウェイトする時間
    DELIVERY_RETRY_WAIT = 60

    def initialize()
	super(:delivery)
	open_logger(Logger::INFO)
	@cluster_config = Cluster::load_config()

	@ban_started_at = []
	@bot_to_index = {}
	@cluster_config.bot_list.each_with_index do |bot, index|
	    @ban_started_at[index] = nil
	    @bot_to_index[bot] = index
	end

	@suspended = []
    end

    def prepare()
	connect_to_monitor(@cluster_config.port_number, 30)
	# 以前に生成され、まだ配送されてない応答の配送を試みる
	@logger.info("delivering unsent output at startup")
	OutputStore::traverse(OUT_ACTIVE).each do |extid|
	    deliver(extid, 'startup')
	end
    end

    # タイムアウト時の処理
    #  - 一時保留状態になっていたものを、再度ポスト依頼をかける
    def timed_out()
	curr = Time.now.to_i

	# 登録順にソートしていったん作業用のリストに移し、元のリストはクリア
	worklist = @suspended.sort do |a, b|
	    a[:queued_at] <=> b[:queued_at]
	end
	@suspended.clear

	@logger.debug("delivering suspended output")
	worklist.each do |data|
	    if curr < data[:timeout]
		# まだタイムアウトしない → 保留リストに戻す
		@suspended << data
	    else
		# タイムアウトした → 配送処理にまわす
		deliver(data[:extid], 'wakeup')
	    end

	    # deliver()の呼び出しの結果、inoutプロセスにてban状態が
	    # 変化するかもしれないのでループごとにコマンドを処理する
	    s = try_to_read_socket()
	    read_socket(s) if s != nil
	end
    end

    def read_socket(s)
	@logger.debug("received command: #{s}")

	case s
	when /req_delivery (.*)/
	    deliver($1, 'request')

	when /ban_started (.*)/
	    index = @bot_to_index[$1.to_sym]
	    @ban_started_at[index] = Time.now.to_i
	when /ban_ended (.*)/
	    index = @bot_to_index[$1.to_sym]
	    @ban_started_at[index] = nil

	when /quit_process/
	    @logger.info("quitting process")
	    close_monitor()
	    exit()
	when /reopen_logger/
	    reopen_logger()
	else
	    @logger.warn("invalid command: #{s}")
	end
    end

    #----------------------------------------------------------------------
    # 配送処理

    # リプライを再配分するプロセス
    def deliver(extid, reason)
	ostore = OutputStore::open_store(OUT_ACTIVE, extid)
	ctx = LogContext.new(ostore, Time.now.to_i)
	curr, action, index = ctx.curr, nil, nil

	ostore.transaction do
	    ostore[:delivery_log] ||= []
	    add_info_log(ctx, "#{extid} starting (#{reason})",
			 "starting delivery")

	    # ポストする候補がない場合はエラーとして破棄
	    if ostore[:postercand] == nil || ostore[:postercand].size == 0
		set_fail_message(ctx, "no candidates")
		action = :discard
		break
	    end

	    if curr < ostore[:valid_after] && !ostore[:jit]
		# まだ有効になっていないものは一時停止状態にする
		add_debug_log(ctx, "#{extid} suspended, not valid yet",
			      "not valid yet")
		@suspended << {
		    :extid => extid,
		    :queued_at => ostore[:registered_at],
		    :timeout => ostore[:valid_after]
		}

	    elsif curr > ostore[:valid_after] + ostore[:time_to_live]
		# 古くなったポストデータは破棄する
		add_warn_log(ctx, "#{extid} discarded, expired", "expired")
		set_fail_message(ctx, "Expired at delivery process")
		action = :discard

	    else
		# 実際の配送処理を開始
		candidates = ostore[:postercand]
		phase = ostore[:nr_retries] % (candidates.size + 1)
		ostore[:nr_retries] += 1

		case phase
		when 0 ... candidates.size
		    index = @bot_to_index[candidates[phase]]
		    sig = "inout#{index}"

		    # 規制中のbotはスキップ
		    if @ban_started_at[index] != nil &&
			    curr < @ban_started_at[index] + BAN_SKIPPING_TIME
			add_debug_log(ctx,
				      "#{extid} skipped, #{sig} being banned",
				      "#{sig} being banned")
			redo
		    end

		    # スキップする必要がなかった場合、
		    # ポスト処理をinoutプロセスに依頼する
		    add_info_log(ctx, "#{extid} requested #{sig}",
				 "req_posting to #{sig}")
		    action = :req_post

		when candidates.size
		    # ひととおり全候補にポスト依頼してダメだったらいったん待つ
		    add_info_log(ctx, "#{extid} suspended, banned",
				 "no postable bots")
		    @suspended << {
			:extid => extid,
			:queued_at => ostore[:registered_at],
			:timeout => curr + DELIVERY_RETRY_WAIT,
		    }
		end
	    end
	    ostore.commit()
	end

	case action
	when :discard
	    OutputStore::move(OUT_ACTIVE, OUT_FAILED, extid)
	    sendmsg("post_done #{extid} 0 failed #{index}", "resgen")

	when :req_post
	    sendmsg("req_post #{extid}", "inout#{index}")
	end
    end

    def add_debug_log(ctx, logmsg, histmsg)
	@logger.debug(logmsg)
	ctx.ostore[:delivery_log] << [ctx.curr, histmsg]
    end

    def add_info_log(ctx, logmsg, histmsg)
	@logger.info(logmsg)
	ctx.ostore[:delivery_log] << [ctx.curr, histmsg]
    end

    def add_warn_log(ctx, logmsg, histmsg)
	@logger.warn(logmsg)
	ctx.ostore[:delivery_log] << [ctx.curr, histmsg]
    end

    def set_fail_message(ctx, msg)
	ctx.ostore[:fail_message] = msg
	ctx.ostore[:failed_at] = ctx.curr
    end

    class LogContext
	attr_reader :ostore, :curr
	def initialize(ostore, curr)
	    @ostore = ostore
	    @curr = curr
	end
    end
end

deliverer = DeliveryProcess.new()
deliverer.start()
