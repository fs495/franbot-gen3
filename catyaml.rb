#!/usr/bin/env ruby

require 'yaml/store'
require 'pp'

class String
    def pretty_print(q)
	return q.text(self)
    end
end

ARGV.each do |arg|
    print "=" * 50 + arg + "\n"

    store = YAML::Store.new(arg)
    store.transaction(true) do
	hash = {}
	store.roots.each do |key|
	    hash[key] = store[key]
	end
	pp hash
    end
end
