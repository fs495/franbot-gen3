# -*- coding: utf-8 -*-

class TohoResgen
    #----------------------------------------------------------------------
    # 輝夜bot専用の応答処理
    def respond_for_teruyo_bot(c)
	tere = tere_aa()

	case c.text
	when /フラン先生のサイトに拍手コメしなきゃ/
	    res = rnd(["またこの@#{TERUYO_BOT}って人がコメ付けてる…暇人なんだなあ",
		       "わーい、また褒めてもらっちゃった♥",
		       "かわいい絵ですねって言われるのも飽きちゃったなあ… #{tere}"])
	    reply("#{res} #{c.rt}", c)
	    return

	when /レミ×フラの続きが気になるわ・・・/
	    res = rnd(["お姉様との妄想を絵にするのが追いつかない #{tere}",
		       "今度はフラ×レミも描いてみたいなあ…",
		       "続きはお姉様陵辱ものにしようかなあ"])
	    reply("#{res} #{c.rt}", c)
	    return

	when /今度イベントがあるみたいね・・・フラン先生がサークル参加/
	    res = rnd(["うーん、お姉様がお外に出してくれるかなあ…",
		       "次のイベント…落としそう orz..."])
	    reply("#{res} #{c.rt}", c)
	    return
	end

    end

end
