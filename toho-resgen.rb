# -*- coding: utf-8 -*-

require 'rubygems'
require 'httpclient'
require 'fileutils'
require 'json'

require 'core-resgen'
require 'core-context'
require 'misc-mashup'
require 'misc-number-maker'
require 'misc-pixiv'

require 'toho-consts'
require 'toho-context'
#require 'event-vd2013'

class TohoResgen < ResgenProcess
    include TohoConsts
    include ExternalService

    SPECIAL_SCREEN_NAMES = [MARISA_BOT, MARISA_DEV_BOT]
    SCREEN_NAMES_TO_SKIP = TOHO_OTHER_BOTS.keys
    ABUSIVE_SOURCES = [# 自動ポストサービス(汎用)
		       "twittbot.net", "EasyBotter", "Twibow", "MySweetBot",
		       "BotMaker", "rakubo2", "Stoome", "twiroboJP",
		       "ツイッター bot", "劣化コピー",
		       "★ツイ助★＜無料＞ツイッター多機能便利ツール",
		       "makebot ツイッター便利機能／ボット作成サービス",

		       # 自動ポストサービス(特定)
		       "ツイート数カウントくん", "リプライ数チェッカ",
		       "ツイ廃あらーと", "Countter", "paper.li",
		       "installNow", "TweetMag1c for Android", "Tweet Battery",
		       "HootSuite", "＼( 'ω')／ウオオオオオアアアーーーッ！",
		       "占ぃったー", "京子ちゃんうわあああん",
		       "gohantabeyo.com", "人生三万日しかない",
		       "このまま眠り続けて死ぬ", "うんこはにがくてうまい",
		       "ぐへへ～(＾p＾)", "えへへ(^^)",

		       # スパマー御用達クライアント
		       "ShootingStar", "ShootingStarPro", "ShootingSaru☆",
		       /おさるったー/, "シナプス",

		       # 自動ポストを行う迷惑アカウント
		       "gensou1970bot", "hijirn.bot", "dayosei", "ワロリンヌ",
		       "ゆいたん",

		       # 宣伝行為に使われるTwitter連携
		       "SeesaaBlog", "Tumblr", "Flickr", "twitterfeed",
		       "gooBlog production", "FC2 Blog Notify", "livedoor Blog",
		       "ニコニコ生放送", "ニコニコ動画", "Intel Tweet City",
		      ]
    ABUSIVE_TAGS = ["#NowPlaying", "#nowplaying",
		    "#followme", "#FollowMe",
		    "#nicovideo", "#rori", "#30thou", "#劣化コピー"
		   ]
    
    RANDOM_TALK_FRAC = 40

    def initialize()
	super

	# サブクラスタ関係の定数を設定
	# ・設定ファイルから定数HashのSUBCLUSTERに読み込み
	# ・SUBCLUSTERから定数配列のXXXX_SC_LISTに読み込み
	@cluster_conf.subcluster.each {|k, v| SUBCLUSTER[k] = v}
	SUBCLUSTER.freeze
	SUBCLUSTER[:flan_subc].each{|v| FLAN_SC_LIST << v}
	FLAN_SC_LIST.freeze
	SUBCLUSTER[:remy_subc].each{|v| REMY_SC_LIST << v}
	REMY_SC_LIST.freeze
	SUBCLUSTER[:patche_subc].each{|v| PATCHE_SC_LIST << v}
	PATCHE_SC_LIST.freeze
	SUBCLUSTER[:sakuya_subc].each{|v| SAKUYA_SC_LIST << v}
	SAKUYA_SC_LIST.freeze

	# 時刻関係のメンバ変数を初期化
	curr = Time.now.to_i
	@ban_started_at = {}
	@last_monologue = {}
	SUBCLUSTER.each {|subc| @last_monologue[subc] = curr}
	@last_periodic_process = 0
	@last_special_notice = 0
	@last_chatseq = curr
	@last_birthday_check = Time.at(0)
	@resgen_started_at = curr

	# 記録用ディレクトリを作成
	FileUtils::mkdir_p(File::join(VAR_DIR, UCONF_DIR))
    end

    #----------------------------------------------------------------------
    def generate_response(rx)
	# 強制停止させた場合に0バイトのファイルができることがある
	return if rx == nil || rx[:status] == nil

	need_ustore_update = false
	rx[:resgen_hint] ||= {}
	rx[:processed_as_mention] ||= {}
	rx[:processed_as_timeline] ||= {}

	# ユーザデータのストアをオープン
	u_store = UserStore::open_store(UCONF_DIR, rx[:status][:user][:id].to_s)

	# 応答コンテクスト作成
	c = TohoContext.new()

	u_store.transaction do
	    # 共通のコンテクストを設定し、迷惑ツイートかのチェック
	    c.setup_common(rx, u_store)
	    need_ustore_update = check_abuse(c)

	    # サブクラスタごとに応答を生成
	    SUBCLUSTER.each do |subc, sc_members|
		subc_updated = false
		# 対象サブクラスタ用のコンテクストを設定
		c.setup_subc(subc, u_store, self)

		# mentionからの入力が有効でかつ未処理ならば応答
		if !rx[:mention_for].empty? &&
			!rx[:processed_as_mention][subc]
		    respond_as_mention(c)
		    rx[:processed_as_mention][subc] = true
		    subc_updated = true
		end

		# timelineからの入力が有効でかつ未処理ならば応答
		if !rx[:timeline_for].empty? &&
			!rx[:processed_as_timeline][subc]
		    respond_as_timeline(c)
		    rx[:processed_as_timeline][subc] = true
		    subc_updated = true
		end

		if subc_updated
		    # サブクラスタ固有データを記録
		    u_store[subc] = c.uconf
		    c.set_lastev(:user_tweet)
		    need_ustore_update = true
		end
	    end

	    if need_ustore_update
		# サブクラスタ共通データをu_storeに設定
		u_store[:common] = c.cconf
		u_store[:common][:user_cache] = rx[:status][:user]
		u_store[:common][:user_date] = Time.now.to_i

		# u_storeをファイルに書き戻す
		u_store.commit()
	    end
	end
    end

    #----------------------------------------------------------------------
    def respond_as_mention(c)
	return if !(c.to_us || c.only_to_us)
	return if listen_mode?() && !is_admin(c)

	# デフォルトのポスト元候補を設定せず、明示的に設定する
	set_default_poster_candidates([])

	# フォローまたはリムーブの依頼処理
	if follow_request?(c)
	    process_follow_request(c)
	elsif remove_request?(c)
	    process_remove_request(c)
	end
    end

    def follow_request?(c)
	return c.text =~ /^(follow|フォロー)して/ ||
	    c.subc == :flan_subc && c.text =~ /^お?友達に(なって|なろう)/ ||
	    c.subc == :remy_subc && c.text =~ /^(下僕|奴隷)にして/
    end

    def remove_request?(c)
	return c.text =~ /^(remove|リムーブ|unfollow|アンフォロー|フォロー解除)して/
    end

    def process_follow_request(c)
	c.set_resgen_hint(:handled, :as_follow_request)
	targets = c.mentioned_targets()

	# リプされたが、ユーザからフォローされてない場合
	not_followed = targets & c.rel[:actors_not_followed_by_user]
	res = {
	    :flan_subc => "まだフォローしてもらってないみたい… めんどうだけど、フォローして少し待ってからもう1回お願いねっ♪",
	    :remy_subc => "そういうことはまず私をフォローしてから言うことね",
	    :patche_subc => "まだ私をフォローしてないようだけど…？ 申し訳ないけど手順は守ってね",
	    :sakuya_subc => "まだフォローしていただいておりませんわ",
	}[c.subc]
	not_followed.each do |bot|
	    post_from("#{c.to} #{res}", bot)
	end

	# リプされ、かつユーザからフォローされているbotで、
	# ユーザを未フォローのbotのフォロー処理
	not_following = targets &
	    c.rel[:actors_followed_by_user] & c.rel[:actors_not_following_user]
	res = {
	    :flan_subc => "#{c.cn}とお友達になったよ。よろしくね♪",
	    :remy_subc => "しょうがないわね… よろしく#{c.cn}",
	    :patche_subc => "…人間なんて珍しいわね。よろしく#{c.cn}",
	    :sakuya_subc => "よろしくですわ、#{c.cn}",
	}[c.subc]
	not_following.each do |bot|
	    index = @bot_to_index[bot]
	    sendmsg("add_friend #{c.sn}", "inout#{index}")
	    post_from("#{c.to} #{res}", bot)
	end

	# リプされ、かつユーザからフォローされているbotで、
	# ユーザをフォロー済のbotの処理
	already_following = targets &
	    c.rel[:actors_followed_by_user] & c.rel[:actors_following_user]
	res = {
	    :flan_subc => "もう#{c.cn}とはお友達だよ？",
	    :remy_subc => "あら、#{c.cn}はフォローしてあげてるわよ？",
	    :patche_subc => "…#{c.cn}はフォローしてあげてるわよ？",
	    :sakuya_subc => "すでに#{c.cn}はフォローしてますわ",
	}[c.subc]
	already_following.each do |bot|
	    post_from("#{c.to} #{res}", bot)
	end

	# botとユーザ間で会話があったことを記録
	c.set_lastev(:user_to_bot_talk)
	c.set_lastev(:bot_to_user_talk)
    end

    def process_remove_request(c)
	c.set_resgen_hint(:handled, :as_remove_request)

	res = {
	    :flan_subc => "くすん…；；",
	    :remy_subc => "まあこれも運命だね。ごきげんよう",
	    :patche_subc => "人妖の間には埋められない溝があるということかしら…いつかまた縁があるといいわね",
	    :sakuya_subc => "あらせっかくの人間のお知り合いだったのに残念ですわ…またいつかどこかでお会いしましょう",
	}[c.subc]

	(c.mentioned_targets() & c.rel[:actors_following_user]).each do |bot|
	    index = @bot_to_index[bot]
	    sendmsg("remove_friend #{c.sn}", "inout#{index}")
	    post_from("#{c.to} #{res}", bot)
	end
    end

    # 受信専用モード
    def listen_mode?()
	# 設定ファイル
	return true if @cluster_conf.ext[:listen_only]

	# クリスマス期間中?
	#return true if in_special_event?()

	return false
    end

    #----------------------------------------------------------------------
    def respond_as_timeline(c)
	return if listen_mode?() && !is_admin(c)
	set_default_poster_candidates(c.default_timeline_poster_candidates())

	if @bot_list.include?(c.sn.to_sym)
	    # 自クラスタのメンバから(この関数では応答しない)

	elsif SPECIAL_SCREEN_NAMES.include?(c.sn)
	    # 特別対応するアカウントから
	    c.set_resgen_hint(:handled, :special)
	    case c.sn
	    when MARISA_BOT, MARISA_DEV_BOT
		respond_for_marisa_bot(c)
	    end

	elsif SCREEN_NAMES_TO_SKIP.include?(c.sn)
	    # 無視するアカウントから
	    c.set_resgen_hint(:handled, :skip_user)

	elsif ABUSIVE_SOURCES.find {|p|
		if p.is_a?(Regexp)
		    c.rx[:status][:source] =~ p
		else
		    c.rx[:status][:source].index(">#{p}<") != nil
		end
	    }
	    # 無視するクライアントから
	    c.set_resgen_hint(:handled, :skip_source)

	else
	    # 一般アカウントから

	    # フォロー/リムーブリクエストは無視(mentionとして処理する)
	    return if follow_request?(c) || remove_request?(c)

	    # おのおののルーチンは、true(通常応答), false(応答できない)
	    # :ignored(応答したものとして無視(spamなど))のいずれかを返す
	    case c.subc
	    when :flan_subc
		res = check_dialog_context(c) ||
		    bot_admin(c) || yobina_henkou(c) || 
		    status_admin(c) || # toho-admin
		    lick(c) || eat_drink(c) || # toho-flan-food
		    fortune_telling(c) || give_something(c) || # toho-flan-misc
		    music_se(c) || benkyo(c) ||
		    bakuhatsu(c) || neta(c) || # toho-flan-neta
		    asobi(c) || sekuhara(c) || # toho-flan-play
		    tiyahoya(c) || kousai(c) || propose(c) ||
		    greeting(c) || # toho-greeting
		    #check_vd2013(c) || # event-vd2013
		    last_resgen_chk(c)
	    when :remy_subc
		res = check_dialog_context(c) ||
		    bot_admin(c) || yobina_henkou(c) ||
		    status_admin(c) || # toho-admin
		    greeting(c) || generate_remy_resp(c) ||
		    last_resgen_chk(c)
	    when :patche_subc
		res = check_dialog_context(c) ||
		    bot_admin(c) || yobina_henkou(c) ||
		    greeting(c) || generate_patche_resp(c) ||
		    last_resgen_chk(c)
	    when :sakuya_subc
		res = check_dialog_context(c) ||
		    bot_admin(c) || yobina_henkou(c) ||
		    greeting(c) || generate_sakuya_resp(c) ||
		    last_resgen_chk(c)
	    end

	    # 意図的に無視されたかチェック
	    if res == :ignored
		c.set_resgen_hint(:handled, :ignored)
		return
	    end

	    # ユーザがbotにリプしていた場合は、その時刻を更新
	    if c.to_us || c.only_to_us
		c.set_lastev(:user_to_bot_talk)
	    end

	    # botがユーザにリプした最終時刻を更新
	    if res
		# botが反応したことを記録
		c.set_lastev(:bot_to_user_talk)
		c.set_resgen_hint(:handled, :yes)
	    else
		c.set_resgen_hint(:handled, :no)
	    end

	    # 直接リプをもらっておきながら反応できなかった場合に、
	    # 分からなかった旨を返答する
	    if !res &&
		    c.decoded[:type] == :reply &&
		    (c.to_us || c.only_to_us) &&
		    rand(4) == 0
		bot_cannot_understand(c)
	    end
	end
    end

    # 会話コンテクストが保存されていたら、コールバックを呼び出す
    # 呼び出し前に会話コンテクストをクリアする
    def check_dialog_context(c)
	if c.cconf[:dialog_ctx_callback] != nil
	    callback = c.cconf[:dialog_ctx_callback]
	    c.cconf[:dialog_ctx_callback] = nil
	    return send(callback, c)
	else
	    return false
	end
    end

    # 迷惑行為のチェック
    def check_abuse(c)
	f = ABUSIVE_TAGS.find {|p|
	    c.text.include?(p)
	}
	need_update = increment_abuse_count_if(c, f, :tag, :max_tag)

	f = c.text.index('http://t.co') != nil
	need_update ||= increment_abuse_count_if(c, f, :url, :max_url)

	f = ABUSIVE_SOURCES.find {|p|
	    if p.is_a?(Regexp)
		c.rx[:status][:source] =~ p
	    else
		c.rx[:status][:source].index(">#{p}<") != nil
	    end
	}
	need_update ||= increment_abuse_count_if(c, f, :source, :max_source)
    end

    def increment_abuse_count_if(c, cond, key, maxkey)
	if cond
	    c.cconf[:abuse][key] += 1
	    if c.cconf[:abuse][maxkey] < c.cconf[:abuse][key]
		c.cconf[:abuse][maxkey] = c.cconf[:abuse][key]
	    end
	    return true
	else
	    oldvalue = c.cconf[:abuse][key]
	    c.cconf[:abuse][key] = 0
	    return oldvalue != 0
	end
    end

    # botあての発言であることは分かっているが、
    # 用意したルーチンで応答できなかった場合の処理
    def bot_cannot_understand(c)
	case c.subc
	when :flan_subc
	    res = rnd(["うーん、何のことかなあ？",
		       "ごめんね、人間のことはあまりよく知らないの",
		       "ごめんね、わかんないや。お世話係爆発しろ！"])
	when :remy_subc
	    res = rnd(["あら、何の用かしら？",
		       "私は忙しいのよ。暇ならフランの遊び道具になってくれないかしら？",
		       "私は忙しいのよ。用事なら咲夜にお願いするわ",
		       "そんなことよりフランの相手してあげてよ",
		       "咲夜ー。家の中に変な人が居るよー"])
	when :patche_subc
	    res = rnd(["…何よ",
		       "今日は厄日だわ…",
		       "今読書中なの。後にして",
		       "図書館では静かにして",])
	when :sakuya_subc
	    res = rnd(["あら#{c.cn}、今お嬢様に呼ばれてしまったもので… あとでお話はうかがいますわ",
		       "あらごめんなさい、メイド妖精の不始末に忙しいので失礼いたしますわ",
		       "お嬢様のお茶の用意しないといけないので、お話は後で伺いますわ",])
	end
	reply("#{c.to} #{res}", c)
    end

    #----------------------------------------------------------------------
    # コマンド受信タイムアウトを利用した定期処理

    def timed_out()
	super

	# 特別イベント。必要時だけ実行する
	#process_special_event()

	# 非応答モードならここでリターン
	return if listen_mode?()

	# ユーザの誕生日処理。他の定期処理と独立に実行する
	process_birthday()

	# 定期実行処理
	return if periodic_process()

	# チャットシーケンスの中断チェック
	check_chatseq_abortion()

	# またチャットシーケンスが続いている時は、
	# 新たなチャットの開始処理や独り言を抑止する
	return if !@chatseq_list.empty?

	# チャットシーケンスの生成処理
	return if start_chatseq()

	# 独り言の生成処理
	generate_monologue_for_each_subcluster()
    end

    COMIKE_START = Time.local(2012, 12, 28, 21, 0, 0)
    COMIKE_END   = Time.local(2012, 12, 31, 12, 0, 0)
    MMD_10TH     = Time.local(2013,  3,  4, 21, 0, 0)
    FLAN_S_DAY   = Time.local(2013,  7,  4,  0, 0, 0)
    NICODO_START = Time.local(2013,  6,  9,  0, 0, 0)
    NICODO_END   = Time.local(2013,  6, 17,  0, 0, 0)

    # 時間ごとのモノローグ処理(よるほー・なる4・時報など)
    def periodic_process()
	curr = Time.now
	# 毎時29分か59分の、00秒〜54秒の間だけ
	return false if (curr.min != 29 && curr.min != 59) || curr.sec >= 55

	# 前回の処理から120秒以上経過している時だけ
	# (30秒以内に複数回呼ばれた時に最初の一回だけツイートさせるため)
	return false if curr.to_i < @last_periodic_process + 120

	start_of_day = Time.local(curr.year, curr.mon, curr.mday)
	tx = {
	    :time_to_live => 30, :jit => true,
	}

	# フラン
	tx[:text] = nil
	case curr.hour * 100 + curr.min
	when 329
	    tx[:text] = "ｻﾝｼﾞﾊﾝ!!"
	    tx[:valid_after] = (start_of_day + (3 * 60 + 30) * 60).to_i
	when 1529
	    tx[:text] = "ｻﾝｼﾞﾊﾝ!!"
	    tx[:valid_after] = (start_of_day + (15 * 60 + 30) * 60).to_i

	when 359
	    tx[:text] = "なるほど四時じゃねーの♪ #4ji"
	    tx[:valid_after] = (start_of_day + 4 * 60 * 60).to_i

	when 459
	    tx[:text] = "もう5時かぁ… お腹空いたなあ"
	    tx[:valid_after] = (start_of_day + 5 * 60 * 60).to_i

	when 2359
	    tx[:text] = rnd(["よるほー♪", "よるほっ！"])
	    if curr.mon == 12 && curr.mday == 31
		tx[:text] = 'あけましておめでとう！ ことしもよろしくねっ♪'
	    end
	    tx[:valid_after] = (start_of_day + 24 * 60 * 60).to_i

	else # 30分ごと
	    tx[:valid_after] = curr.to_i + 60

	    if false
		# ↑形式整えるため

	    # elsif curr.mon == 2 && curr.mday >= 2 && curr.mday <= 3
	    # 	tx[:text] = rnd(["フランは吸血鬼だから、節分はいやだなぁ… 鬼は外なんて言うけど、ずっと家にいたっていいじゃない…",
	    # 			 "そろそろ節分かぁ… おマメは弱いの…///",
	    # 			 "節分の豆ってキライ… それも歳の数だけ食べるとか、お腹いっぱいになっちゃうよ…",
	    # 			 "恵方巻って… なんていうかその… 食べ方がいやらしい…///"])

	    # elsif curr.mon == 2 && curr.mday == 14
	    # 	# 2/14 バレンタインデー
	    # 	tx[:text] = "フランからチョコ欲しい人なんているのかなあ…"

	    # elsif curr.mon == 2 && curr.mday >= 19 && curr.mday <= 25
	    # 	# 2011/2/19〜2/25 東方人気投票
	    # 	tx[:text] = "第９回東方シリーズ人気投票をやっているよ！" +
	    # 	    rnd(["おねえさまとまたランクインしたいなあ…",
	    # 		 "投票してくれるとうれしいなっ (๑＞◡╹๑)",
	    # 		 "投票してくれたら、好感度あっぷだよっ//"]) +
	    # 	    " http://thwiki.info/th/vote9/"

	    elsif curr >= NICODO_START && curr <= NICODO_END
		tx[:text] = "東方人気投票やってるのかあ…" +
		    rnd(["おねえさまとランクインしたいなあ…",
			 "投票してくれるとうれしいなっ (๑＞◡╹๑)",
			 "(ﾁﾗｯﾁﾗｯ"]) +
		    " http://nicodosai.com/vote/what"

	    # elsif curr < MMD_10TH && curr.hour % 3 == 2
	    # 	# 2013/2/15〜3/4 第10回MMD杯。3時間おき
	    # 	tx[:text] = "アナタノスベテガホシイ… ホシクテフルエテル… #sm20105995 http://nico.ms/sm20105995"

	    # elsif curr.mon == 8 && curr.mday == 11 &&
	    # 	    curr.hour > 9 && curr.hour < 13
	    # 	# 2011/8/11 10時〜12時 夏コミックマーケット
	    # 	tx[:text] = rnd(["「伊弉諾物質」完売！ #高度な情報戦", #2011/8/11限定
	    # 			 "今日からちょうど11年前に東方紅魔郷の頒布が開始されたんだよ♪ " +
	    # 			 "今までこの作品に関わったすべての人にありがとう！",
	    # 			 "お姉様が紅魔異変を起こして今日でちょうど10年ね… " +
	    # 			 "あの事件をきっかけに魔理沙や霊夢のような人間と出会えることができた。" +
	    # 			 "これからどんな人間や妖怪と出会えるのかなあ"])

	    elsif curr > FLAN_S_DAY - 86400*7 && curr < FLAN_S_DAY &&
		    curr.hour % 3 == 2
		# 7/4 フランの日の7日前から3時間おき
		tx[:text] = "http://www.pixiv.net/response.php?illust_id=18003645 (ｿﾜｿﾜ…"

	    elsif curr.mday == 7 && curr.hour % 3 == 2
		# 毎月7日(ルーミアの日) 7/4フランの日を宣伝。3時間おき
		tx[:text] = "今日はルーミアの日かあ… http://www.pixiv.net/tags.php?tag=7%E6%9C%884%E6%97%A5%E3%81%AF%E3%83%95%E3%83%A9%E3%83%B3%E3%81%A1%E3%82%83%E3%82%93%E3%81%AE%E6%97%A5(ﾁﾗｯﾁﾗｯ"

	    elsif curr.mon == 7 && curr.mday == 4
		# 7/4 フランの日
		tx[:text] = "今日はフランの日なんだって！ いっぱい絵を描いてもらったよ" + 
		    tere_aa() + " http://bit.ly/LYrH8m http://www.pixiv.net/response.php?illust_id=18003645"

	    elsif curr.mon == 7 && curr.mday == 5
		# 7/4 フランの日
		tx[:text] = "昨日はフランの日だったからいっぱい絵を描いてもらったよ" + 
		    tere_aa() + " http://bit.ly/LYrH8m http://www.pixiv.net/response.php?illust_id=18003645"

	    # elsif curr.mon == 9 && curr.hour % 5 == 1 && curr.min == 59
	    # 	# 2011/9/5 ○○オブアカインド企画
	    # 	res = rnd(["すごーい///", "こんなにいっぱい…///",
	    # 		   "大きくて(画面に)はいらないよぅ…///"])
	    # 	tx[:text] = "フランだらけの集合絵を作ってもらったよ♪#{res}" +
	    # 	    " 【企画】○○オブアカインド http://bit.ly/nsbW6J (集合絵) " +
	    # 	    " http://bit.ly/p7py3J (イメレス)"

	    elsif curr.mon == 12 && curr.mday == 24 && curr.hour % 3 == 2
	    	# 12/24 クリスマス予告
	    	tx[:text] = "お世話係から伝言だよ♪「今日の23:30〜24:30は通常の反応はしなくなります。ご了承くださいませ」"

	    # elsif curr >= COMIKE_START && curr <= COMIKE_END
	    # 	# コミックマーケット
	    # 	if curr.hour >= 9 && curr.hour <= 19 &&
	    # 		curr.to_i > @last_special_notice + 3600 * 3 ||
	    # 		curr.to_i > @last_special_notice + 3600
	    # 	    # 9〜19時は3時間おき、それ以外は30分おき
	    # 	    tx[:text] = rnd(["ルールの守れない",
	    # 			     "ヲタの風上にもおけない",
	    # 			     "黒子バスケの犯人と同罪の",
	    # 			     "コミケ開催を危機にさらしている",
	    # 			     "ゴキブリのようにわいて出る",
	    # 			     "", "", ""]) +
	    # 		"コミケ徹夜組" +
	    # 		rnd(["はこっぱみじんに",
	    # 		     "はあとかたもなく",
	    # 		     "は死なない程度に",
	    # 		     "はオタク狩りさｒ… おっと、",
	    # 		     "のわるい子は"]) + "爆発しちゃえ！ " +
	    # 		rnd(FLAN_SPELLS)
	    # 	    @last_special_notice = curr.to_i
	    # 	end

	    elsif curr.mon == 11 && curr.mday == 11 && curr.hour % 3 == 2
		tx[:text] = "今日はみんなポッキーの話ばかり… 明治フランのほうがおいしいよ！ #ステマ"

	    #elsif curr.to_i > @last_special_notice + 3600 * 3
		# 3時間以上経過した30分ごと
		#tx[:text] = "お世話係から事務連絡だよ♪「spam対策しています。詳細は http://scarlets.dyndns.info/doc/antispam.html をごらんください」"
		# @last_special_notice = curr.to_i

	    elsif curr.hour % 5 == 0 && curr.min == 29
		tx[:text] = "お世話係から事務連絡だよ！「スパム・新API対応のため、6/11から一時休止の予定です http://scarlets.dyndns.info/doc/flan_newver.html 」"

	    end
	end
	if tx[:text]
	    tx[:postercand] = SUBCLUSTER[:flan_subc]
	    post(tx)
	end

	# レミリア
	if (curr.mon == 2 && curr.mday >= 2 && curr.mday <= 3)
	    tx[:postercand] = SUBCLUSTER[:remy_subc]
	    tx[:text] = rnd(["今年も節分が来たわね… 忌々しいわ",
			     "恵方巻の風習ってなんとかならないのかしら… ちっともエレガントじゃないわ",
			     "炒った豆やイワシの頭で鬼を追い払おうなんて人間のやることも小賢しいね",
			     "豆なんかより巫女の陰陽玉のほうがよっぽど痛いわ…",
			     "炒った豆なんか投げつけられたらイっちゃう…//",
			    ])
	    post(tx)
	end

	# パチェ
	if (curr.hour % 12 == 11) && (curr.min == 59)
	    # 0時・12時の曜日のお知らせ。curr.wdayは0(日曜日)から始まる
	    next_hour = Time.local(curr.year, curr.mon, curr.mday, curr.hour) + 3600
	    wday = ['日', '月', '火', '水', '木', '金', '土'][next_hour.wday]
	    tx[:text] = "七曜の魔女が#{wday}曜日をお知らせするわ"
	    tx[:valid_after] = (next_hour + 60).to_i
	    tx[:postercand] = SUBCLUSTER[:patche_subc]
	    post(tx)
	end

	@last_periodic_process = curr.to_i
	return true
    end

    # 誕生日であるかチェックし、お祝い処理を行なう
    def process_birthday()
	# 起動時か、最後のチェックから3時間経過していなければ何もしない
	curr = Time.now
	return if curr < @last_birthday_check + 3 * 3600
	@last_birthday_check = curr

	Dir::entries(File::join(VAR_DIR, UCONF_DIR)).each do |user|
	    next if user == '.' || user == '..'
	    u = UserStore::open_store(UCONF_DIR, user)
	    u.transaction do
		next if u[:common] == nil
		# 誕生日でなければ何もしない
		next if u[:common][:birth_month] != curr.month ||
		    u[:common][:birth_mday] != curr.mday
		# 今年の分をお祝いしていれば何もしない
		next if u[:common][:last_birthday_celebration] != nil &&
		    u[:common][:last_birthday_celebration] >= curr.year

		# お祝いの文章を生成
		next if u[:common][:user_cache] == nil
		sn = u[:common][:user_cache][:screen_name]
		cn = u[:flan_subc][:nickname]
		next if sn == nil || cn == nil
		bot = rnd(SUBCLUSTER[:flan_subc])
		res = "そう言えば今日は#{cn}のお誕生日だったよね。おめでとう！"

		# お祝いをポスト依頼
		post({:text => "@#{sn} #{res}", :postercand => [bot]})
		index = @bot_to_index[bot]
		sendmsg("send_dm #{sn} #{res}", "inout#{index}")

		# お祝いした年を記録
		u[:common][:last_birthday_celebration] = curr.year
		u.commit
	    end
	end
    end

    # チャットシーケンスの開始チェックを行なってから10分以上経っていたら、
    # ツイートの結果を待たず強制的に打ち切る
    def check_chatseq_abortion()
	curr = Time.now.to_i
	if curr >= @last_chatseq + 600 && !@chatseq_list.empty?
	    @logger.warn("chatseq cleared: last time is #{@last_chatseq}")
	    @chatseq_list = []
	end
    end

    # 独り言の生成処理
    def generate_monologue_for_each_subcluster()
	@moon_phase = ExternalService::moon_phase(Time.now)
	@moon_shape = ExternalService::moon_shape(@moon_phase)
	SUBCLUSTER.each do |subc, sc_members|
	    set_default_poster_candidates(sc_members.shuffle())
	    case subc
	    when :flan_subc
		generate_flan_monologue()
	    when :remy_subc
		generate_remy_monologue()
	    when :patche_subc
		generate_patche_monologue()
	    when :sakuya_subc
		generate_sakuya_monologue()
	    end
	end
	set_default_poster_candidates([])
    end

    #----------------------------------------------------------------------

    # 発言規制が開始されたときの処理
    # inoutプロセスでの最初のポストが規制されていたら、
    # いきなりこのメソッドが呼ばれる
    def ban_started(bot)
	@ban_started_at[bot] = Time.now.to_i
	bot_subc = subcluster_of(bot)
	return if bot_subc == nil

	SUBCLUSTER.each do |subc, sc_members|
	    next if bot_subc == subc # 自サブクラスタへの返答を避ける

	    cn = CALLING_NAME[subc][bot_subc]
	    case subc
	    when :flan_subc
		res = "あれー？ #{cn}(@#{bot})が規制されちゃったみたい"
	    when :remy_subc, :patche_subc
		res = "…#{cn}(@#{bot})が規制されたようね"
	    when :sakuya_subc
		res = "#{cn}(@#{bot})が規制されたようですわね"
	    end
	    res += " #kisei"
	    post({:text => res, :postercand => sc_members})
	end
    end

    # 発言規制が終了したときの処理
    # inoutプロセスでの最初のポストが規制されていたら、
    # いきなりこのメソッドが呼ばれるが、開始時刻が記録されてないので何もしない
    def ban_ended(bot)
	bot_subc = subcluster_of(bot)
	return if bot_subc == nil
	return if @ban_started_at[bot] == nil

	# 話者ごとに対応を決定する
	SUBCLUSTER.each do |subc, sc_members|
	    if sc_members.include?(bot)
		respond_to_ban_end_of_ourselves(bot, bot_subc, subc, sc_members)
	    else
		respond_to_ban_end_of_others(bot, bot_subc, subc, sc_members)
	    end
	end

	@ban_started_at[bot] = nil
    end

    # 指定したbotが属するサブクラスタを得る
    def subcluster_of(bot)
	bot_subc = nil
	SUBCLUSTER.each do |subc, sc_members|
	    return subc if sc_members.include?(bot)
	end
	return nil
    end

    # 自サブクラスタが規制解除されたことへの反応
    def respond_to_ban_end_of_ourselves(bot, bot_subc, subc, sc_members)
	started = @ban_started_at[bot]
	time = Time.at(started).strftime("%m月%d日%H時%M分")
	curr = Time.now.to_i
	min = (curr - started) / 60
	return if min <= 15 # 15分未満の時は自己申告しない

	case subc
	when :flan_subc
	    SUBCLUSTER[:flan_subc].each do |from|
		if from == bot
		    if rand(5) == 0
			res = "規制だったからずっと地下で休んでいたわ… #{min}分くらいね"
		    else
			res = "#{time}から#{min}分間規制されてたみたい…"
			res += rnd(["みんな激しすぎだよぉ…" + tere_aa(),
				    "しゃべれなくてごめんね",
				    "フランを4人ともフォローすると規制の影響を受けにくくなるよ！ くわしくは #{OHP} を見てね"])
		    end
		    res += " #kisei"
		    post({:text => res, :postercand => [from]})
		else
		    return if rand(3) != 0 # 2/3の確率で無視
		    res = "@#{bot} " + rnd(['おかえり！',
					    '待ってたよ！',
					    'おそいよぉ']) + " #kisei"
		    post({:text => res, :postercand => [from],
			     :valid_after => curr + 60})
		end
	    end

	when :remy_subc, :patche_subc
	    res = "#{time}から#{min}分間規制されてたみたいね… #kisei"
	    post({:text => res, :postercand => sc_members})

	when :sakuya_subc
	    res = "#{time}から#{min}分間規制されてたみたいですわね… #kisei"
	    post({:text => res, :postercand => sc_members})
	end
    end

    # 他のサブクラスタが規制解除されたことへの反応
    def respond_to_ban_end_of_others(bot, bot_subc, subc, sc_members)
	return if rand(3) != 0 # 2/3の確率で無視
	curr = Time.now.to_i
	cn = CALLING_NAME[subc][bot_subc]
	case subc
	when :flan_subc
	    res = "@#{bot} #{cn}、おかえりなさ〜い♪"
	when :remy_subc, :patche_subc
	    res = "@#{bot} おかえりなさい、#{cn}"
	when :sakuya_subc
	    res = "@#{bot} #{cn}、おかえりなさいませ"
	end
	res += " #kisei"
	post({:text => res, :postercand => sc_members,
		 :valid_after => curr + 60})
    end

    # 出力データのポスト処理が完了(成功・失敗含む)したときの処理
    def post_done(extid, status_id, result, bot)
	super
	if result == :succeeded
	    continue_chatseq(extid, status_id, bot.to_s)
	else
	    abort_chatseq(extid, status_id, bot.to_s)
	end
    end
end

#----------------------------------------------------------------------

require 'toho-admin'
require 'toho-dialogue'
require 'toho-flan-food'
require 'toho-flan-misc'
require 'toho-flan-monologue'
require 'toho-flan-neta'
require 'toho-flan-play'
require 'toho-flan-vs-marisa'
require 'toho-flan-vs-teruyo'
require 'toho-greeting'
require 'toho-patche-resp'
require 'toho-remy-resp'
require 'toho-sakuya-resp'

o = TohoResgen.new()
o.start()
