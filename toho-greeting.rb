# -*- coding: utf-8 -*-

class TohoResgen < ResgenProcess
    # 挨拶処理
    # - 時刻の挨拶などは特に自分あてでなくても応答する
    # - しかし、他の人に同じ挨拶を繰り返す場合があるので、
    #   同じ挨拶を短時間で繰り返さないようにする
    # - 挨拶と同じ字句が含まれる挨拶返しがある
    #   (「おやすみ」に対して「おやすみあり」など)ため、
    #   挨拶返しと感謝の挨拶がかぶるおそれがある(「おやすみありがとう」など)
    # - 挨拶返しには応答しない(「おやあり」など)

    # 2011/3/26 AC挨拶追加 -> 9/11 削除

    def greeting(c)
	case c.subc
	when :flan_subc
	    return greeting1_flan(c) || greeting2_flan(c) ||
		greeting3_flan(c) || greeting4_flan(c)
	when :remy_subc
	    return greeting1_remy(c) || greeting2_remy(c)
	when :patche_subc
	    return greeting1_patche(c) || greeting2_patche(c)
	when :sakuya_subc
	    return greeting1_sakuya(c) || greeting2_sakuya(c)
	end
    end

    #----------------------------------------------------------------------
    # 挨拶1 (時刻)
    # 他ユーザー向けの発言には応答しない
    # おはようとおやすみは頻度高いので、できるだけバリエーション増やしていく

    GREET_MORNING = /おはよ(う|ー)?|ｵﾊﾖ(ｳ|ｰ)?|お早う|おはっす|むくり|mkr|ｍｋｒ|えむけーあーる/
    GREET_DAY = /(こんにち|こんち|こにゃにゃち)(は|わ)|こんちゃ/
    GREET_EVENING = /(こんばん(は|わ|にゃ)|こばにゃ)/
    GREET_NIGHT = /(おやすみ|お休み|ｵﾔｽﾐ)/

    def greeting1_flan(c)
	return false if !(c.to_all || c.to_us || c.only_to_us)
	return false if c.text =~ /あり(がと|です|でした)|あり$/
	return false if c.text =~ /^【口ぐせ分析】.*#ymnn/

	curr = Time.now.to_i
	return false if !(c.to_us || c.only_to_us) &&
	    curr > c.get_lastev(:user_to_bot_talk) + 14 * 86400

	case c.text
	when GREET_MORNING
	    return true if curr < c.get_lastev(:greet_morning) + 600
	    c.set_lastev(:greet_morning, curr)

	    res = rnd(["おはよう、#{c.cn}！", "おはよう、#{c.cn}♪",
		       "#{c.cn}、おはよう！", "#{c.cn}、おはよう♪",
		       "#{c.cn}、おはよっ！"," #{c.cn}、おはよっ♪",
		       "おはよう…まだ眠いよぉ",
		       "んゅ… #{c.cn}おはよぉ…",
		       "むにゅ… おはよぉ#{c.cn}…",
		       "おはようじょ♪",
		       "おはようむ♪",
		       "おはようどんげ！",
		       "おはよう、#{c.cn}！フランはこれから寝るところだよ///"])
	    reply("#{c.to} #{res}", c)
	    c.uconf[:base_affection] += dice(1, 6)
	    return true

	when GREET_DAY
	    return true if curr < c.get_lastev(:greet_day) + 600
	    c.set_lastev(:greet_day, curr)

	    case rand(4)
	    when 0
		res = "ろりこんにちは！"
	    else
		res = "こんにちは、#{c.cn}！"
	    end
	    reply("#{c.to} #{res}", c)
	    c.uconf[:base_affection] += dice(1, 6)
	    return true

	when GREET_EVENING
	    return true if curr < c.get_lastev(:greet_evening) + 600
	    c.set_lastev(:greet_evening, curr)

	    case c.moon_shape
	    when :new_moon
		c.uconf[:base_affection] += dice(2, 6)
		res = "こんばんは。今日は月が冥い夜だね"

	    when :full_moon
		c.uconf[:base_affection] += dice(1, 4)
		case rand(5)
		when 0
		    res = "こんばんは…満月が近くてなんだか落ち着かないね"
		when 1
		    res = "こんばんは…今日はなんだか熱っぽいかも"
		when 2
		    res = "N'ak kode tihs ot tuyg... あ、こんばんは！気がつかなくてごめんね"
		else
		    res = "こんばんは。そろそろ満月かな"
		end

	    when :izayoi
		c.uconf[:base_affection] += dice(1, 6)
		res = "こんばんは。今晩は十六夜かな。咲夜のことじゃないよ？"

	    else # その他
		c.uconf[:base_affection] += dice(1, 6)
		case rand(4)
		when 0
		    res = "ろりこんばんは！"
		else
		    res = "こんばんは、#{c.cn}！"
		end
	    end
	    reply("#{c.to} #{res}", c)
	    return true

	when GREET_NIGHT
	    return true if curr < c.get_lastev(:greet_night) + 600
	    c.set_lastev(:greet_night, curr)

	    res = rnd(["また明日ね、#{c.cn}！",
		       "#{c.cn}、また明日ね♪",
		       "#{c.cn}、いい夢見てね！",
		       "#{c.cn}、またあとで遊ぼうね！おやすみ！",
		       "…いっしょに寝ていい？…なーんてウソだよ☆",
		       "うぅ、さびしいなあ…おやすみなさい！",
		       "えーっ、夜はこれからなのに… おやすみなさい！",
		       "おやすみなさい、#{c.cn}！",
		       "おやすみなさい、#{c.cn}♪"])

	    # 好感度が高い場合は、1/10の確率で反応上書き
	    if c.affection > 70 && c.tsundere > 0 && rand(10) == 0
		tere = tere_aa()
		res = rnd(["おやすみのちゅー…してほしいな#{tere}",
			   "いっしょに寝ようよ…#{tere}",
			   "いっしょに寝てほしいな…#{tere}",
			   "ひとりじゃさみしいな…#{tere}",
			   #【ロリ度UP】フランといっしょにすりーぴんぐをアテレコ【してみた】
			   "いっしょにすりーぴんぐしてほしいな#{tere} http://www.nicovideo.jp/watch/sm9443005",
])
	    end

	    reply("#{c.to} #{res}", c)
	    c.uconf[:base_affection] += dice(1, 6)
	    return true
	end

	return false
    end

    def greeting1_remy(c)
	hash = {
	    :greet_morning => [
			       "あら、私はもう寝るわよ？",
			       "こんな時間に起きてくるなんて人間は怠惰ね",
			       "おはよう、#{c.cn}。眠いわね…",
			       "おはよう、#{c.cn}。私はそろそろ寝ようかしら",
			      ],
	    :greet_day=> [
			  "#{c.cn}、ごきげんよう",
			 ],
	    :greet_evening=> [
			      "こんばんは、#{c.cn}",
			     ],
	    :greet_night=> [
			    "あら、夜はこれからよ？ しかたないわね… おやすみ",
			    "ずいぶん早いのね… おやすみ#{c.cn}",
			   ],
	}
	return generic_greet(c, hash)
    end

    def greeting1_patche(c)
	hash = {
	    :greet_morning=> [
			      "あらおはよう、#{c.cn}",
			      "早いわね。おはよう、#{c.cn}",
			     ],
	    :greet_day=> [
			  "ああ、もうお昼なのね。こんにちは、#{c.cn}",
			  "…こんにちは、#{c.cn}",
			 ],
	    :greet_evening=> [
			      "こんばんは、#{c.cn}。まあ、ここはいつも暗くて夜みたいなものなんだけどね",
			      "こんばんは、#{c.cn}。",
			     ],
	    :greet_night=> [
			    "おやすみ、#{c.cn}",
			    "おやすみ、#{c.cn}。良い夢を",
			   ],
	}
	return generic_greet(c, hash)
    end

    def greeting1_sakuya(c)
	hash = {
	    :greet_morning=> ["おはようございます、#{c.cn}"],
	    :greet_day=> ["こんにちは、#{c.cn}"],
	    :greet_evening=> ["こんばんは、#{c.cn}"],
	    :greet_night=> ["おやすみなさいませ、#{c.cn}"],
	}
	return generic_greet(c, hash)
    end

    #----------------------------------------------------------------------
    # 挨拶2 (出発/帰宅/風呂/食事)
    # 他ユーザー向けの発言には応答しない

    MEAL1_STR = '((食事|食べ|たべ|飯)(に)?(行|い)(っ)?て|食事して|食べて|たべて|飯って|メシって|食って)'
    MEAL2_STR = '(飯|めし|メシ)'
    GREET_MEAL_DONE = /(#{MEAL1_STR}(きた|きました)|#{MEAL2_STR}った)(?!ら)/o
    GREET_MEAL_WILL = /#{MEAL1_STR}(くる|きま)|#{MEAL2_STR}る/o

    BATH1_STR = '((風呂|ふろ|フロ|シャワ(ー)?|ｼｬﾜ(ｰ)?|ほか)(って|(に)?(行|い)(っ)?て|(に)?(入|はい)って))'
    BATH2_STR = '(風呂|ふろ|フロ|シャワ(ー)?|ｼｬﾜ(ｰ)?|ほか)'
    GREET_BATH_TOOK = /(#{BATH1_STR}(きた|きました)|#{BATH2_STR}った)(!?ら)|ほかいま/o
    GREET_BATH_WILL = /#{BATH1_STR}(くる|きま)|#{BATH2_STR}る/o

    GREET_VISIT = /(い|行|逝)(っ)?てきま(あ|っ|ー|〜|～)*す/

    HOME_STR = '(帰宅|きたく)'
    GREET_HOME = /ただい(ま|も)|たらい(ま|も)|#{HOME_STR}(した|しました|った)(!?ら)|#{HOME_STR}(〜|$)/
    # memo: wktkにはマッチしないがktkにマッチする正規表現は (?<!wktk)ktk
    # しかし、Ruby1.8では否定後読みがないため使えない

    def greeting2_flan(c)
	return false if !(c.to_all || c.to_us || c.only_to_us)
	return false if c.text =~ /あり(がと|です|でした)|あり$/
	return false if c.text =~ /^【口ぐせ分析】.*#ymnn/

	curr = Time.now.to_i
	return false if !(c.to_us || c.only_to_us) &&
	    curr > c.get_lastev(:user_to_bot_talk) + 14 * 86400

	# 食事
	case c.text
	when GREET_MEAL_DONE
	    return true if curr < c.get_lastev(:greet_meal_done) + 600
	    c.set_lastev(:greet_meal_done, curr)

	    res = rnd(["#{c.cn}、おかえりなさ〜い"])
	    reply("#{c.to} #{res}", c)
	    c.uconf[:base_affection] += dice(1, 6)
	    return true

	when GREET_MEAL_WILL
	    return true if curr < c.get_lastev(:greet_meal_will) + 600
	    c.set_lastev(:greet_meal_will, curr)

	    res = rnd(["もう食事の時間かあ。フランもお腹すいた…",
		       "もう食事の時間かあ。#{c.cn}、いってらっしゃい！",
		       "もう食事の時間かあ。咲夜に何か作ってもらおうかな",
		       "ちょっと待って！#{c.cn}はフランが食べるんだから行っちゃダメ！"])
	    reply("#{c.to} #{res}", c)
	    c.uconf[:base_affection] += dice(1, 6)
	    return true

	    # 風呂・シャワー
	when GREET_BATH_TOOK
	    return true if curr < c.get_lastev(:greet_bath_took) + 600
	    c.set_lastev(:greet_bath_took, curr)

	    res = rnd(['おかえり♪', 'おかえり〜♪',
		       'ほかえり♪', 'ほかえり〜♪'])
	    reply("#{c.to} #{res}", c)
	    c.uconf[:base_affection] += dice(1, 6)
	    return true

	when GREET_BATH_WILL
	    return true if curr < c.get_lastev(:greet_bath_will) + 600
	    c.set_lastev(:greet_bath_will, curr)

	    res = rnd(["#{c.to} いってらっしゃーい",
		       "#{c.to} いってら〜♪",
		       "#{c.to} ほかってら♪",
		       "#{c.to} ほかてら〜♪",
		       "#{c.to} ●REC",
		      ])
	    reply(res, c)
	    c.uconf[:base_affection] += dice(1, 6)
	    return true

	    # 外出: 風呂や食事の挨拶と誤爆しやすいので後に判定する
	when GREET_VISIT
	    return true if curr < c.get_lastev(:greet_visit) + 600
	    c.set_lastev(:greet_visit, curr)

	    res = rnd(["#{c.cn}、いってらっしゃい！",
		       "#{c.cn}、いってらっしゃい！今日もがんばってね！",
		       "#{c.cn}、いってらっしゃい！帰ったら遊ぼうね！",
		       "#{c.cn}、いってらっしゃい！早く帰ってきてね！",])
	    reply("#{c.to} #{res}", c)
	    c.uconf[:base_affection] += dice(1, 6)
	    return true

	    # 帰宅(東方)
	when /ただい(魔理沙|まりさ|まりしゃ|マスター|ますたー|マーガトロイド|まーがとろいど)/
	    return true if curr < c.get_lastev(:greet_home) + 600
	    c.set_lastev(:greet_home, curr)

	    res = rnd(['おかえーりん',
		       'おかえりんのすけ',
		       'おかえりりーほわいと',
		       'おかえりぐる'])
	    if c.text =~ /(mogmog|mgmg|もぐもぐ|モグモグ|ﾓｸﾞﾓｸﾞ)/
		res += 'のドロワーズmogmog'
	    end
	    res += '！'

	    reply("#{c.to} #{c.cn}、#{res}", c)
	    return true

	    # 帰宅(一般)
	when GREET_HOME
	    return true if curr < c.get_lastev(:greet_home) + 600
	    c.set_lastev(:greet_home, curr)

	    tere = tere_aa()
	    if rand(10) == 0
		res = "弾幕にする？って、なんでそんなガッカリした顔してるの？"
		if c.affection > 70 && c.tsundere > 0 && rand(3) == 0
		    res = rnd(["フランにする？#{tere}",
			       "フランと遊ぶ？#{tere}"])
		end
		reply("#{c.to} おかえりなさい！ご飯にする？お風呂にする？それとも…", c)
		reply("#{c.to} #{res}", c)
	    else
		res = rnd(["#{c.cn}、おかえりなさい！",
			   "#{c.cn}、おかえりなさい！早く遊ぼ！",
			   "わーい、#{c.cn}おかえり！"])
		if c.affection > 70 && c.tsundere > 0 && rand(10) == 0
		    res = rnd(["#{c.cn}、おかえりなさい！さびしかったんだからね#{tere}",
			       "#{c.cn}、おかえりなさい！ずっと待ってたんだからねっ#{tere}"])
		end
		reply("#{c.to} #{res}", c)
	    end

	    c.uconf[:base_affection] += dice(1, 6)
	    return true
	end

	return false
    end

    def greeting2_remy(c)
	hash = {
	    :greet_visit => ["あら、いってらっしゃい"],
	    :greet_home => ["おかえり、#{c.cn}"],
	}
	return generic_greet(c, hash)
    end

    def greeting2_patche(c)
	hash = {
	    :greet_visit => ["ああ、いってらっしゃい"],
	    :greet_home => ["…おかえり、#{c.cn}"],
	}
	return generic_greet(c, hash)
    end

    def greeting2_sakuya(c)
	hash = {
	    :greet_meal_done => ["おかえりなさいませ"],
	    :greet_meal_will => ["お食事ですね。いってらっしゃいませ"],
	    :greet_bath_took => ["ほかえりなさい"],
	    :greet_bath_will => ["ほかてらです"],
	    :greet_visit => ["いってらっしゃい"],
	    :greet_home => ["おかえりなさい"],
	}
	return generic_greet(c, hash)
    end

    #----------------------------------------------------------------------
    # 挨拶3(時候の挨拶)
    # 明示的にbotに話しかける必要あり
    # 「今年もよろしく」はgreeting4()でもひっかかってしまうので、先に判定する
    MERRY_CRISTMAS_RE = /メリー?クリ|ﾒﾘｰ?ｸﾘ|[Mm]erry (Christ|christ|X'?)mas/
    HAPPY_NEW_YEAR_RE = /(あ|明|開)け(まして、?)?おめ|今年もよろ/

    def greeting3_flan(c)
	return false if !(c.to_us || c.only_to_us)

	curr = Time.now
	case c.text
	when MERRY_CRISTMAS_RE
	    if curr.month == 12 && curr.mday <= 25
		res = rnd(["うー… クリスマス爆発しろ！",
			   "フランが悪魔の妹だって知って言ってるの？＞＜"])
		reply("#{c.to} #{res}", c)
		return true
	    end

	when HAPPY_NEW_YEAR_RE
	    if curr.month == 1 && curr.mday >= 1 && curr.mday <= 7
		res = rnd(["あけましておめでとう！",
			   "あけましておめでとう！ところで「お年玉」っていうものをもらえるって聞いたんけど…(ｷﾗｷﾗ)"])
		reply("#{c.to} #{res}", c)
		return true
	    end
	end

	return false
    end

    #----------------------------------------------------------------------
    # 挨拶4(感謝・謝罪・よろしく)
    def greeting4_flan(c)
	return false if !(c.to_us || c.only_to_us)

	case c.text
	when /(おは(よう)?|おや(すみ)?|てら|おか(えり)?)(あり|サンクス|thank|thx)/i
	    # 挨拶返し: 応答をポストせず、以降の処理を無視する
	    return :ignored

	when /(ありがと|thank|thx)/i
	    # ありがとんを入れたいが、機種依存文字
	    if c.tsundere < 0
		res = "た、ただの気まぐれなんだからね！"
		res += tere_aa() if c.affection > 40
		reply("#{c.to} #{res}", c)
	    else
		reply("#{c.to} どういたしまして、#{c.cn}！", c)
	    end
	    c.uconf[:base_affection] += dice(2, 6)
	    return true

	when /ごめん|ゴメン|すいません/
	    if c.tsundere < -5
		res = rnd(["絶対に許早苗", "絶対に許さないよ"])
	    elsif c.tsundere < 5
		res = rnd(["しょうがないなあ", "まあいいけどね"])
	    else
		res = rnd(["え、気にしないでいいよ♪", "別にいいよ♪"])
	    end
	    reply("#{c.to} #{res}", c)
	    c.uconf[:base_affection] += dice(2, 6)
	    return true

	when /よろしく|よろぴく|よろぴこ/
	    reply("#{c.to} こちらこそよろしくね♪", c)
	    c.uconf[:base_affection] += dice(1, 12)
	    return true

	when /(お|御)(かえ|帰)り|ｵｶｴﾘ|(お|御)(つか|疲)れ|(ご|御)(くろう|苦労)|待ってた/
	    res = rnd(["ただいまっ♪",
		       "おかあり♪",
		       "遅くなっちゃってごめんね",
		       "おまたせ"])
	    reply("#{c.to} #{res}", c)
	    c.uconf[:base_affection] += dice(1, 12)
	    return true

	when /(お|御)(めでと|目出度)/
	    res = 'ありがとう！'
	    reply("#{c.to} #{res}", c)
	    c.uconf[:base_affection] += dice(1, 12)
	    return true
	end

	return false
    end

    #----------------------------------------------------------------------
    TAG_TO_REGEX = {
	:greet_morning => GREET_MORNING,
	:greet_day => GREET_DAY,
	:greet_evening => GREET_EVENING,
	:greet_night => GREET_NIGHT,

	:greet_meal_done => GREET_MEAL_DONE,
	:greet_meal_will => GREET_MEAL_WILL,
	:greet_bath_took => GREET_BATH_TOOK,
	:greet_bath_will => GREET_BATH_WILL,
	:greet_visit => GREET_VISIT,
	:greet_home => GREET_HOME,
    }

    def generic_greet(c, hash)
	return false if !(c.to_all || c.to_us || c.only_to_us)
	return false if c.text =~ /あり(がと|です|でした)|あり$/
	return false if c.text =~ /^【口ぐせ分析】.*\ymnn/

	# botを指名しておらず、かつユーザが長期間botに話しかけてない場合は
	# 挨拶しない(空気化対策)
	curr = Time.now.to_i
	return false if !(c.to_us || c.only_to_us) &&
	    curr > c.get_lastev(:user_to_bot_talk) + 3 * 86400

	hash.each do |tag, res_cond_list|
	    if c.text =~ TAG_TO_REGEX[tag]

		# 同じ挨拶が連続した場合は挨拶しない
		return true if curr < c.get_lastev(tag) + 600
		c.set_lastev(tag, curr)

		res = rnd(res_cond_list)
		reply("#{c.to} #{res}", c) if res != nil
		return true
	    end
	end
	return false
    end
end
