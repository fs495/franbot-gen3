# -*- coding: utf-8 -*-

module ExternalService
    # その他ユーティリティー(外部サービス)

    class Twitpic
	VERIFY_CRED = '/1/account/verify_credentials.json'
	PROVIDER_SITE = 'https://api.twitter.com'
	TWITPIC_API = 'http://api.twitpic.com/2/upload.xml'

	# twitpicに画像を投稿する。
	# 投稿失敗時に例外が発生することがある。
	def self.post(args)
	    # OAuth Echo用のデータを作成する
	    opt = {:site => PROVIDER_SITE}
	    consumer = OAuth::Consumer.new(args[:consumer_key],
					   args[:consumer_secret], opt)
	    acc_token = OAuth::AccessToken.new(consumer,
					       args[:access_token],
					       args[:access_token_secret])
	    req = consumer.create_signed_request(:get, VERIFY_CRED, acc_token)
	    auth = req['authorization'] +
		", " + "oauth_token=\"#{args[:access_token]}\"" +
		", " + "realm=\"#{PROVIDER_SITE}\""

	    # ファイルを読み込み、twitpicにアップロードを依頼する
	    hc = HTTPClient.new
	    File.open(args[:file], 'r') do |file|
		postdata = {
		    :key => args[:api_key],
		    :media => file,
		    :message => args[:message],
		}
		headers = {
		    'X-Auth-Service-Provider' => "#{PROVIDER_SITE}#{VERIFY_CRED}",
		    'X-Verify-Credentials-Authorization' => auth,
		}
		return hc.post(TWITPIC_API, postdata, headers)
	    end
	    raise "cannot post #{args[:file]}"
	end
    end

    # 診断メーカー(http://shindanmaker.com)
    class ShindanMaker
	SHINDAN_MAKER_URL = 'http://shindanmaker.com'

	# "診断メーカー"(http://shindanmaker.com)での診断を行なう
	# 診断結果が得られなかった場合はnilを返す。例外はraiseしない
	def self.get(id, user)
	    begin
		client = HTTPClient.new
		url = "#{SHINDAN_MAKER_URL}/#{id}"
		resp = client.post_content(url, {'u' => user, 'from' => ''})
		# post_content()はタイムアウト例外が発生する可能性あり

		if resp =~ %r(a href="https?://twitter.com/intent/tweet\?text=(.*?)&url=(.*?)\")
		    return URI.decode($1.gsub('+', ' ')) + ' ' + url
		end
	    rescue Timeout::Error, StandardError => ex
	    end
	    return nil
	end
    end

    # かなこん(http://t-rack.net/kanakon/)
    class Kanakon
	def self.register(kanalist)
	    kanalist.reverse_each do |kana|
		add_friend("Kanakon_#{kana}")
		sleep(2)
	    end
	end

	def self.unregister(kanalist)
	    kanalist.each do |kana|
		remove_friend("Kanakon_#{kana}")
	    end
	end
    end

    # 楽天ブックス
    class RakutenBooks
	# r["Header"]["Status"] => "Success"
	# r["Body"] => ["BooksBookSearch"]
	# r["Body"]["BooksBookSearch"] => ["hits", "pageCount", "Items", "carrier", "last", "page", "count", "first"]
	# r["Body"]["BooksBookSearch"]["Items"] => ["Item"]
	# r["Body"]["BooksBookSearch"]["Items"]["Item"] => Array of Hash
	# r["Body"]["BooksBookSearch"]["Items"]["Item"][0] => ["mediumImageUrl", "publisherName", "authorKana", "chirayomiUrl", "size", "seriesName", "itemPrice", "postageFlag", "isbn", "listPrice", "title", "seriesNameKana", "subTitle", "author", "titleKana", "contentsKana", "affiliateUrl", "reviewCount", "smallImageUrl", "availability", "subTitleKana", "largeImageUrl", "limitedFlag", "reviewAverage", "itemUrl", "contents", "salesDate", "booksGenreID", "discountRate", "itemCaption"]

	def self.query(q)
	    # 固定パラメータを追加
	    q[:operation] ||= 'BooksBookSearch'
	    q[:version] ||= '2011-01-27'

	    client = HTTPClient.new()
	    res = client.get('http://api.rakuten.co.jp/rws/3.0/json', q)
	    #puts res.content ###
	    if res.status == 200
		return JSON.parse(res.content)
	    end
	    return nil
	end
    end

    #----------------------------------------------------------------------
    # その他ユーティリティ

    # 乱数発生
    # dice(n, m)=m面ダイスをn回振る dice(m)=m面ダイスを1回振る
    def dice(a, b = 0)
	if b == 0
	    m = a; count = 1
	else
	    m = b; count = a
	end
	sum = 0
	count.times { sum += rand(m) }
	return sum + count
    end

    def rnd(array)
	return array[rand(array.size)]
    end

    # テレ顔のAAをランダムに返す
    def tere_aa()
	return rnd(['(///ω///)', '(//∇//)', '（*/∇＼*）', '(*ﾉωﾉ)',
		    '＞△＜', '(*／ω＼*)'])
    end

    # 月齢計算
    MOON_PHASE_OFFSETS = [0, 2, 0, 2, 2, 4, 5, 6, 7, 8, 9, 10]
    def self.moon_phase(day)
	# 出典: http://ja.wikipedia.org/wiki/%E6%9C%88%E9%BD%A2
	# もともと近似式であるが、さらに月齢と月相を同一視する
	# より正確には http://www1.odn.ne.jp/kentaurus/mooncal.htm
	# 0:朔, 2:三日月, 14:満月, 15:十六夜
	return (((day.year - 11) % 19) * 11 + 
		MOON_PHASE_OFFSETS[day.month - 1] + day.day) % 30
    end

    def self.moon_shape(mop)
	case mop
	when 0, 29
	    return :new_moon
	when 13, 14
	    return :full_moon
	when 15
	    return :izayoi
	else
	    return nil
	end
    end
end
