#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
require 'fileutils'

# ID番号を名前とするファイルを、サブディレクトリに配置しなおす
# varディレクトリ内のファイル整理に使う

if ARGV.size < 1
    STDERR.print "Error: dirsize needed\n"
    exit
end
dirsize = ARGV[0].to_i

Dir.entries('.').each do |file|
    if File::directory?(file)
	print "skipped #{file}: is directory\n"
	next
    end

    if file.length <= dirsize
	print "skipped #{file}: too short name\n"
	next
    end

    middir = file[0, file.length - dirsize]
    FileUtils::mkdir_p(middir)
    FileUtils::mv(file, middir)
end
