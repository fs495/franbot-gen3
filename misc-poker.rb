# -*- coding: utf-8 -*-

module Poker
    SUIT_MARKS = {
	:spade => '♠',
	:heart => '♡',
	:diamond => '◇',
	:club => '♣',
    }
    SUIT_DESCS = {
	:spade => 'スペード',
	:heart => 'ハート',
	:diamond => 'ダイヤ',
	:club => 'クラブ',
    }
    SUITS = SUIT_DESCS.keys

    RANK_MARKS = [
		  '2', '3', '4', '5', '6', '7', '8', '9', '10',
		  'J', 'Q', 'K', 'A'
		 ]
    RANK_DESCS = [
		  '2', '3', '4', '5', '6', '7', '8', '9', '10',
		  'ジャック', 'クイーン', 'キング', 'エース'
		 ]
    # ランクはこの順番で強い
    RANK_INDEX_OF_2 = 0
    RANK_INDEX_OF_10 = 8
    RANK_INDEX_OF_ACE = 12
    class Card
	attr_reader :suit # :spade, :heart, :diamond, :class
	attr_reader :rank # 0..12 (13種類)

	def initialize(suit, rank)
	    @suit, @rank = suit, rank
	end

	def to_s()
	    return "#{SUIT_MARKS[@suit]}#{RANK_MARKS[@rank]}"
	end

	# 強いカードが先にくるようにソート
	def self.sort(cards)
	    return cards.sort{|x,y| y.rank <=> x.rank}
	end

	# シャッフル済みのデックをCardオブジェクトのArrayとして返す
	def self.deck()
	    result = []
	    SUITS.each do |suit|
		(RANK_INDEX_OF_2 ... RANK_INDEX_OF_ACE).each do |rank|
		    result << Card.new(suit, rank)
		end
	    end
	    return result.shuffle()
	end
    end

    class PokerHand
	attr_reader :cards
	attr_reader :hands, :score, :desc

	def initialize(ph)
	    @cards = Card.sort(ph) # 強カードが先にくるようにソート済み
	    @buf = @cards.clone()
	end

	# 
	def eval()
	    highest = @cards[0].rank
	    straight = is_straight?()
	    flush = is_flush?()

	    if flush && straight
		@hands, @score = :straight_flush, straight
		@desc = "#{RANK_DESCS[straight]}のストレートフラッシュ"
		return
	    end

	    if r = get_n_of_a_kind(4)
		@hands, @score = :four_of_a_kind, r
		@desc = "#{RANK_DESCS[r]}のフォーオブアカインド"
		return
	    end
	    reset_buffer()

	    r3 = get_n_of_a_kind(3)
	    r2 = get_n_of_a_kind(2)
	    if r3 && r2
		@hands, @score = :full_house, r3 * 100 + r2
		@desc = "#{RANK_DESCS[r3]}が3枚と#{RANK_DESCS[r2]}が2枚のフルハウス"
		return
	    end
	    reset_buffer()

	    if flush
		@hands, @score = :flush, highest
		@desc = "#{RANK_MARKS[highest]}のフラッシュ"
		return
	    elsif is_straight?
		@hands, @score = :straight, straight
		@desc = "#{RANK_DESCS[straight]}のストレート"
		return
	    end

	    if r3 = get_n_of_a_kind(3)
		@hands, @score = :three_of_a_kind, r3
		@desc = "#{RANK_DESCS[r3]}のスリーオブアカインド"
		return
	    end
	    reset_buffer()

	    if r2 = get_n_of_a_kind(2)
		if r3 = get_n_of_a_kind(2)
		    @hands, @score = :two_pairs, r2 * 100 + r3
		    @desc = "#{RANK_DESCS[r2]}と#{RANK_DESCS[r3]}のツーペア"
		    return
		else
		    @hands, @score = :one_pair, r2
		    @desc = "#{RANK_DESCS[r2]}のワンペア"
		    return
		end
	    end

	    @hands, @score = :no_pair, @cards[0].rank * 10000 +
		@cards[1].rank * 1000 +
		@cards[2].rank * 100 +
		@cards[3].rank * 10 +
		@cards[4].rank
	    @desc = "ノーペア"
	end

	def to_s()
	    x = @cards.map{|card| card.to_s}
	    x << @hands
	    x << @score
	    x << @desc
	    return "[" + x.join(", ") + "]"
	end

	private

	# フラッシュであるか判定する
	def is_flush?()
	    SUITS.each do |suit|
		if @cards.all?{|c| c.suit == suit}
		    return true
		end
	    end
	    return false
	end

	# ストレートであるか判定し、その強度を返す。
	# ストレートでない場合はfalseを返す
	# (強) <------------------------------------------------> (弱)
	# "AKQJ10" "KQJ109" "QJ1098" ... "76543" "65432" "A5432"(特別)
	def is_straight?()
	    # 2〜5番目が連続してなければストレートではない
	    return false if @cards[1].rank != @cards[2].rank + 1 ||
		@cards[1].rank != @cards[3].rank + 2 ||
		@cards[1].rank != @cards[4].rank + 3

	    if @cards[0].rank == RANK_INDEX_OF_ACE &&
		    @cards[4].rank == RANK_INDEX_OF_2
		return -1
	    elsif @cards[0].rank == @cards[1].rank + 1
		return @cards[0].rank
	    else
		return false
	    end
	end

	# バッファにN-of-a-kind部分があれば取り除いてランクを返す
	# なければnilを返す
	def get_n_of_a_kind(n)
	    RANK_INDEX_OF_ACE.downto(RANK_INDEX_OF_2) do |rank|
		x = @buf.partition{|c| c.rank == rank}
		if x[0].size == n
		    @buf = x[1]
		    return rank
		end
	    end
	    return nil
	end

	# 判定用のバッファをリセットする
	def reset_buffer()
	    @buf = @cards.clone()
	end

    end
end

if $0 == __FILE__
    include Poker
    srand()

    100.times do
	deck = Card.deck()
	pokerhand = PokerHand.new(deck[0 .. 4])
	pokerhand.eval
	puts pokerhand.to_s
    end
end
