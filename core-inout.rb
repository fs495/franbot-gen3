# -*- coding: utf-8 -*-

require 'rubygems'
require 'rubytter'
require 'time'
require 'logger'
require 'socket'
require 'yaml/store'

require 'core-consts'
require 'core-subprocess'
require 'core-stores'

class InoutProcess < Subprocess
    include Commondef

    BOT_STATE_VERSION = 20100823
    INPUT_MAX_TTL = 120
    PURGE_INTERVAL = INPUT_MAX_TTL

    ACTION_CMD_TO_API = {
	'follow' => :follow,
	'unfollow' => :leave,
	'block' => :block,
	'unblock' => :unblock,
	'fav' => :favorite,
	'unfav' => :remove_favorite,
	'rt' => :retweet,
	'delete' => :remove_status,
    }

    attr_reader :sym

    def initialize(nr)
	super("inout#{nr}".to_sym) 
	open_logger(Logger::INFO)

	@cluster_config = Cluster::load_config()
	@sym = @cluster_config.bot_list[nr]

	@fstore = VarStore::open_store(FRIENDSHIP_STORE_FILE)
	@vstat = VolatileState.new()
	@last_api_exception = nil
    end

    def gstat_rwlock()
	@gstat_store.transaction do
	    yield @gstat_store[:bot_state]
	    @gstat_store.commit()
	end
    end

    def gstat_rdlock()
	@gstat_store.transaction(true) do
	    yield @gstat_store[:bot_state]
	end
    end

    #----------------------------------------------------------------------

    def prepare()
	connect_to_monitor(@cluster_config.port_number, 5)

	@logger.info("prepare: signing into twitter")
	sign_in_twitter()

	# friendship情報を取得
	@logger.info("prepare: updating friendship info")
	update_friendship_cache_immediately()
	sendmsg("friendship_updated", "resgen")

	@logger.info("prepare: purging old inputs")
	purge_old_input()

	request_resgen_for_pended_inputs()
    end

    # 定期実行処理
    # - timeline・mentionを取得し、入力ストアに格納する
    # - friendshipを取得し、ファイルキャッシュに格納する
    # - 古い入力データを捨てる
    def timed_out()
	# タイムアウト時、インターバルのチェックと各種取得処理を実施
	fetch_mention()
	fetch_timeline()
	update_friendship_cache()
	purge_old_input()
    end

    def read_socket(s)
	@logger.debug("received command: #{s}")

	case s
	when /req_post (.*)/
	    post_to_timeline($1)
	when /send_dm (.*?) (.*)/
	    send_dm($1, $2)

	when /add_friend (.*)/
	    add_user_as_friend($1)
	when /remove_friend (.*)/
	    remove_user_from_friend($1)

	when /(follow|unfollow|block|unblock|fav|unfav|rt|delete) (.*)/
	    direct_api($1, $2)

	when /quit_process/
	    @logger.info("quitting process")
	    close_monitor()
	    exit()
	when /reopen_logger/
	    reopen_logger()
	else
	    @logger.warn("inout: invalid command: #{s.chomp}")
	end
    end

    #----------------------------------------------------------------------
    # Twitterへのアクセス

    # Twitterにアクセスできるよう、各メンバを初期化する
    def sign_in_twitter()
	# bot設定を読み込む
	file = sprintf(BOT_CONFIG_FILE, @sym)
	gconf_store = ConfigStore::open_store(file)
	gconf_store.transaction(true) do
	    @gconf = gconf_store[:bot_config]
	    raise "#{file}: invalid bot config file" if @gconf == nil
	end

	# bot状態(保存用)
	file = sprintf(BOT_STATE_FILE, @sym)
	@gstat_store = VarStore::open_store(file)
	@gstat_store.transaction do
	    gstat = @gstat_store[:bot_state] || InoutState.new()
	    gstat.normalize()
	    @gstat_store[:bot_state] = gstat
	    @gstat_store.commit()
	end

	# Twitter APIハンドル(実体はRubytterインスタンス)
	oauth = Rubytter::OAuth.new(@gconf.consumer_key,
				    @gconf.consumer_secret)
	token = OAuth::AccessToken.new(oauth.create_consumer(),
				       @gconf.access_token,
				       @gconf.access_token_secret)
	@api_handle = OAuthRubytter.new(token)
    end

    API_MAX_RETRIES = 4
    API_RETRY_SLEEP = 2

    # Twitter APIとの低レベルインターフェース
    # リトライつきでAPIを呼び出す。
    # 呼び出しが失敗したときは例外を発生させずにnilを返す。
    # 例: ret = api(:update_status, args)
    def api(name, *args)
	retries = 0
	@last_api_exception = nil

	begin
	    # @api_handle.name(args)と同等のことをする
	    @api_handle.send(name, *args)
	rescue Timeout::Error, StandardError => ex
	    retries += 1
	    if ex.message =~ /^\d+: unexpected token/ && 
		    retries < API_MAX_RETRIES
		@logger.warn("api failed: retrying #{name}")
		sleep(API_RETRY_SLEEP)
		retry
	    end
	    @last_api_exception = ex
	    return nil
	end
    end

    RATE_LIMIT_EXPIRES = 120
    MIN_RATE_LIMIT = 5

    # rate_limitを取得し、残りAPI回数が十分か検査する。
    # - 前回取得済みの場合で、まだあまり時間が経っていないか
    #   今回取得できない場合はキャッシュで判定する
    # - 初めての取得試行で失敗した場合は例外を発生させる
    #
    # rate_limitが取得できたとき、残りAPI回数が十分であればそのオブジェクトを
    # 返し、そうでなければnilを返す
    def check_rate_limit()
	# キャッシュが入ってないか古すぎる場合は取得
	curr = Time.now.to_i
	if curr > @vstat.rate_limit_seen_at + RATE_LIMIT_EXPIRES
	    limit = api(:limit_status)

	    if limit != nil
		s = "#{limit.remaining_hits}/#{limit.hourly_limit}"
		@logger.debug("rate_limit updated: #{s}")
		@vstat.rate_limit_cache = limit
		@vstat.rate_limit_seen_at = curr
	    else
		# 取得失敗につき、古いキャッシュを再利用
		limit = @vstat.rate_limit_cache
	    end
	else
	    limit = @vstat.rate_limit_cache
	end

	# 一回も取得できていない場合
	if limit == nil
	    raise "cannot update rate_limit"
	end

	# 残りAPI回数を判定
	if limit.remaining_hits < MIN_RATE_LIMIT
	    @logger.warn("insufficient API count: #{limit.inspect} at #{Time.now.to_i}")
	    return nil
	end
	return limit
    end

    def parse_date(desc)
	begin
	    return Time.parse(desc)
	rescue ArgumentError
	    @logger.error("parse_date: invalid date format: #{desc}")
	    return Time.now
	end
    end

    def direct_api(action, arg)
	# 自クラスタ自身を対象にはできない。誤指定防止用
	return if @cluster_config.bot_list.include?(arg.to_sym)

	api = ACTION_CMD_TO_API[action]
	return if api == nil

	result = api(api, arg)
	status = result.nil? ? "failed" : "ok"
	@logger.info("direct_api: #{action} #{arg}, #{status}")
    end

    #----------------------------------------------------------------------
    # mention処理

    MENTION_FETCH_INTERVAL = 120

    # MentionからStatusをフェッチし、入力ストアに格納する。
    # Statusは非フォロワーのものも含むため、フォロー依頼処理するために使う。
    def fetch_mention()
	# 前回取得時刻・ID値を得る
	seen_at = last_id = nil
	gstat_rdlock do |gstat|
	    seen_at = gstat.mention_seen_at
	    last_id = gstat.mention_last_id
	end

	# rate_limitが取得できない、またはAPI回数が減っている場合はリターン
	limit = check_rate_limit()
	if limit == nil
	    @logger.warn("fetch_mention: cannot get rate_limit")
	    return 
	end

	# 前回の取得から一定時間経過してない場合は何もしないでリターン
	curr = Time.now.to_f
	if curr < seen_at + MENTION_FETCH_INTERVAL
	    @logger.debug("fetch_mention: skipped")
	    return 
	end
	@logger.debug("fetch_mention: started from id #{last_id}")

	# mentionを取得する
	opt = {
	    :count => 200,
	    :since_id => last_id,
	}
	mentions = api(:mentions, opt)
	if mentions == nil
	    @logger.warn("fetch_mention: failed to fetch mentions")
	    return
	end
	s = mentions.map{|status| status[:id]}.join(",")
	@logger.debug("fetch_mention: fetched: #{s}")
	msec = ((Time.now.to_f - curr) * 1000).to_i
	@logger.info("fetch_mention: #{mentions.size} #{msec}ms #{curr.to_i}")

	# 入力ストアに格納
	store_inputs(:mention, mentions)

	gstat_rwlock do |gstat|
	    # 最終IDと最終時刻を更新
	    mentions.each do |status|
		if gstat.mention_last_id <= status[:id]
		    gstat.mention_last_id = status[:id]
		end
	    end
	    gstat.mention_seen_at = curr.to_i
	end
	@logger.debug("fetch_mention: ended")
    end

    #----------------------------------------------------------------------
    # タイムライン取得

    TIMELINE_FETCH_INTERVAL = 20

    # TimelineからStatusをフェッチし、入力ストアに格納する。
    # Statusはbotがフォローするユーザのものしか含まれない
    def fetch_timeline()
	# 前回取得時刻・ID値を得る
	seen_at = last_id = nil
	gstat_rdlock do |gstat|
	    seen_at = gstat.timeline_seen_at
	    last_id = gstat.timeline_last_id
	end

	# rate_limitが取得できない、またはAPI回数が減っている場合はリターン
	limit = check_rate_limit()
	if limit == nil
	    @logger.warn("fetch_timeline: cannot get rate_limit")
	    return 
	end

	# 現在のrate_limitから取得インターバルを算出し、
	# 前回の取得時刻からその時間が経過してなければ何もしないでリターン
	curr = Time.now.to_i
	if curr < seen_at + TIMELINE_FETCH_INTERVAL
	    @logger.debug("fetch_timeline: skipped")
	    return
	end
	@logger.debug("fetch_timeline: started from id #{last_id}")

	# タイムラインを取得する
	opt = {
	    :count => 200,
	    :since_id => last_id,
	}
	timelines = api(:friends_timeline, opt)
	if timelines == nil
	    @logger.warn("fetch_timeline: failed to fetch timeline")
	    return
	end
	s = timelines.map{|status| status[:id]}.join(",")
	@logger.debug("fetch_timeline: fetched: #{s}")

	# 入力ストアに格納
	store_inputs(:timeline, timelines)

	gstat_rwlock do |gstat|
	    # 最終IDと最終時刻を更新
	    timelines.each do |status|
		if gstat.timeline_last_id <= status[:id]
		    gstat.timeline_last_id = status[:id]
		end
	    end
	    gstat.timeline_seen_at = curr
	end
	@logger.debug("fetch_timeline: ended")
    end

    #----------------------------------------------------------------------
    # 入力ストア処理

    # 入力ストアに格納する
    def store_inputs(type, statuses)
	return if statuses.size == 0

	@logger.debug("fetched #{statuses.size} status(es) as #{type.to_s}")
	statuses.each do |status|
	    case type
	    when :mention
		what_for = :mention_for
	    when :timeline
		what_for = :timeline_for
	    else
		raise "invalid type: #{type.to_s}"
	    end
	    extid = status[:id].to_s
	    istore = InputStore::open_store(IN_ACTIVE, extid)

	    # 入力を格納。入力は他のinoutプロセスやresgenプロセスから
	    # 同時にアクセスされるため、アトミックに操作する必要がある
	    resgen_needed = false
	    istore.transaction do
		if istore.root?(:status)
		    # すでに格納済み → 受信したbot情報のみを追記
		    @logger.debug("fetched #{extid} as #{type}, updated")

		    # このタイプで初めて追加された場合にだけ応答生成依頼
		    resgen_needed = true if istore[what_for].size == 0
		    istore[what_for] << @sym
		else
		    # まだ格納されてない → フェッチデータを作成してHashに格納
		    @logger.debug("fetched #{extid} as #{type}, created")
		    resgen_needed = true # 常に応答生成依頼

		    istore[:extid] = extid
		    istore[:status] = status
		    istore[:fetched_date] = Time.now.to_f
		    istore[:mention_for] = []
		    istore[:timeline_for] = []
		    istore[what_for] << @sym
		    istore[:processed_as_mention] = nil
		    istore[:processed_as_timeline] = nil
		end
		istore.commit()
	    end

	    # regenプロセスに応答生成を依頼
	    sendmsg("req_resgen #{extid}", "resgen") if resgen_needed
	end
    end

    #----------------------------------------------------------------------
    # 既存の入力データの処理

    def purge_old_input()
	curr = Time.now.to_i
	if curr < @vstat.last_input_purge + PURGE_INTERVAL
	    # @logger.debug("purging old input... skipped") # 頻繁すぎ
	    return
	end
	@vstat.last_input_purge = curr

	input_lock = VarStore::open_store(GLOBAL_INPUT_LOCK)
	input_lock.transaction do
	    curr = Time.now.to_i
	    InputStore::traverse(IN_ACTIVE).each do |extid|
		store = InputStore::open_store(IN_ACTIVE, extid)
		rx = InputStore::load_hash(store)
		if !rx.has_key?(:fetched_date)
		    # 時刻情報がない場合は、入力データの保存に失敗したと
		    # みなして即座にパージする
		    @logger.warn("purging #{extid}... no fetched_date!")
		    InputStore::move(IN_ACTIVE, IN_DONE, extid)
		elsif curr > rx[:fetched_date] + INPUT_MAX_TTL
		    @logger.debug("purging #{extid}... expired")
		    InputStore::move(IN_ACTIVE, IN_DONE, extid)
		end
	    end
	end
    end

    # すべての入力データに対して応答生成を依頼する。
    # 起動時において、未処理の入力データを一気に処理するために呼び出す。
    def request_resgen_for_pended_inputs()
	input_lock = VarStore::open_store(GLOBAL_INPUT_LOCK)
	input_lock.transaction do
	    if input_lock[:initial_push] == true
		@logger.debug("requesting resgen for pended inputs... skipped")
	    else
		@logger.info("requesting resgen for pended inputs... started")
		InputStore::traverse(IN_ACTIVE).each do |extid|
		    sendmsg("req_resgen #{extid}", "resgen")
		end
		@logger.info("requesting resgen for pended inputs... done")
		input_lock[:initial_push] = true
		input_lock.commit()
	    end
	end
    end

    #----------------------------------------------------------------------
    # タイムラインへのポスト

    def post_to_timeline(extid)
	ostore = OutputStore::open_store(OUT_ACTIVE, extid)
	tx = OutputStore::load_hash(ostore)
	if tx[:jit]
	    post_to_timeline_immediately(ostore, tx)
	else
	    post_to_timeline_with_retry(ostore, tx)
	end
    end

    # 再送に対応したポスト処理
    def post_to_timeline_with_retry(ostore, tx)
	extid = tx[:extid]
	@logger.info("#{extid} posting")

	# ポスト試行
	args = {:status => tx[:text]}
	if tx[:in_reply_to] != nil
	    args[:in_reply_to_status_id] = tx[:in_reply_to]
	end
	result = api(:update_status, args)
	ex = @last_api_exception
	### 以下をコメントアウトすると、常にポストを失敗扱いする
	#result = nil
	#ex = Exception.new('dummy exception')

	if result != nil
	    # ポスト試行が成功
	    @logger.info("#{extid} posted successful, #{result[:id]}")

	    # 今まで規制状態だったら、規制復帰処理を実施
	    if @vstat.post_banned
		@logger.info("post_ban_ended")
		@vstat.post_banned = false
		sendmsg("ban_ended #{@sym}", "delivery")
		sendmsg("ban_ended #{@sym}", "resgen")
	    end

	    record_successful_post(ostore, result, extid)

	elsif ex.is_a?(Exception) &&
		ex.message =~ /^User is over daily status update limit/
	    # ポスト規制によるポスト失敗
	    @logger.info("#{extid} post failed: post limit")

	    # いままで非規制状態だったら、規制開始処理を実施
	    if !@vstat.post_banned
		@logger.info("post_ban_started")
		@vstat.post_banned = true
		sendmsg("ban_started #{@sym}", "resgen")
		sendmsg("ban_started #{@sym}", "delivery")
	    end

	    # ポストデータを分配プロセスに戻す
	    sendmsg("req_delivery #{extid}", "delivery")

	else
	    # ポスト規制ではないポスト失敗
	    record_failed_post(ostore, ex, extid)
	end
    end

    # botごとの出力ストアから出力データを取り出し、Twitterにポストする
    # ポストが失敗しても再送処理は行なわない
    def post_to_timeline_immediately(ostore, tx)
	extid = tx[:extid]
	@logger.info("#{extid} posting (immediately)")

	# 指定時刻までウェイト
	curr = Time.now.to_f
	if tx[:valid_after] > curr
	    sleep(tx[:valid_after] - curr)
	end

	# ポスト試行
	args = {:status => tx[:text]}
	if tx[:in_reply_to] != nil
	    args[:in_reply_to_status_id] = tx[:in_reply_to]
	end
	result = api(:update_status, args)
	ex = @last_api_exception

	if result != nil
	    # ポスト試行が成功
	    @logger.info("#{extid} post successful: id #{result[:id]}")

	    record_successful_post(ostore, result, extid)

	else
	    record_failed_post(ostore, ex, extid)
	end
    end

    # 出力データにポスト成功の記録を追加
    def record_successful_post(ostore, result, extid)
	ostore.transaction do
	    ostore[:status_id] = result[:id]
	    ostore[:posted_at] = Time.now.to_f
	    ostore[:post_accepted_at] = parse_date(result[:created_at]).to_i
	    ostore.commit()
	end
	begin
	    OutputStore::move(OUT_ACTIVE, OUT_DONE, extid)
	    sendmsg("post_done #{extid} #{result[:id]} succeeded #{@sym}", "resgen")
	rescue => ex
	    @logger.error("failed to move #{extid} from #{OUT_ACTIVE} to #{OUT_DONE}")
	    raise ex
	end
    end

    def record_failed_post(ostore, ex, extid)
	case ex.to_s
	when /<title>Twitter \/ Over capacity<\/title>/
	    msg = 'twitter is over capacity'
	else
	    msg = ex.to_s
	end
	@logger.error("#{extid} post failed: #{msg}")

	# 失敗の記録を追加して処理対象から除外
	ostore.transaction do
	    ostore[:fail_message] = msg
	    ostore[:failed_at] = Time.now.to_i
	    ostore.commit()
	end
	OutputStore::move(OUT_ACTIVE, OUT_FAILED, extid)
	sendmsg("post_done #{extid} 0 failed #{@sym}", "resgen")
    end

    # direct_messageを送信する
    def send_dm(sn, text)
	opt = { :screen_name => sn, :text => text }
	result = api(:send_direct_message, opt)
	if result == nil
	    @logger.error("sending direct_message failed: @#{sn} #{text}")
	end
    end

    #----------------------------------------------------------------------
    # friendship
    # friendship情報を格納するYAMLファイルは、以下のメンバを持つ
    # [:friends]
    #	 	botがフォローするuseridのリスト
    #		Hash of {:botname => List of Integer}
    # [:followers]
    #	 	botをフォローするuseridのリスト
    #		Hash of {:botname => List of Integer}
    #

    FRIENDSHIP_CACHE_LIFE = 300

    # friendship情報のキャッシュを一定時間ごとに更新する
    # - 一定時間ごとに実際の再取得を行なう
    # - 更新が終わったらresgenプロセスに報告する
    def update_friendship_cache()
	# rate_limitが取得できない、またはAPI回数が減っている場合はリターン
	return if check_rate_limit() == nil

	# 前回のキャッシュ更新から一定時間以上経過していたら、更新する
	curr = Time.now.to_i
	if curr < @vstat.friendship_seen_at + FRIENDSHIP_CACHE_LIFE
	    @logger.debug("updating friendship cache... skipped")
	    return
	end

	@logger.debug("updating friendship cache... started")
	update_friendship_cache_immediately()
	sendmsg("friendship_updated", "resgen")
	@logger.debug("updating friendship cache... ended")
    end

    # friendship情報のキャッシュを一括で更新する
    def update_friendship_cache_immediately()
	# friendship情報を取得
	friends_ids = social_graph_api(:friends_ids)
	followers_ids = social_graph_api(:followers_ids)
	if friends_ids == nil || followers_ids == nil
	    @logger.error("update_friendship_cache_immediately: API failed")
	    return
	end

	# データベースを更新
	@fstore.transaction do
	    hash = @fstore[:friends] || {}
	    hash[@sym] = friends_ids || []
	    @fstore[:friends] = hash

	    hash = @fstore[:followers] || {}
	    hash[@sym] = followers_ids || []
	    @fstore[:followers] = hash

	    @fstore.commit()
	end

	# friendship情報の更新時刻を記録する
	@vstat.friendship_seen_at = Time.now.to_i
    end

    def social_graph_api(apitype)
	ids = []
	cursor = -1
	begin
	    opt = {
		:cursor => cursor,
		:screen_name => @sym.to_s,
	    }
	    result = api(apitype, '', opt)
	    if result == nil || !result.has_key?(:ids)
		@logger.error("social_graph_api: #{apitype} of #{@sym}")
		return nil
	    end

	    ids += result[:ids]
	    cursor = result[:next_cursor]
	    @logger.debug("social_graph_api: got #{result[:ids].length} IDs for #{apitype} of #{@sym}")
	end while cursor != 0
	return ids
    end

    # friend(botがフォローするユーザ)を追加する。
    # 追加に失敗した場合(すでにfriendであったりタイムアウトの場合)はnilを返す
    # 成功した場合はUserの構造体を返す
    def add_user_as_friend(f_name)
	user = api(:follow, f_name)
	if user == nil
	    ex = @last_api_exception
	    @logger.error("add_user_as_friend: failed to add #{f_name}: #{ex.to_s}")
	    return
	end

	@fstore.transaction do
	    @fstore[:friends][@sym] << user[:id]
	    @fstore.commit()
	end
	sendmsg("friendship_updated", "resgen")
    end

    # friend(botがフォローするユーザ)を削除する。
    # 削除に失敗した場合(すでにfriendでなかったりタイムアウトの場合)はnilを返す
    # 成功した場合はUserの構造体を返す
    def remove_user_from_friend(f_name)
	user = api(:leave, f_name)
	if user == nil
	    ex = @last_api_exception
	    @logger.error("remove_user_from_friend: failed to remove #{f_name}: #{ex.to_s}")
	    return
	end

	@fstore.transaction do
	    @fstore[:friends][@sym].delete(user[:id])
	    @fstore.commit()
	end
	sendmsg("friendship_updated", "resgen")
    end

    #----------------------------------------------------------------------

    # 設定ファイルのひな型を出力する
    def self.save_sample_config()
	gconf = InoutProcess::InoutConfig.new()

	puts "Enter consumery key: "
	gconf.consumer_key = STDIN.gets().chomp()
	puts "Enter consumery secret: "
	gconf.consumer_secret = STDIN.gets().chomp()
	consumer = OAuth::Consumer.new(gconf.consumer_key,
				       gconf.consumer_secret,
				       {:site => 'http://twitter.com'})

	request_token = consumer.get_request_token
	url = request_token.authorize_url

	puts "Visit following URL using bot account: #{url}\n"
	puts "Enter verifier number given at above site: "
	num = STDIN.gets().chomp()

	access_token = request_token.get_access_token(:oauth_verifier => num)
	gconf.access_token = access_token.token
	gconf.access_token_secret = access_token.secret

	puts "Saving bot config file... 'bot_config_new.yaml'\n"
	cstore = YAML::Store.new('bot_config_new.yaml')
	cstore.transaction do
	    cstore[:bot_config] = gconf
	    cstore.commit()
	end
	puts "done.\n"
    end

    # TLの既読情報をリセットし、最新のものまで読んだことにする
    def self.discard_pended(nr)
	inout = InoutProcess.new(nr)
	inout.sign_in_twitter()
	timelines = inout.api(:friends_timeline, {:count => 1})
	if timelines != nil && timelines.size > 0
	    curr = Time.now.to_i
	    inout.gstat_rwlock do |gstat|
		gstat.mention_last_id = timelines[0][:id]
		gstat.mention_seen_at = curr
		gstat.timeline_last_id = timelines[0][:id]
		gstat.timeline_seen_at = curr
	    end
	    print "discarded pending input: #{inout.sym}\n"
	end
    end


    #----------------------------------------------------------------------
    # インナークラス

    # botの固定設定
    class InoutConfig
	attr_accessor :consumer_key, :consumer_secret
	attr_accessor :access_token, :access_token_secret
	def initialize()
	    @consumer_key = @consumer_secret = ''
	    @access_token = @access_token_secret = ''
	end
    end

    # 保存する動的な情報
    class InoutState
	attr_accessor :version

	attr_accessor :friendship_seen_at
	attr_accessor :mention_seen_at
	attr_accessor :mention_last_id
	attr_accessor :timeline_seen_at
	attr_accessor :timeline_last_id

	def normalize()
	    @version ||= BOT_STATE_VERSION

	    @mention_seen_at ||= 0
	    @mention_last_id ||= 1
	    @timeline_seen_at ||= 0
	    @timeline_last_id ||= 1
	end
    end

    class VolatileState
	attr_accessor :rate_limit_cache
	attr_accessor :rate_limit_seen_at
	attr_accessor :friendship_seen_at
	attr_accessor :last_input_purge
	attr_accessor :post_banned

	def initialize()
	    @rate_limit_seen_at = 0
	    @friendship_seen_at = 0
	    @last_input_purge = 0
	    @post_banned = :unknown
	end
    end
end

#----------------------------------------------------------------------

if $0 == __FILE__
    # 起動方法:
    #   ruby core-inout.rb -options
    #	  特殊動作(bot設定ファイル生成等)
    #   ruby core-inout number
    #     inoutプロセスのインスタンスを実行
    # 引数なしで起動された場合はエラー表示して終了
    if ARGV.size > 0
	if ARGV[0] == '-save-sample-config'
	    InoutProcess::save_sample_config()

	elsif ARGV[0] == '-discard-pended'
	    nr = ARGV[1].to_i
	    InoutProcess::discard_pended(nr)

	else
	    inout = InoutProcess.new(ARGV[0].to_i)
	    inout.start()
	end
    else
	STDERR.puts("Error: no argument")
    end
end
