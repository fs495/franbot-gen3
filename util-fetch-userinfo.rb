#!/usr/bin/ruby
# -*- coding: utf-8 -*-

require 'rubygems'
require 'rubytter'
require 'yaml/store'

require 'core-consts'
require 'core-stores'

class UserInfo
    include Commondef

    def initialize()
	# クラスタ設定
	@cluster_config = Cluster::load_config()
	STDERR.puts "Loading bots: #{@cluster_config.bot_list.join(',')}"
	@bots = @cluster_config.bot_list

	# bot設定
	@bot_config = {}
	STDERR.print "Loading bot config:"
	@bots.each do |bot|
	    STDERR.print " #{bot}"
	    file = sprintf(BOT_CONFIG_FILE, bot)
	    bot_config_yaml = ConfigStore::open_store(file)
	    bot_config_yaml.transaction(true) do
		@bot_config[bot] = bot_config_yaml[:bot_config]
	    end
	end
	STDERR.print "\n"

	# APIハンドル
	@api = {}
	#STDERR.print "Signing into Twitter:"
	@bots.each do |bot|
	    #STDERR.print " #{bot}"
	    cfg = @bot_config[bot].ivars
	    oauth = Rubytter::OAuth.new(cfg['consumer_key'],
					cfg['consumer_secret'])
	    token = OAuth::AccessToken.new(oauth.create_consumer(),
					   cfg['access_token'],
					   cfg['access_token_secret'])
	    @api[bot] = OAuthRubytter.new(token)
	end
	#STDERR.print "\n"

	# 結果格納用のストア
	@yaml = VarStore::open_store('user_info.yaml')
    end

    def init()
	@yaml.transaction do
	    [:users, :invalids, :abuses,
	     :friends, :followers, :blocks].each do |key|
		@yaml[key] = {}
	    end
	    @yaml[:bots] = @bots
	    @yaml.commit()
	end
    end

    #----------------------------------------------------------------------
    # APIを発行してIDリストを得る処理群

    def issue_ids_api(key)
	@yaml.transaction do
	    @bots.each do |bot|
		cursor = -1
		result = []
		begin
		    STDERR.puts "Getting #{key} for #{bot}... (#{cursor})"
		    opt = { :cursor => cursor }
		    x = yield bot, opt
		    result += x[:ids]
		    cursor = x[:next_cursor]
		end while cursor != 0
		@yaml[key][bot] = result.sort()
	    end
	    @yaml.commit()
	end
    end

    def fetch_friend()
	issue_ids_api(:friends) do |bot, opt|
	    opt[:screen_name] = bot.to_s
	    @api[bot].friends_ids('', opt)
	end
    end

    def fetch_follower()
	issue_ids_api(:followers) do |bot, opt|
	    opt[:screen_name] = bot.to_s
	    @api[bot].followers_ids('', opt)
	end
    end

    def fetch_block()
	issue_ids_api(:blocks) do |bot, opt|
	    @api[bot].blocking_ids(opt)
	end
    end

    #----------------------------------------------------------------------
    # uconfの処理

    def read_uconf()
	users = {}
	abuses = {}
	invalids = []

	Dir::entries(File::join(VAR_DIR, UCONF_DIR)).each do |file|
	    path = File::join(VAR_DIR, UCONF_DIR, file)
	    next if not File.file?(path)
	    print "Reading #{path}...\r"

	    u, a = {}, {}
	    ustore = YAML::Store.new(path)
	    ustore.transaction(true) do
		# 古いタイプのuconfデータは読み込まない
		break if ustore[:common][:user_cache] == nil
		break if ustore[:common][:abuse] == nil
		break if ustore[:common][:abuse][:max_source] == nil

		# user_cacheから主要なエントリを抽出
		[:id, :screen_name, :name, :description, :location,
		 :followers_count, :friends_count, :listed_count,
		 :favourites_count, :statuses_count].each do |element|
		    u[element] = ustore[:common][:user_cache][element]
		end

		# abuse
		[:max_source, :max_tag, :max_url].each do |element|
		    a[element] = ustore[:common][:abuse][element]
		end
	    end

	    if u.size == 0
		invalids << file.to_i
	    else
		users[u[:id]] = u
		abuses[u[:id]] = a
	    end
	end

	@yaml.transaction do
	    @yaml[:users] = users
	    @yaml[:invalids] = invalids
	    @yaml[:abuses] = abuses
	    @yaml.commit()
	end
    end

    def remove_old_uconf()
	@yaml.transaction(true) do
	    @yaml[:invalids].each do |id|
		print "Removing old uconf... #{id}     \r"
		path = File::join(VAR_DIR, UCONF_DIR, id.to_s)
		File::unlink(path)
	    end
	end
	puts "\nRemoving old uconf done."
    end

    #----------------------------------------------------------------------

    def list_oneside()
	@yaml.transaction(true) do
	    @bots.each do |bot|
		count = 0
		@yaml[:friends][bot].each do |user_id|
		    if not @yaml[:followers][bot].include?(user_id)
			if @yaml[:users][user_id] == nil
			    puts "#{bot} #{user_id} no-screen-name"
			else
			    sn = @yaml[:users][user_id][:screen_name]
			    puts "#{bot} #{user_id} http://twitter.com/#{sn}"
			end
			count += 1 
		    end
		end
		puts "#{bot} follows #{count} users who does not follow bot"
	    end
	end
    end

    def remove_oneside()
	@yaml.transaction do
	    @bots.each do |bot|
		n_remove = 0
		@yaml[:friends][bot].each do |user_id|
		    next if @yaml[:followers][bot].include?(user_id)
		    next if n_remove > 50 ### XXX

		    begin
			opt = {:user_id => user_id}
			@api[bot].leave('', opt)
			@yaml[:friends][bot].delete(user_id)
			n_remove += 1
			puts "#{bot} removed #{user_id} successfully"
		    rescue
			puts "#{bot} removed #{user_id} unsuccessfully (Error)"
		    end
		end
	    end
	end
    end

    #----------------------------------------------------------------------

    def select_abuse()
	@yaml.transaction do
	    ids = []
	    total_remove = 0
	    @yaml[:abuses].each do |id, u|
		abuse_pt = 0
		abuse_pt += 1 if (u[:max_source] || 0) >= 1000
		abuse_pt += 1 if (u[:max_tag] || 0) >= 500
		abuse_pt += 1 if (u[:max_url] || 0) >= 500

		removal = 0
		@bots.each do |bot|
		    removal += 1 if @yaml[:friends][bot].include?(id)
		end

		if abuse_pt > 0 && removal > 0
		    ids << id
		    total_remove += removal
		end
	    end

	    puts "#{ids.size} new abusers found, #{total_remove} removal needed"
	    @yaml[:abuse_ids] = ids.sort()
	    @yaml.commit()
	end
    end

    ENTRIES = [['source', :max_source],
	       ['tag', :max_tag],
	       ['url', :max_url]]

    def generate_abuse_html()
	@yaml.transaction(true) do
	    puts "<table border=1 cellspacing=0>"

	    # タイトル
	    puts "<tr><th>id<th>screen_name"
	    ENTRIES.each do |entry|
		puts "<th>#{entry[0]}"
	    end
	    puts "</tr>"

	    # 表本体
	    @yaml[:abuse_ids].each do |id|
		sn = @yaml[:users][id][:screen_name]
		abuse = @yaml[:abuses][id]

		puts "<tr>"
		puts "<td>#{id}"
		puts "<td><a href='http://twitter.com/#{sn}'>#{sn}</a>"
		ENTRIES.each do |entry|
		    x = abuse[entry[1]] || '<br>'
		    puts "<td>#{x}"
		end
		puts "</tr>"
	    end
	    puts "</table>"
	end
    end

    def remove_abuser()
	@yaml.transaction do
	    @yaml[:abuse_ids].each do |abuse_id|
		print "#{abuse_id}: "
		@bots.each do |bot|
		    if @yaml[:friends][bot].include?(abuse_id)
			begin
			    opt = {:user_id => abuse_id}
			    @api[bot].leave('', opt)
			    @yaml[:friends][bot].delete(abuse_id)
			    print "R" # 削除した
			rescue
			    print "F" # 失敗
			end
		    else
			print "-" # 何もしない
		    end
		end
		puts ""
	    end
	end
    end

    def reset_abuse()
	Dir::entries(File::join(VAR_DIR, UCONF_DIR)).each do |file|
	    path = File::join(VAR_DIR, UCONF_DIR, file)
	    next if not File.file?(path)

	    print "Reading #{path}...\r"
	    ustore = YAML::Store.new(path)
	    ustore.transaction do
		[:source, :tag, :url,
		 :max_source, :max_tag, :max_url].each do |element|
		    ustore[:common][:abuse][element] = 0
		end
	    end
	end
    end

    #----------------------------------------------------------------------
    def populate_block()
	@yaml.transaction do
	    # いずれかのbotにブロックされているIDのリストを得る
	    ids = []
	    @yaml[:blocks].each_value do |list|
		ids |= list
	    end

	    # リストされたIDについて、まだ未ブロックのbotからブロックする
	    ids.sort.each do |id|
		STDERR.print "#{id}: "
		@yaml[:blocks].each_key do |bot|
		    if @yaml[:blocks][bot].include?(id)
			STDERR.print "-"
		    else
			begin
			    opt = {:user_id => id}
			    @api[bot].block('', opt)
			    @yaml[:blocks][bot] << id
			    STDERR.print "B"
			rescue
			    STDERR.print "F"
			end
		    end
		end
		puts ""
	    end
	end
    end

    def direct_op(user, action)
	@bots.each do |bot|
	    opt = {:screen_name => user}
	    begin
		@api[bot].send(action, '', opt)
		STDERR.puts "#{bot} #{action}s #{user}"
	    rescue
		STDERR.puts "Error: #{bot} cannot #{actione} #{user}"
	    end
	end
    end

    def block(user)
	direct_op(user, :block)
    end

    def remove(user)
	direct_op(user, :leave)
    end

    def show_sizes()
	@yaml.transaction(true) do
	    [:friends, :followers, :blocks].each do |key|
		@bots.each do |bot|
		    puts "#{key},#{bot},#{@yaml[key][bot].size}"
		end
	    end
	end
    end
end

#----------------------------------------------------------------------
# メインルーチン

uinfo = UserInfo.new()
while arg = ARGV.shift do
    case arg
    when '-init'
	uinfo.init()
    when '-fetch-all'
	uinfo.init(); uinfo.read_uconf()
	uinfo.fetch_friend(); uinfo.fetch_follower(); uinfo.fetch_block()

    when '-read-uconf'
	uinfo.read_uconf()
    when '-remove-old-uconf'
	uinfo.remove_old_uconf()

    when '-fetch-friend'
	uinfo.fetch_friend()
    when '-fetch-follower'
	uinfo.fetch_follower()
    when '-fetch-block'
	uinfo.fetch_block()

    when '-list-oneside'
	uinfo.list_oneside()
    when '-remove-oneside'
	uinfo.remove_oneside()

    when '-list-abuse'
	uinfo.select_abuse()
    when '-html-abuse'
	uinfo.generate_abuse_html()
    when '-remove-abuse'
	uinfo.remove_abuser()
    when '-reset-abuse'
	uinfo.reset_abuse()

    when '-populate-block'
	uinfo.populate_block()

    when '-block'
	uinfo.block(ARGV.shift)
    when '-remove'
	uinfo.remove(ARGV.shift)

    when '-size'
	uinfo.show_sizes()

    else
	STDERR.puts "Error: unknown argument: #{arg}"
    end
end
