include etc/secret.mk

restart:
	-@$(MAKE) quit
	-@$(MAKE) run

run:
	-@ruby core-monitor.rb &

quit:
	-@for sym in delivery inout0 inout1 inout2 inout3 inout4 inout5 inout6 resgen; do \
		echo -n "killing $$sym "; \
		file=var/pid_$$sym; \
		if [ -f $$file ]; then \
			pid=`cat $$file`; \
			echo -n "(pid $$pid) "; \
			kill $$pid; \
			rm $$file; \
		fi; \
		echo "done"; \
	done
	-@pid=`cat var/pid_monitor`; \
	echo "waiting for monitor (pid $$pid)"; \
	wait $$pid

flush:
	ruby core-monitor.rb -discard-pended

ps:
	@ps -w -O ppid,%cpu,rss,%mem -p `pgrep -f twibot`
	uptime

log:
	tail -f var/twibot.log

commit:
	hg commit -m .; hg push -v

#----------------------------------------------------------------------
publish: publih-script publish-html

publih-script:
	rm -f $(publishdir)/3rd-gen/*.rb
	cp -p *.rb $(publishdir)/3rd-gen
	cp -p Makefile $(publishdir)/3rd-gen

publish-html:
	cp -p doc/*.html doc/*.png doc/*.css $(publishdir)/doc

dropbox:
	mkdir -p "$(net_storate_dir)"
	rsync -v *.rb "$(net_storate_dir)/"
	rsync -v -r doc "$(net_storate_dir)/"

#----------------------------------------------------------------------

backup-uconf:
	pdumpfs var/uconf var/uconf_archive

save-daily-log:
	-mv -vi var/twibot.log.*.bz2 var/daily_archive
	-./util-compress-daily-log.sh var/twibot.log.*
	-mv -vi var/twibot.log.*.bz2 var/daily_archive

clean-log-backup:
	rm -r var/in_done_backup; mkdir var/in_done_backup
	rm -r var/out_done_backup; mkdir var/out_done_backup
	rm -r var/out_failed_backup; mkdir var/out_failed_backup

summarize-log:
	ruby util-summarize-log.rb -a
	$(MAKE) clean-log-backup
	$(MAKE) save-daily-log

#----------------------------------------------------------------------

