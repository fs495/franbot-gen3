# -*- coding: utf-8 -*-

# === ユーザのコンテクスト ===
# 「サブクラスタごとのコンテクスト」と「ユーザ固有のコンテクスト」
#
# サブクラスタごとのコンテクスト: @uconf, store[:subc][:entry_name]
# サブクラスタ共通のコンテクスト: @cconf, store[:common][:entry_name]
# 
# 例: store[:flan_subc][:nickname] = 'お兄様'
#     store[:global][:birth_month] = 9
# 
# プログラムからは、以下のアクセス方法がある
# - 直接storeを触る。推奨しない
# - 短縮形を使う(まとめて)
#  @cconf = @store[:common]
#  @uconf = @store[:xxx_subc]
# - 短縮形を使う(メンバ個別)
#  @cn == @store[:xxx_subc][:nickname]
# - アクセスメソッドを使う
# - リードオンリーのコピーを参照する
#  @affection, @tsundere, @charisma
#
# === サブクラスタ共通のコンテクストメンバ ===
# (廃止 :screen_name)
# :user_cache, :user_cache_date
# :birth_year, :birth_month, :birth_day, :last_birthday_celebration
#
# === サブクラスタごとのコンテクストメンバ ===
# :nickname
# :lastev
#   - bot_to_user_talk
#   - user_to_bot_talk
#   - user_tweet
#   - greet_{morning,day,evening,night,meal,bath,visit,home}
# :history
#   - bomb, mew, ufufu
# :base_affection, :base_tsundere
# :uwaki_aite, :uwaki_status_id
#
class TohoContext < Context
    include TohoConsts

    attr_reader :subc, :moon_phase, :moon_shape
    attr_reader :text_expanded
    attr_reader :to, :rt
    attr_reader :uconf, :cconf
    attr_reader :cn, :affection, :tsundere, :charisma

    HTTPCLIENT = HTTPClient.new()

    #----------------------------------------------------------------------
    def setup_common(rx, u_store)
	super(rx)
	@cconf = u_store[:common] || {}
	@moon_phase = ExternalService::moon_phase(Time.now)
	@moon_shape = ExternalService::moon_shape(@moon_phase)

	# t.coを展開
	@text_expanded = @text.gsub(/http:\/\/t\.co\/\w+/) do |tco|
	    begin
		HTTPCLIENT.get(tco).header['Location']
	    rescue
		tco
	    end
	end

	# あて先の文字列
	@to = "@#{@sn}"
	@rt = "RT @#{@sn}: #{@orig_text}"

	# データの正規化/誕生日
	@cconf[:birth_year] ||= nil
	@cconf[:birth_month] ||= nil
	@cconf[:birth_mday] || nil
	@cconf[:last_birthday_celebration] ||= 0

	# 悪用防止用データ
	@cconf[:abuse] ||= {}
	[:tag, :max_tag, :url, :max_url, :source, :max_source].each do |key|
	    @cconf[:abuse][key] ||= 0
	end
    end

    def setup_subc(subc, u_store, resgen_proc)
	super(SUBCLUSTER[subc], resgen_proc)
	@subc = subc

	# データベースのデータの一部を取り出してメモリ上に保持
	@uconf = u_store[subc] || {}

	# データの正規化/呼び名
	if @uconf[:nickname] == nil
	    @uconf[:nickname] = rx[:status][:user][:name].gsub(/@/, '＠')
	    @uconf[:nickname] += 'さん' if subc == :sakuya_subc
	end
	@cn = @uconf[:nickname]

	# データの正規化/履歴
	@uconf[:lastev] ||= {}
	[:bot_to_user_talk, :user_to_bot_talk, :user_tweet,
	 :greet_morning, :greet_day, :greet_evening, :greet_night,
	 :greet_meal, :greet_bath, :greet_visit, :greet_home].each do |key|
	    @uconf[:lastev][key] ||= 0
	end

	@uconf[:history] ||= {}
	if subc == :flan_subc
	    [:bomb, :mew, :ufufu, :rt].each do |key|
		@uconf[:history][key] ||= []
	    end
	    @uconf[:uwaki_aite] ||= ''
	end

	# データの正規化/好感度など
	if subc == :flan_subc || subc == :remy_subc
	    @uconf[:base_affection] ||= 20
	    @uconf[:base_affection] = 0 if @uconf[:base_affection] < 0
	    @uconf[:base_affection] = 100 if @uconf[:base_affection] > 100
	end
	if subc == :flan_subc
	    @uconf[:base_tsundere] ||= 0
	    @uconf[:base_tsundere] = -25 if @uconf[:base_tsundere] < -25
	    @uconf[:base_tsundere] = +25 if @uconf[:base_tsundere] > +25

	    # 好感度: 基本0〜100 ランダム-15〜+15
	    # ツンデレ: 基本-25〜+25 月相-25〜+25
	    @affection = @uconf[:base_affection] + (rand(30) - 15)
	    @tsundere = @uconf[:base_tsundere] +
		(25 * Math::cos(@moon_phase * Math::PI / 15)).to_i

	elsif subc == :remy_subc
	    @uconf[:base_charisma] ||= 0
	    @uconf[:base_charisma] = -25 if @uconf[:base_charisma] < -25
	    @uconf[:base_charisma] = +25 if @uconf[:base_charisma] > +25

	    # 好感度: 基本0〜100 ランダム-5〜+5
	    # カリスマ: 基本-25〜+25 月相-20〜+20 ランダム-5〜+5
	    @affection = @uconf[:base_affection] + (rand(10) - 5)
	    @charisma = @uconf[:base_charisma] + (rand(10) - 5)
	    @charisma += 20 if @moon_shape == :full_moon
	    @charisma -= 20 if @moon_shape == :new_moon
	end
    end

    #----------------------------------------------------------------------

    # 指定時間より前の履歴を消去し、かつ指定時間より後の履歴の個数が
    # 指定回数以上であるときtrueを返す
    # e.g.
    # return if check_history(:bomb, 6, 24 * 3600)
    # bomb_them()
    # add_history(:bomb)
    def check_history(bot, count, period = 86400)
	# return true # ぬるぽ祭り対応
	curr = Time.now.to_i
	@uconf[:history][bot].delete_if do |time|
	    curr > time + period
	end
	return @uconf[:history][bot].size >= count
    end

    # 履歴を追加する
    def add_history(bot)
	@uconf[:history][bot] << Time.now.to_i
    end

    # リプライかRTかを決定し、その文字列を返す
    def select_flan_resp(res, tsun_thre = -10, count = 2, history = :rt)
	if self.tsundere > tsun_thre || 
		self.check_history(history, count) ||
		self.affection < 90 ||
		rand(16) != 0
	    return "#{self.to} #{res}"
	else
	    self.add_history(history)
	    return "#{res} #{self.rt}"
	end
    end

    #----------------------------------------------------------------------
    # 
    def set_lastev(subject, curr = Time.now.to_i)
	@uconf[:lastev][subject] = curr
    end

    def get_lastev(subject)
	value = @uconf[:lastev][subject]
	return (value == nil) ? 0 : value
    end

    #----------------------------------------------------------------------
    # 
    def set_resgen_hint(subject, value)
	@rx[:resgen_hint][subject] = value
    end

end
