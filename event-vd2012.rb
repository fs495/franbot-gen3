# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

require 'GD'

class VdImage
    attr_reader :ow, :oh

    TTF_FILE = 'etc/image_orig/uzura.ttf'
    TTF_PT = 22

    def initialize(path)
	# ベース画像を元にステレオグラム画像を作成する
	@oimg = GD::Image.new_from_jpeg(path)
	@ow, @oh = @oimg.width, @oimg.height
	@img = GD::Image.newTrueColor(@ow, @oh)

	@oimg.copy(@img, 0, 0, 0, 0, @ow, @oh)
    end

    def get_color(r, g, b)
	return @img.colorAllocate(r, g, b)
    end

    def put_pattern(pimg, x, y)
	pimg.copy(@img, x, y, 0, 0, pimg.width, pimg.height)
    end

    def draw_pattern_frame(pimg)
	pw, ph = pimg.width, pimg.height
	nw, nh = @ow / pw, @oh / ph
	@off_w, @off_h = (@ow - nw * pw) / 2, (@oh - nh * ph) / 2

	(0 ... nh).each do |idx_h|
	    [0, nw - 1].each do |idx_w|
		x, y = @off_w + idx_w * pw, @off_h + idx_h * ph
		put_pattern(pimg, x, y)
	    end
	end
	(1 ... nw-1).each do |idx_w|
	    [0, nh - 1].each do |idx_h|
		x, y = @off_w + idx_w * pw, @off_h + idx_h * ph
		put_pattern(pimg, x, y)
	    end
	end
    end

    def draw_string(s, color, x, y, orientation)
	ox, oy = nil, nil
	r = GD::Image.stringTTF(color, TTF_FILE, TTF_PT, 0, 0, 0, s)[1]
	case orientation
	when :bl; ox, oy = r[0], r[1]
	when :br; ox, oy = r[2], r[3]
	when :tr; ox, oy = r[4], r[5]
	when :tl; ox, oy = r[6], r[7]
	end
	@img.stringTTF(color, TTF_FILE, TTF_PT, 0, x - ox, y - oy, s)
    end

    def draw_recipient(name, color)
	x, y = @off_w + 64, @off_h + 64
	draw_string(name, color, x, y, :tl)
    end

    def draw_messages(msgs, color)
	msgs.reverse.each_with_index do |s, index|
	    x = @img.width - @off_w - 64
	    y = @img.height - @off_h - 64 - 40 * index
	    draw_string(s, color, x, y, :br)
	end
    end

    def save(path)
	open(path, "w+") do |file|
	    @img.jpeg(file, 95)
	end
    end
end

class TohoResgen
    VALENTINES_DAY = Time.local(2012, 2, 14)

    COLORS = {
	:red =>		[255,   0,   0],
	:pink =>	[255, 179, 204],
	:yellow =>	[255, 255,   0],
	:white =>	[255, 255, 255],
	:skyblue =>	[ 51, 204, 230],
    }

    PIC_TAB = [# URL・ファイル名・作者・文字色
	       ['http://www.flickr.com/photos/cup_prof/4335861843/',
		'4335861843_bf1dec5dd9.jpg', 'cupprof', :pink],
	       ['http://www.flickr.com/photos/eszter/68153223/',
		'68153223_270d6e369d_o.jpg', 'eszter', :pink],
	       ['http://www.flickr.com/photos/427/2508045734/',
		'2508045734_a9a4ccd78e.jpg', '427', :red],
	       ['http://www.flickr.com/photos/quintanaroo/489358037/',
		'489358037_646e5aa407.jpg', 'QuintanaRoo', :red],
	       ['http://www.flickr.com/photos/quintanaroo/404148118/',
		'404148118_e2968abf6e.jpg', 'Quintanaroo', :red],
	       ['http://www.flickr.com/photos/bunchofpants/34840919/',
		'34840919_1851c6ed02.jpg', 'bunchofpants', :red],
	       ['http://www.flickr.com/photos/emilywaltonjones/1112839370/',
		'1112839370_c3c564a7d7_b.jpg', 'emilywjones', :pink],
	       ['http://www.flickr.com/photos/megpi/8567753/',
		'8567753_e6d40e8387_o.jpg', 'megpi', :skyblue],
	       ['http://www.flickr.com/photos/lynac/525957708/',
		'525957708_f965f92c8f.jpg', 'lynac', :white],
	       ['http://www.flickr.com/photos/tracyhunter/110294234/',
		'110294234_6f3695a78f_o.jpg', 'Tracy Hunter', :pink],
	       ['http://www.flickr.com/photos/ajagendorf25/3280407786/',
		'3280407786_70f0278ffc.jpg', 'ajagendorf25', :skyblue],
	       ['http://www.flickr.com/photos/quintanaroo/2242732240/',
		'2242732240_fe4d03b6a4_o.jpg', 'Quintanaroo', :pink],
	       ['http://www.flickr.com/photos/stephbond/2859151108/',
		'2859151108_39c950428d_b.jpg', 'stephbond', :red],
	      ]

    def check_vd2012(c)
	return false if !c.only_to_us
	return false if c.text !~ /(チョコ|ﾁｮｺ|Choco).*(頂戴|ちょうだい|ちょーだい|欲しい|ほしい|please|プリーズ|ﾌﾟﾘｰｽﾞ|ぷりーず|下さい|ください|もらいたい|貰いたい)/

	# テスト用: お世話係でなければ反応しない
	#return false if c.sn != 'fs495'

	# バレンタインデー当日でないと反応しない
	curr = Time.now()
	return false if curr < VALENTINES_DAY || curr > VALENTINES_DAY + 86400

	# すでに渡していた場合
	tere = tere_aa()
	if c.cconf[:vd2012_done]
	    url = c.cconf[:vd2012_url]
	    reply("#{c.to} い、いっこだけだよぉ…#{tere} #{url}", c)
	    return true
	end

	img_msgs = ["いつもフランと", "あそんでくれてありがとう", "大好き///"]
	res_msg = rnd(["えっと…これ…#{tere} っ",
		       "えーと…はい…#{tere} っ",
		       "ど、どうぞ…っ #{tere} っ",
		       "あとで見ておいてねっ#{tere} っ"])

	begin
	    uid = c.u[:id]
	    pic = rnd(PIC_TAB)
	    in_path = "etc/image_orig/#{pic[1]}"
	    out_path = "etc/image_dest/#{uid}"

	    # 画像の生成
	    img = VdImage.new(in_path)
	    color = pic[3]
	    pimg = GD::Image.new_from_png('etc/image_orig/heart.png')
	    img.draw_pattern_frame(pimg)
	    img.draw_recipient(c.cn, color)
	    img.draw_messages(img_msgs, color)
	    img.save(out_path + '.jpg')

	    # HTMLの生成
	    out = open(out_path + '.html', 'w')
	    out.puts <<"EOS"
<head>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class=baloon>
<div align=right>
<a href="http://twitter.com/share" class="twitter-share-button"
   data-text="" data-count="none" data-lang="ja">Tweet</a>
<script type="text/javascript"
        src="http://platform.twitter.com/widgets.js"></script>
</div>
<div class=content>
<img src="#{uid}.jpg">
</div>
</div>
</body>
EOS

	    # TLにポスト
	    url = "http://scarlets.dyndns.info/vd2012/#{uid}.html"
	    c.cconf[:vd2012_url] = url
	    c.cconf[:vd2012_done] = true
	    reply("#{c.to} #{res_msg} #{c.cconf[:vd2012_url]}", c)
	    return true

	rescue Timeout::Error, StandardError => ex
	    s = ex.message
	    ex.backtrace.each{|bt| s += "\n\t" + bt.to_s}
	    @logger.error("vd2012: #{s}")
	end
    end
end

if $0 == __FILE__
    orig_path = 'etc/image_orig/1112839370_c3c564a7d7_b.jpg'
    pat_path = 'etc/image_orig/heart.png'
    out_path = 'foo.jpg'

    img = VdImage.new(orig_path)
    pimg = GD::Image.new_from_png(pat_path)
    img.draw_pattern_frame(pimg)
    img.draw_string("Hello 世界♥", img.get_color(255, 0, 0), 10, 200, :tl)
    img.save(out_path)
end
