# -*- coding: utf-8 -*-

# 応答を生成するためのコンテクスト
class Context
    # 入力と、その簡易アクセス用メソッド
    attr_reader :rx		# 入力
    attr_reader :u		# user
    attr_reader :sn		# screen_name

    # サブクラスタに属するすべてのbotのSymbolリスト
    attr_reader :actors

    # ユーザとbotの関係性を収めたハッシュ
    attr_reader :rel

    # ツイートの解析結果と、その簡易アクセス用メソッド
    attr_reader :decoded	# テキストに関する詳細コンテクスト
    attr_reader :text		# テキスト本体
    attr_reader :orig_text	# オリジナルのテキスト

    # リプライ先情報
    attr_reader :to_all
    attr_reader :only_to_us, :to_us, :only_to_others, :to_none

    #------------------------------------------------------------
    NBSP = [0xa0].pack('U')		# non-breaking space
    FWSP = '　'				# fullwidth space
    WS = "(#{FWSP}|#{NBSP}|\\s)"	# any white space
    ID = "([a-zA-Z0-9_]+)"		# screen name

    #------------------------------------------------------------
    # メンバーのセットアップ
    # commonとsubcの両方をこの順番で呼ばないと完全にはセットアップされない
    # subcは複数回呼び出すことができる

    # サブクラスタに依存しない初期化
    def setup_common(rx)
	@rx = rx
	@u = rx[:status][:user]
	@sn = @u[:screen_name]

	analyze_tweet_text(rx[:status][:text])
    end

    # サブクラスタに依存する初期化
    def setup_subc(actors, res_generator)
	@actors = actors
	@rel = res_generator.analyze_relationship(@u, @actors)

	analyze_target()
    end

    #------------------------------------------------------------
    # ツイートの形式を判定し、宛名リスト・本文・RT宛先・RT部に分解して
    # @decodedハッシュに格納する。判定できる形式は以下のとおり
    #
    # - リプライ形式(:reply)		@user1 @user2 本文
    # - メンション形式(:mention)	. @user1 @user2 本文
    # - 非公式RT形式(:rt)		本文 RT @rt_to: rt_body
    # - その他(:plain)			本文
    #
    # @decodedハッシュのメンバは以下の通り
    # - type: ツイートの形式。:reply, :mention, :rt, :plainのいずれか
    # - text: 本文。リプライ先やRT部を含まない
    # - orig_text: もともとのテキスト
    # - r_to: (リプライ形式のみ) リプライ先を保持する配列
    # - m_to: (メンション形式のみ) 言及先を保持する配列
    # - rt_to: (非公式RT形式のみ) RT元のツイートの発信ユーザ
    # - rt_text: (非公式RT形式のみ) RT元のツイートの本文
    #
    # また、Contextオブジェクトの以下のメンバも設定する
    # - @text:	本文。リプライ先やRT部を含まない
    # - @orig_text: もともとのテキスト
    def analyze_tweet_text(orig_text)
	# 初期値を設定する
	@decoded = {
	    :type => nil,
	    :text => nil,
	    :orig_text => orig_text,
	    :r_to => [], :m_to => [], :rt_to => [],
	    :rt_text => nil,
	}

	s = orig_text.sub(/^#{WS}*/mo, '')
	if s[0] == ?@
	    @decoded[:type] = :reply
	    @decoded[:r_to] << $1 while s.sub!(/^@#{ID}#{WS}*/mo, '')
	    @decoded[:text] = s

	elsif s[0] == ?.
	    @decoded[:type] = :mention
	    s.sub!(/^\.#{WS}*/mo, '')
	    @decoded[:m_to] << $1 while s.sub!(/^@#{ID}#{WS}*/mo, '')
	    @decoded[:text] = s

	elsif s =~ /^(.*?)#{WS}*[RQ]T:?#{WS}*@#{ID}:?#{WS}*(.*)$/mo
	    @decoded[:type] = :rt
	    @decoded[:text],@decoded[:rt_to],@decoded[:rt_text] = $1,[$4],$6

	else
	    @decoded[:type] = :plain
	    @decoded[:text] = s
	end

	@text = @decoded[:text].freeze
	@orig_text = @decoded[:orig_text].freeze
    end

    #------------------------------------------------------------
    # リプライの中で指定されているリプライ先(Symbol値)のリストを返す
    def mentioned_targets()
	return (@decoded[:r_to] + @decoded[:m_to] + @decoded[:rt_to]).map {|x|
	    x.to_sym
	}
    end

    # リプライのターゲットを解析し、以下のメンバに設定する。
    #
    # @to_all: 「暗黙的に全員あて」だったかを示す
    #
    # @only_to_us: 明示的にリプ先が指定されたのが自分たちだけ
    # @to_us: 明示的にリプ先が指定されたのが自分たちと他のユーザ
    # @only_to_others: 明示的にリプ先が指定されたのが他のユーザだけ
    # @to_none: 明示的には誰も指定していない
    #
    # 前の1つと後ろの4つは独立に設定される。
    # (to_allとto_noneが同時にtrueになることがありうる)
    # 後ろの4つは互いに排反で、必ずどれかは成立する。
    def analyze_target()
	# 暗黙的指定
	@to_all = (@decoded[:type] != :reply)

	# 明示的指定
	include_actor = false
	include_other = false
	mentioned_targets().each do |bot|
	    if @actors.include?(bot)
		include_actor = true
	    else
		include_other = true
	    end
	end
	@only_to_us = include_actor && !include_other
	@to_us = include_actor && include_other
	@only_to_others = !include_actor && include_other
	@to_none = !include_actor && !include_other
    end

    private :analyze_tweet_text, :analyze_target

    #----------------------------------------------------------------------
    # ポストするbotの候補のリストのデフォルト値を得る
    def default_timeline_poster_candidates()
	# 以下の順番でリストアップする
	# (1) サブクラスタに属し、かつユーザがリプしたbot
	# (2) ユーザがリプしていない、相互フォローのbot
	targets = mentioned_targets()
	r = (@actors & targets).shuffle()
	r += (@actors & @rel[:actors_following_user] &
	      @rel[:actors_followed_by_user] - targets).shuffle()
	return r
    end

end
