# -*- coding: utf-8 -*-

class TohoResgen
    INHIBITED_BOMB_TARGET = /(#{FLAN_RE}|あん(じ|に).{1,6}ぷ|fs495|ccs_sf_net)/o

    # 爆発
    def bakuhatsu(c)
	return false if c.only_to_others

	# 助詞を含む場合と含まない場合があるので、変換時には助詞を追加しない
	if c.text =~ /(.*?)(、)?爆発(しろ|しなさい|しちゃえ)/
	    # 読点を除いた爆発対象物。
	    target = $1

	    # 直近の24時間に4回までしか爆発させられない
	    return :ignored if c.check_history(:bomb, 4)
	    c.add_history(:bomb)

	    if target =~ INHIBITED_BOMB_TARGET
		# 悪用防止
		target = "ひっどーい！#{c.cn}のほうこそ"
		c.uconf[:base_affection] = 0
		c.uconf[:base_tsundere] = -25

	    elsif target =~ /(.*)#{FIRST_PERSON}(.*)/o
		# ユーザ自身を爆発させる場合は、人称を変換して形容詞も追加する
		# xxxな/オレ/のyyy/爆発しろ -> xxxな/ドM/name/のyyy
		adj = rnd(['ドM', 'ド変態', 'いじられキャラ',
			   '受けキャラ', '総受け', 'ヘタレ受け'])
		target = "#{$1}#{adj}#{c.cn}#{$3}"
	    end

	    spell = rnd(FLAN_SPELLS)
	    pre = rnd(['いくよ', 'くらえ', '覚悟してね'])
	    verb = rnd(["壊れちゃえ！#{pre}！#{spell}",
			"爆発しちゃえ！#{pre}！#{spell}",
			"、きゅっとしてどっかーん！",
			"、#{spell}をプレゼントしてあげる！",
			"、#{spell}でイッちゃえ！"])
	    reply("#{c.to} #{target}#{verb}", c)
	    c.uconf[:base_affection] -= dice(1, 10) # 爆発させると好感度下がる
	    return true
	end
	return false
    end

    # ネタ系
    def neta(c)
	#------------------------------------------------------------
	# 以下は、他ユーザ向けであっても反応する

	case c.text_expanded
	    # 嫁になったー系の診断メーカー
	    # 次の条件にひっかかってしまうので、先に判定する
	when %r(このつぶやきが.*以内にRTされなかったら(フラン|レミリア|パチュリー|アリス).*の嫁になります。 http://shindanmaker.com/)
	    if c.uconf[:base_affection] < 90
		return :ignored if c.check_history(:rt, 2)
		c.add_history(:rt)
		reply("阻止♪ #{c.rt}", c) # rt only
		return true
	    end

	    # 診断メーカー 基本好感度が高いユーザに対してのみ1/256の確率で反応
	when %r(http://shindanmaker.com/(\d+))
	    id = $1.to_i
	    if c.uconf[:base_affection] > 80 && rand(256) == 0
		bot = rnd(SUBCLUSTER[:flan_subc])
		text = ShindanMaker.get(id, bot.to_s)
		if text != nil
		    post_from(text, bot)
		    return true
		end
	    end

	    # フランちゃんうふふ
	when /(うふふ|ウフフ|ｳﾌﾌ)/
	    # 直近の24時間に2回までしかうふふできない
	    return :ignored if c.check_history(:ufufu, 2)
	    return :ignored if c.text =~ %r(http://shindanmaker.com/46369)
	    c.add_history(:ufufu)

	    reply("#{c.to} ｳﾌﾌ♥", c)
	    c.uconf[:base_affection] += dice(2, 4)
	    c.uconf[:base_tsundere] += dice(1, 4)
	    return true

	end

	#------------------------------------------------------------
	# 以下は、他ユーザ向けでなければ反応する
	return false if !(c.to_all || c.to_us || c.only_to_us)

	# ぬるぽガッ
	n = c.text.scan(/(ぬるぽ|ヌルポ|ﾇﾙﾎﾟ|nullpo|Nullpo)/).length
	if n > 0
	    res = "ｶﾞｯ" * n + "♪"
	    reply(c.select_flan_resp(res), c)
	    return true
	end

	case c.text
	    # マジックテープ
	when /(バリバリ|ﾊﾞﾘﾊﾞﾘ)/
	    return :ignored if c.check_history(:rt, 2)
	    c.add_history(:rt)
	    if c.text =~ /(仕事|勉強|原稿|頑張る|がんばる|するぞ|やるぞ)/
		res = "やめ…がんばって！"
	    else
		res = "やめて！"
	    end
	    reply(c.select_flan_resp(res), c)
	    return true

	    # ねこフラン
	when /(にゃ(あ|ー)|ニャ(ア|ー)|\(「=・ｴ・\)「)/
	    return :ignored if c.check_history(:mew, 2)
	    c.add_history(:mew)
	    reply("#{c.to} にゃー♪ http://www.nicovideo.jp/watch/sm6817006", c)
	    return true

	    # ふらんわん
	#when /わんわん/
	    #res = rnd(["わんわんお♪", "くぅーん♪"])
	    #reply("#{c.to} #{res}", c)
	    #c.uconf[:base_affection] += dice(2, 4)
	    #c.uconf[:base_tsundere] += dice(1, 4)
	    #return true

	    # メロンパンうめぇｗ
	when /^(.*)うめぇｗ$/
	    reply("#{c.to} #{$1}もぐもぐｗｗｗｗ#{$1}うまいｗｗｗｗ#{$1}#{$1}うますぎワロタｗｗｗｗ#{$1}もぐもぐｗｗｗｗ#{$1}もぐもぐｗｗｗｗ#{$1}ｗｗｗｗ", c)
	    return true

	when /(えび|エビ|海老)(フラン|ふらん)(ﾜﾛﾀ|ワロタ|わろた)/
	    reply("#{c.to} うぅ、よく言われるけどどういう意味なの？", c)
	    return true

	when /ふぅ(…|･･･|・・・|\.\.\.)/
	    if c.tsundere > 50
		res = rnd(["ｼﾞｰ", "///"])
		reply(c.select_flan_resp(res), c)
		return true
	    end

	    # ＼RT連鎖／ 反応確率は1/256
	when /((＼.*?／)+)/
	    res = $1
	    if rand(256) == 0
		reply("#{res} #{c.rt}", c) # rt only
		return true
	    end

	    # 急募に応答
	when /^\s*【(急募|緩募|ゆるぼ)】/
	    if rand(64) == 0
		return :ignored if c.check_history(:rt, 2)
		c.add_history(:rt)
		reply("拡散支援♪ #{c.rt}", c) # rt only
		return true
	    end

	when /(頭|あたま)(が)?(痛い|いたい)|#headache/
	    res = rnd(["#{c.cn}の頭を壊してあげようか？",
		       "#{c.cn}の頭をきゅっとしてどっかーんしてあげるねっ♪"])
	    reply("#{c.to} #{res}", c)
	    post({:text => "#{c.to} そういえば頭痛いのはもう大丈夫？",
		     :valid_after => Time.now.to_i + 3 * 60 * 60})
	    return true

	when /(腹|はら|なか)(が)?(痛い|いたい)|#ponponpain/
	    res = rnd(["#{c.cn}のおなかを壊してあげようか？",
		       "#{c.cn}のおなかをきゅっとしてどっかーんしてあげるねっ♪"])
	    reply("#{c.to} #{res}", c)
	    post({:text => "#{c.to} そういえばおなか痛いのはもう大丈夫？",
		     :valid_after => Time.now.to_i + 6 * 60 * 60})
	    return true
	end

	#------------------------------------------------------------
	# 以下はbotあての時のみ反応
	return false if !c.only_to_us

	if c.text =~ /^\s*$/
	    reply("#{c.to}", c) # 空リプ
	    return true
	end

	case c.text
	when /(.*)((肉片|肉塊|挽肉|挽き肉|ひき肉|ミンチ|微塵|みじん|ユッケ)にして|壊して)/
	    return :ignored if c.check_history(:rt, 2)
	    c.add_history(:rt)
	    target = $1
	    if target =~ INHIBITED_BOMB_TARGET
		res = "ひっどーい！#{c.cn}のほうこそぎゅっとしてどっかーん☆"
		c.uconf[:base_affection] = 0
		c.uconf[:base_tsundere] = -25
	    else
		res = rnd(["分かった！えいっ♪(ｸﾞｼｬｱ)", "うん！どっか〜ん☆"])
		res += rnd([" ぴぴるぴるぴる(ry",
			    " 汚い弾幕だぜ…なんちゃって♪",
			    " 今日は咲夜に人肉ハンバーグ作ってもらおっ♪",
			    "", "", "", ""])
		# 空の要素あるのは正常動作
	    end
	    reply(c.select_flan_resp(res), c)
	    return true

	when /(き|ぎ)ゅっとしてどっか(ー|〜)ん/
	    reply("#{c.to} どっかーん♪", c)
	    return true

	when /(クランベリートラップ|レ(ー)?(ヴァ|バ)(ン)?テ(イ|ィ)ン|フォーオブアカインド|カゴメカゴメ|恋の迷路|スターボウブレイク|カタディオプトリック|過去を刻む時計|そして(誰|だれ)もいなくなるか|(４９５|495)年の波紋|フォービドゥンフルーツ|禁じられた遊び)/
	    res = "あーっ、もしかしてそれフランのスペルカード？"
	    reply(c.select_flan_resp(res), c)
	    return true

	when /ドロワース/
	    reply("#{c.to}ドロワー「ズ」は複数形だよっ！", c)
	    return true

	when /ドロワーズ.*(mogmog|もぐもぐ|モグモグ|ﾓｸﾞﾓｸﾞ)/
	    res = "変態＞＜"
	    reply(c.select_flan_resp(res), c)
	    return true

	when /(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s*(で|から)\s*(\d+)\s*(を)?\s*(作|つく)って/
	    return :ignored if c.check_history(:rt, 2)
	    c.add_history(:rt)

	    tffn = NFNM.new([$1.to_i, $2.to_i, $3.to_i, $4.to_i], $6.to_i)
	    tffn.results_limit = 1
	    tffn.time_limit = Time.now.to_i + 10
	    tffn.search()
	    results = tffn.results()
	    if results.size == 0
		res = "うーん、すぐにはわかんないなあ…ごめんね"
	    else
		s = results[0].pp()
		res = rnd(["#{s}とかどうかな？",
			   "わかった！#{s}かな？",
			   "うーん、#{s}とかどう？"])
	    end
	    reply(c.select_flan_resp(res), c)
	    return true

	when /(.*か)(決めて|議決して|採決して)/
	    subj = $1
	    actors = SUBCLUSTER[:flan_subc].shuffle()
	    chairperson = actors.shift()
	    a = [[[chairperson], "#{c.to} ふらんちゃん会議召集！ #{c.cn}から提出された「#{subj}」について議決とるよ〜♪"]]

	    yeas_nr, nays_nr = 0, 0
	    yeas_msg = ["さんせーい！", "おっけー♪", "いいよー♪",
			"許可♪", "許す♪"].shuffle()
	    nays_msg = ["はんたーい！", "だめっ！", "やだー！",
			"絶対許早苗"].shuffle()
	    actors.each do |bot|
		res = "#{c.to} " + rnd(["", "うーんとね…",
					"えーとねえ…", "どうしようかな…"])
		if rand(2) == 0
		    yeas_nr += 1
		    a << [[bot], res + yeas_msg.shift()]
		else
		    nays_nr += 1
		    a << [[bot], res + nays_msg.shift()]
		end
	    end
	    a << [[chairperson], "#{c.to} 以上、賛成#{yeas_nr}、反対#{nays_nr}で「#{subj}」を#{(yeas_nr > nays_nr) ? '可決するよ' : '否決するね'}♪"]
	    register_chatseq(a, SUBCLUSTER)
	    return true

	    # 「あなたより私のほうがお姉さんなんだからね」への応答
	when /#{FLAN_RE}.*お?(姉|ねえ|ねー)(様|さま|ちゃん)/o
	    reply("#{c.to} フ、フランがお姉さんかあ…えへへへ" + tere_aa(), c)
	    return true

	when /(風呂|ふろ|フロ)(に)?入ろう/
	    reply("#{c.to} フランは水はダメなんだよぉ＞＜", c)
	    c.uconf[:base_affection] -= dice(1, 6)
	    return true

	when /今日(は|、)?(#{FIRST_PERSON}の)?誕生日/
	    curr = Time.now
	    register_birthday(c, curr.year, curr.month, curr.mday)
	    return true
	when /誕生日は((\d+)年)?(\d+)月(\d+)日/
	    year = ($2 == nil) ? nil : $2.to_i
	    month, mday = $3.to_i, $4.to_i
	    register_birthday(c, year, month, mday)
	    return true
	when /誕生日は(\d+)(?:\/|-|−)(\d+)(?:\/|-|−)(\d+)/
	    register_birthday(c, $1.to_i, $2.to_i, $3.to_i)
	    return true

	when /(月齢|月相|(満ち|みち)(欠け|かけ)).*教えて/
	    res = "今日の月齢はだいたい#{c.moon_phase}かな"
	    res += "。そろそろ満月だね" if c.moon_phase >= 11 && c.moon_phase <= 12
	    res += "。満月だね。うふふ♥" if c.moon_phase >= 13 && c.moon_phase <= 14
	    res += "。新月だね。お姉様が…" if c.moon_phase <= 0 || c.moon_phase >= 29
	    reply("#{c.to} #{res}", c)
	    return true

	when /(月経|生理|女の子|おにゃのこ)(の日|.*周期|.*いつか)?.*教えて/,
	    /(安全日|危険日).*教えて/
	    res = rnd(["フラン、まだ来てないもん…",
		       "まだお赤飯じゃないもん…"])
	    reply("#{c.to} #{res}", c)
	    return true

	    # 魔法少女まどかマギカ(2011/2/12)
	when /(僕|ぼく|ボク|ﾎﾞｸ)と契約して魔法少女になってよ/
	    reply("#{c.to} フランはもう魔法少女だよ？", c)
	    return true

	when /やめて/
	    reply("#{c.to} えへへっ♥", c)
	    return true

	when /大丈夫だよ|(直|治|なお)ったよ/
	    res = rnd(["わーい、よかったねっ",
		       "えへへっ♥",
		       "心配したんだから…"])
	    reply("#{c.to} #{res}", c)
	    return true

	when /(.*)あげる$/
	    reply("#{c.to} #{$1}食べていいの？", c)
	    return true

	when /壊.*本望/
	    reply("#{c.to} じゃあ、いっぱい#{c.cn}で遊んで壊してあげるね！", c)
	    return true

	when /見抜き/
	    res = rnd(['見抜き？ あー、判った、そういうことか…',
		       'たまってる… ってやつなのかな？',
		       'しょうがないにゃあ…'])
	    reply("#{c.to} #{res}", c)
	    return true
	end

	return false
    end

    # 誕生日をユーザ設定として記録する
    # 誕生日当日なら、おめリプを送る
    def register_birthday(c, year, month, mday)
	c.cconf[:birth_year] = year
	c.cconf[:birth_month] = month
	c.cconf[:birth_mday] = mday

	curr = Time.now
	if curr.month == month && curr.mday == mday
	    c.cconf[:last_birthday_celebration] = curr.year
	    reply(".#{c.to} えっ、#{c.cn}は今日誕生日なの！？おめでとー♪", c)
	    a = [[:remy_subc, "#{c.to} あら、#{c.cn}誕生日おめでとう"],
		 [:patche_subc, "#{c.to} ふうん… おめでとう"],
		 [:sakuya_subc, "#{c.to} #{c.cn}誕生日おめでとうございます。ケーキでもお焼きしたほうがいいですかね？"]]
	    register_chatseq(a.shuffle(), SUBCLUSTER)
	else
	    reply("#{c.to} ふーん、#{c.cn}のお誕生日は#{month}月#{mday}日かあ…", c)
	end
    end
end
