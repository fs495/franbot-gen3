# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

class TohoResgen
    def thvote9(c)
	if c.text =~ /(.*)toho_vote9/
	    text = $1
	    @logger.error("text=#{text}")
	    if text =~ /フランドール・スカーレット（一押し）/
		res = rnd(["どうもありがとう！ #{c.cn}とっても大好きだよっ♪",
			   "#{c.cn}、どうもありがとう！またおねえさまといっしょにランクインできるといいなあ…"])
		reply("#{c.to} #{res}", c)
		c.uconf[:base_affection] += 100
		return true

	    elsif text =~ /フランドール・スカーレット/
		res = "どうもありがとう！"
		reply("#{c.to} #{res}", c)
		c.uconf[:base_affection] += dice(2, 20)
		return true

	    end
	end
	return false
    end
end
