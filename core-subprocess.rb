# -*- coding: utf-8 -*-

class Subprocess
    include Commondef

    def initialize(prog_name)
	@prog_name = prog_name
	@logger = nil
	$0 = "[#{File.basename(Dir.pwd)}]#{@prog_name}"

	@sock = nil
	@port_timeout = 30
    end

    #----------------------------------------------------------------------

    def open_logger(level = Logger::INFO)
	@logger = Logger.new(File::join(VAR_DIR, LOG_FILE))
	@logger.level = level
	@logger.datetime_format = LOG_DATETIME_FORMAT
	@logger.formatter = proc{|severity, datetime, progname, message|
	    d = datetime.strftime('%H:%M:%S')
	    if severity == "DEBUG" || severity == "INFO"
		s = ''
	    else
		s = "#{severity}: "
	    end
	    "#{d} #{progname}: #{s}#{message}\n"
	}
	@logger.progname = @prog_name
	@logger.debug("started execution")
	return @logger
    end

    def reopen_logger(msg = "closing log")
	@logger.info(msg)
	@logger.close
	return open_logger(@logger.level)
    end

    #----------------------------------------------------------------------

    def connect_to_monitor(port_number, port_timeout = 30)
	@port_number = port_number
	@port_timeout = port_timeout
	begin
	    # モニタに接続し、初期化コマンドを送信
	    @sock = TCPSocket.open(HOSTNAME, @port_number)
	    @sock.puts("init #{@prog_name.to_s}")

	    # モニタからの初期化応諾コマンドを受信
	    s = @sock.gets().chomp()
	    raise "junk input from monitor: #{s}" if s != 'init_ack'
	rescue Timeout::Error, StandardError => ex
	    @logger.error(exception_desc(ex))
	    raise "cannot establish connect to monitor"
	end
    end

    def close_monitor()
	@sock.close()
    end

    #----------------------------------------------------------------------

    # 定常実行処理
    def start()
	begin
	    prepare()
	    run()
	rescue Timeout::Error, StandardError => ex
	    @logger.error("exception occured: #{exception_desc(ex)}")
	    raise ex
	end
    end

    # 定常実行時の準備処理
    def prepare()
	# 親クラスでは何もしない
    end

    # 定常実行時のメインループ処理
    def run()
	# 実行タイミングが重ならないように初期ウェイト量をランダムにする
	timeout = rand(@port_timeout)

	while true
	    avails = select([@sock], nil, nil, timeout)
	    if avails == nil
		timed_out()
	    elsif avails[0].include?(@sock)
		line = @sock.gets()
		next if line == nil
		read_socket(line.chomp)
	    end
	    timeout = @port_timeout
	end
    end

    # タイムアウト時の処理
    def timed_out()
	# 親クラスでは何もしない
    end

    #----------------------------------------------------------------------

    # ソケットからブロッキングせずにコマンドを読み取れるか試みる。
    # 読み取れる場合はコマンドを示すString、読み取れない場合はnilを返す
    def try_to_read_socket()
	avails = select([@sock], nil, nil, 0)
	return nil if avails == nil
	return nil if !avails[0].include?(@sock)
	line = @sock.gets()
	return line.chomp()
    end

    # 他プロセスへのメッセージを、モニタを経由して送信する
    def sendmsg(msg, to)
	@logger.debug("sendmsg: ->#{to}: #{msg}")
	@sock.puts("<#{to}>#{msg}")
    end

    # 例外オブジェクトの表示用文字列を得る
    def exception_desc(ex)
	# 例外の記述がHTTPステータスとして返される場合(Twitter過負荷時など)、
	# "unexpected token"の場合は異常に長くなってしまうので書略する
	# TODO: 例外種別を限定する
	s = "#{ex.class.to_s}: "
	s += (ex.message =~ /^(\d+: unexpected token) at/) ? $1 : ex.message
	ex.backtrace.each{|bt| s += "\n\t" + bt.to_s}
	return s
    end
end
