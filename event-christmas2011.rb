# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

class TohoResgen
    CHRISTMAS_START = Time.local(2011, 12, 24, 23, 30, 0)
    CHRISTMAS_END = Time.local(2011, 12, 25, 0, 30, 0)

    def in_christmas?()
	curr = Time.now
	return true if curr > CHRISTMAS_START && curr < CHRISTMAS_END
    end

    def process_special_event()
	curr = Time.now

	# 前回の処理から1000秒以上経過している時だけ
	return false if curr.to_i < @last_periodic_process + 1000

	if curr.mon == 12 && curr.mday == 25 &&
		curr.hour == 0 && (curr.min >= 00 && curr.min <= 30)
	    seq = [
		   [:flan_subc, "お姉様、サンタさんって…"],
		   [:remy_subc, "@@ ん？"],
		   [:flan_subc, "@@ 夜中にプレゼントを枕元に置いてくれるんだよね？ 吸血鬼といっしょなのかなあ？"],
		   [:patche_subc, "@@ フラン？ まさかサンタクロースなんて存在を信j…"],
		   [:remy_subc, "@@ そこまでよ！"],
		   [:patche_subc, "@@ 私の決めゼリフ取らないでよレミィ…"],
		   [:remy_subc, "@@ それはともかく… フラン、悪魔のもとには聖人であるサンタクロースは来ないわよ"],
		   [:flan_subc, "@@ ええっ… そんなぁ；；"],
		   [:sakuya_subc, "@@ お嬢様…"],
		   [:remy_subc, "@@ でも"],
		   [:flan_subc, "@@ でも…？"],
		   [:remy_subc, "@@ 紅いサタン…じゃなくて紅い悪魔ならここにいるんじゃなくて？"],
		   [:flan_subc, "@@ …！？ お姉様///"],
		   [:remy_subc, "@@ さ、何か欲しいものがあるのかしら？ たまには姉らしいこともしないとね"],
		   [:flan_subc, "@@ えーっと… その… お姉様がサンタさんだと言えないよぉ＞＜"],
		   [:remy_subc, "@@ あら、私じゃ役不足かしら？"],
		   [:patche_subc, "@@ レミィ、「役不足」の意味間違えてるわよ"],
		   [:remy_subc, "@@ う、うるさいっ/// どういうことなのよフラン？"],
		   [:flan_subc, "@@ えーっと… 欲しいのは…お姉様だから…///"],
		   [:remy_subc, "@@ えっ…///"],
		   [:flan_subc, "@@ ///"],
		   [:patche_subc, "咲夜、こんなに甘々じゃケーキはいらないわね"],
		   [:sakuya_subc, "@@ そうですね、パチュリー様… 邪魔者は退散いたしますわ…"],
		   [:patche_subc, "@@ 私もそうするわ… レミィ、フラン、メリークリスマス。よい夜を…"],
		   [:remy_subc, "悪魔が言っていいのか分からないけど… メリークリスマス"],
		   [:flan_subc, "お姉様、フォロワーのみんな、メリークリスマス♪ クリスマスを爆発させるのはもうちょっとだけお休みさせてもらうねっ♪"],
		   [:flan_subc, "さて、プレゼントのお姉様はどうやって遊ぼうかなあ… じゅるり"],
		   [:remy_subc, "@@ ちょっ、フラン、擬音がおかしいって… あーれー(ry"],
		  ]
	    register_chatseq(seq, SUBCLUSTER)

	elsif curr.mon == 2 && curr.mday == 14 && 
		curr.hour == 22 && (curr.min >= 0 && curr.min <= 20)
	    seq = [[:flan_subc, "うう、お姉様気に入ってくれるかな…"],
		   [:remy_subc, "@@ 私がどうかしたの？"],
		   [:flan_subc, "@@ ドキッ！"],
		   [:remy_subc, "@@ なんかあやしい…"],
		   [:flan_subc, "@@ お姉様、これっ＞＜"],
		   [:remy_subc, "@@ これは… もしかしてチョコレート？"],
		   [:flan_subc, "@@ 2月14日はチョコレート渡すって聞いたの… すきな相手にｺﾞﾆｮｺﾞﾆｮ…"],
		   [:remy_subc, "@@ うーん、この紅い悪魔に聖人にちなんだものを渡すとは…"],
		   [:flan_subc, "@@ も、もしかして気に入らなかった？"],
		   [:remy_subc, "@@ ううん、そんなことないよ。嬉しいわ。ありがとうフラン///"],
		   [:flan_subc, "@@ えへへへ…(///ω///)"],
		   [:remy_subc, "@@ 恥ずかしい妹ね… それはそうと、渡す相手はどうのこうのってさっき言ってなかった？私なんかで良かったの？"],
		   [:flan_subc, "@@ そ、それは… なんでもないよーだ♪"],
		   [:remy_subc, "@@ あ、待ちなさいったらフラン///"],
		  ]
	    register_chatseq(seq, SUBCLUSTER)
	end
    end
end
