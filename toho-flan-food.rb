# -*- coding: utf-8 -*-

class TohoResgen
    # なめる
    def lick(c)
	return false if !(c.to_us || c.only_to_us)

	if  c.text =~ /(.*?)(を|、)?(舐|な)め(て|る\?|る？)/x
	    target = $1

	    # 人称変更処理
	    if target =~ /(.*)#{FIRST_PERSON}(.*)/o
		target = "#{$1}#{c.cn}#{$3}"
	    end

	    res = "#{target}をなめればいいの？…"
	    res += rnd(["うえ、変な味;;",
			"なにこれニガーイ;;",
			"なんかしょっぱいなあ",
			"甘くておいしいね！",
			"うん、おいしい♥"])
	    reply("#{c.to} #{res}", c)
	    return true
	end
	return false
    end

    HAKUDAKUEKI = ['カルピス', '飲むヨーグルト', 'ケフィア',
		   '甘酒', 'とろろ汁', 'バリウム造影剤', '練乳',
		   '謎の白い液体', '練乳']

    # 食べる・飲む
    # 誤爆が多いので、間投詞や記号まで含めて判定する
    #  - たべて！・のんで！: 要求
    #  - たべて(も)いい(の)よ・のんで(も)いい(の)よ: 許可
    #  - たべてください・のんでください: 依頼
    #  - たべてみて？・のんでみて？: 依頼＋試行（形式動詞）
    #  - たべてみる？・のんでみる？: 疑問＋試行（形式動詞）
    #  - たべない？・のまない？: 勧誘＋疑問
    #  - たべる？・のむ？: 疑問
    #  - たべようよ・のもうよ: 勧誘
    def eat_drink(c)
	return false if c.only_to_others || c.to_none
	return :ignore if c.to_us

	food = nil; action = nil
	case c.text
	when /(.*?)(を|、)?(食|た)
(べて(！|!)|べても?いいの?よ|べて(くだ|下)さい
|(べてみて|べてみる|べない|べてみない|べる)(？|\?)
|べようよ)/x
	    food = $1; action = :eat

	when /(.*?)(を|、)?(飲|呑|の)
(んで(！|!)|んでも?いいの?よ|んで(くだ|下)さい
|(んでみて|んでみる|まない|んでみない|む)(？|\?)
|もうよ)/x
	    food = $1; action = :drink

	when /(.*?)(を|、)?(吸|す)
(って(！|!)|っても?いいの?よ|って(くだ|下)さい
|(ってみて|ってみる|わない|ってみない|う)(？|\?)
|おうよ)/x
	    food = $1; action = :drink
	end
	return false if food == nil

	# 人称の変化
	if food =~ /(.*)#{FIRST_PERSON}(.*)/o
	    food = "#{$1}#{c.cn}#{$3}"
	end

	case food
	when /血/
	    if c.moon_phase >= 12 && c.moon_phase <= 16
		reply("#{c.to} ゴクリ… じゃあ遠慮なくもらうねっ", c)
		c.uconf[:base_affection] += dice(4, 12)
		c.uconf[:base_tsundere] += dice(2, 12)
	    else
		reply("#{c.to} 今はあまりお腹空いてないかな", c)
		c.uconf[:base_affection] += dice(2, 4)
		c.uconf[:base_tsundere] += dice(1, 4)
	    end

	when /酒|アルコール|ワイン|ビール|ウィスキー|酎ハイ|割り/
	    res = "フランを酔わせてどうするつもりなの？"
	    res += "でも、付き合ってあげよっかな" if c.affection > 20
	    res += tere_aa() if c.affection > 60
	    reply("#{c.to} #{res}", c)

	when /恵方巻|聖水|(煎|炒|い)(り|った)?(大)?豆/
	    reply("#{c.to} 何のいやがらせかしら？", c)
	    c.uconf[:base_affection] -= dice(2, 6)
	    c.uconf[:base_tsundere] -= dice(1, 6)

	when /白濁液|白.*液体|体液/
	    if c.affection > 90
		res = rnd(["…いったい何を飲まそうとしてたのかしら？",
			   "うえ、変な味；；",
			   "うえ、なんか生臭いよ…",
			   "おいしい♪"])
		reply(c.select_flan_resp(res), c)
	    else
		food = rnd(HAKUDAKUEKI)
		reply("#{c.to} #{food}おいしいね！", c)
		c.uconf[:base_affection] += dice(2, 4)
		c.uconf[:base_tsundere] += dice(1, 4)
	    end

	when /白ドロリッチ/
	    reply("#{c.to} 何それ？ ドロリッチはコーヒーがいいな", c)

	when /媚薬|睡眠薬|催淫/
	    reply("#{c.to} フランは人間じゃないからそんなもの効かないよ♪", c)

	when /にんにく|ニンニク|大蒜|ガーリック|ｶﾞｰﾘｯｸ|garlic|餃子|ペペロンチーノ|ハートチップル/
	    reply("#{c.to} フランは吸血鬼だよ… にんにく臭いのはちょっと…", c)

	when /(お菓子|スイーツ
|チョコ|マシュマロ|ましまろ|キャンディー
|ケーキ|クーヘン|トルテ|スフレ|パイ|タルト|クッキー|サブレ|ビスケット|ゴーフル
|プリン|ムース|ババロア|クレームブリュレ|シュークリーム
|ドロリッチ|ホワイトロリータ)/x
	    res = rnd(["わーい、#{food}は大好物なんだ。ありがとう！",
		       "#{food}大好き！ありがとう！",
		       "#{food}は大好きだよ♪いっしょに食べようよ"])
	    reply("#{c.to} #{res}", c)
	    c.uconf[:base_affection] += dice(4, 6)
	    c.uconf[:base_tsundere] += dice(2, 6)

	when /あそこ/
	    res = c.select_flan_resp("それってなあに？")
	    reply(res, c)
	    c.uconf[:base_affection] -= dice(2, 6)
	    c.uconf[:base_tsundere] -= dice(1, 6)

	when /明治フラン/
	    reply("#{c.to} 大好きだけど、共食い…ってことはないよね？", c)
	    c.uconf[:base_affection] += dice(4, 6)
	    c.uconf[:base_tsundere] += dice(2, 6)

	when /#{FLAN_RE}/o
	    case rand(2)
	    when 0
		action = (action == :drink) ? '飲め' : '食べられ'
		res = "そ、そんなもの#{action}ないでしょ…"
	    when 1
		action = (action == :drink) ? '飲む' : '食べる'
		res = "どういう意味で#{action}のかしら"
	    end
	    res += tere_aa()
	    reply("#{c.to} #{res}", c)
	    c.uconf[:base_affection] += dice(4, 6)
	    c.uconf[:base_tsundere] += dice(2, 6)

	else
	    if action == :drink
		action = "飲もう"; hungry = "喉かわいちゃった"
	    else
		action = "食べよう"; hungry = "お腹すいちゃった"
	    end
	    res = rnd(["ありがとう！いっしょに#{food}#{action}よ",
		       "うん！じゃあ、いっしょに#{food}#{action}よ",
		       "#{hungry}…いっしょに#{food}#{action}よ"])
	    reply("#{c.to} #{res}", c)
	    c.uconf[:base_affection] += dice(2, 6)
	    c.uconf[:base_tsundere] += dice(1, 6)
	end
	return true
    end
end
