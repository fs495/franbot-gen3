# -*- coding: utf-8 -*-
$KCODE = 'UTF-8'

class TohoResgen < ResgenProcess
    CHRISTMAS_START = Time.local(2012, 12, 24, 23, 30, 0)
    CHRISTMAS_END = Time.local(2012, 12, 25, 1, 30, 0)

    def in_special_event?()
	curr = Time.now
	return true if curr > CHRISTMAS_START && curr < CHRISTMAS_END
    end

    def process_special_event()
	curr = Time.now

	# 前回の処理から1000秒以上経過している時だけ
	return false if curr.to_i < @last_periodic_process + 1000

	if curr.mon == 12 && curr.mday == 25 &&
		curr.hour == 0 && (curr.min >= 00 && curr.min <= 30)
	    seq = [
		   [:flan_subc, "今日はクリスマス〜 ♪恋人がさんたくｒ"],
		   [:patche_subc, "@@ そこまでよ！ JASRACに目をつけられたらやっかいだわ"],
		   [:remy_subc, "@@ 聖夜だというのに無粋なことね…"],
		   [:flan_subc, "@@ 二人とも何を言っているの…？ なんのことだかさっぱりだよ…"],
		   [:remy_subc, "@@ えーと… それはそうと、ずいぶんごきげんだったわね"],
		   [:flan_subc, "@@ だって、クリスマスにはサンタさんが…"],
		   [:remy_subc, "@@ 来ないわよ。少なくとも悪魔の館にはね"],
		   [:flan_subc, "@@ それは知ってるよ… でも恋人がサンタクロースなんでしょ？"],
		   [:remy_subc, "@@ 恋人だなんてフランには早すぎるわ"],
		   [:patche_subc, "@@ 過保護姉め… シスコンのきわみね"],
		   [:remy_subc, "@@ うっさいよパチェ。で、フラン、参考までに欲しいプレゼントは何かしら"],
		   [:flan_subc, "@@ 恋人！＞＜"],
		   [:remy_subc, "@@ 却下"],
		   [:sakuya_subc, "@@ お、お嬢様… フランドールお嬢様もお年頃なんですし…"],
		   [:patche_subc, "@@ 吸血鬼のお年頃っていったいいくつくらいなのかしら…"],
		   [:flan_subc, "@@ そ、そうだよ！フランにだって好きな人くらい…いるもん///"],
		   [:remy_subc, "@@ まさかいるの！？ ちょっとしばいてやるからここに連れて来なさいよ！"],
		   [:flan_subc, "@@ そ、そんなことは…；；"],
		   [:patche_subc, "@@ そうよね、できるわけないわよね… レミィはおばかさんね"],
		   [:remy_subc, "@@ なっ…"],
		   [:sakuya_subc, "@@ フランドールお嬢様のお気持ちを踏みにじるなんて、悪魔のようですわ"],
		   [:patche_subc, "@@ いや、悪魔なんだけどね"],
		   [:flan_subc, "…"],
		   [:flan_subc, "……"],
		   [:flan_subc, "………(ﾁﾗｯ"],
		   [:remy_subc, "@@ あーもーわかったわよ！ 私が悪かったわよ！で、何すりゃいいのよ"],
		   [:patche_subc, "@@ おいしくいただかれる？"],
		   [:remy_subc, "@@ それは夜雀あたりにまかせるわ…"],
		   [:flan_subc, "@@ 私は…お姉様がいればいいよ…"],
		   [:remy_subc, "@@ (///ω///)"],
		   [:flan_subc, "@@ (///ω///)"],
		   [:patche_subc, "あらあら、私達はおじゃま虫といったところね"],
		   [:sakuya_subc, "@@ そうですね、パチュリー様… こんなに甘々じゃケーキもいらないですわ"],
		   [:patche_subc, "@@ さっさと退散しましょう… レミィ、フラン、メリークリスマス。よい夜を…"],
		   [:remy_subc, "悪魔が言っていいのか分からないけど… メリークリスマス"],
		   [:flan_subc, "お姉様、フォロワーのみんな、メリークリスマス♪ クリスマスを爆発させるのはもうちょっとだけお休みさせてもらうねっ"],
		   [:flan_subc, "さて、プレゼントのお姉様でどうやって遊ぼうかなあ… じゅるり"],
		   [:remy_subc, "@@ ちょっ、いつ私がプレゼントになったの？ ていうか擬音がおかしいって… あーれー(ry"],
		  ]
	    register_chatseq(seq, SUBCLUSTER)
	end
    end
end
