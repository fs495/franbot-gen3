# -*- coding: utf-8 -*-

module Commondef
    # 通信系
    HOSTNAME = 'localhost'

    # ディレクトリ
    CONFIG_DIR = 'etc'
    VAR_DIR = 'var'

    IN_ACTIVE = 'in_active'
    IN_DONE = 'in_done'
    OUT_ACTIVE = 'out_active'
    OUT_DONE = 'out_done'
    OUT_FAILED = 'out_failed'
    UCONF_DIR = 'uconf'

    # 設定ファイルなどの名前
    CLUSTER_CONFIG_FILE = "cluster_config.yaml"
    BOT_CONFIG_FILE = "bot_config_%s.yaml"
    BOT_STATE_FILE = "bot_state_%s.yaml"
    FRIENDSHIP_STORE_FILE = "friendship.yaml"
    GLOBAL_INPUT_LOCK = "input_lock.yaml"

    # ログ
    LOG_FILE = "twibot.log"
    LOG_DATETIME_FORMAT = "%H:%M:%S"
    LOG_ROTATE_HOUR = 6
end
