# -*- coding: utf-8 -*-

class TohoResgen
    def ourselves(status, uconf)
	c = create_context(status, uconf)
	case c.text
	when /禁弾「過去を刻む時計」\((\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)\)/
	    # 自分自身の発言にしか反応しない
	    return if c.sn != botname

	    t0 = Time.local($1.to_i, $2.to_i, $3.to_i,
			    $4.to_i, $5.to_i, $6.to_i).to_i
	    s0 = "#{$4}:#{$5}:#{$6}"

	    o1 = parse_date(c.status.created_at)
	    t1 = o1.to_i
	    s1 = o1.strftime("%H:%M:%S")

	    t2 = @curr_time
	    s2 = @curr_obj.strftime("%H:%M:%S")

	    res = "ポストが#{s0}、反映が#{s1}、取得が#{s2}だったから、"
	    res += "反映に#{t1-t0}秒、取得に#{t2-t1}秒かかってるかな。"

	    if t1 < t0 - 5 || t2 < t1 - 5
		res += "時計が狂ってるみたいね。"
	    else
		n =  (t1 > t0 + 20) ? 6 : (t1 > t0 + 5) ? 3 : 0;
		n += (t2 > t1 + 120) ? 2 : (t2 > t1 + 60) ? 1 : 0;

		res += [
			'ポストも取得も大丈夫',
			'ポストは大丈夫だけど、取得がちょっと遅れてる',
			'ポストは大丈夫だけど、取得がすごい遅れてる',

			'ポストはちょっと遅れてるけど、取得は大丈夫',
			'ポストも取得もちょっと遅れてる',
			'ポストはちょっと遅れてて、取得はすごい遅れてる',

			'ポストはすごく遅れてるけど、取得は大丈夫',
			'ポストはすごく遅れてて、取得もちょっと遅れてる',
			'ポストも取得もすごく遅れてる',
		       ][n] + 'みたい。'
	    end
	    reply(res)# TODO

	when /^\d+月\d+日\d+時\d+分から\d+分間ポスト規制されてたみたい…/x
	    # 他の分身がポスト規制から復帰した場合
	    return if c.sn == botname

	    res = "#{c.to} " + rnd("おかえり〜♪",
				    "おつかれさまっ♪",
				    "まってたよ〜♪")
	    reply(res, c)
	end
    end
end
