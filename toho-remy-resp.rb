# -*- coding: utf-8 -*-

class TohoResgen

    REMY_MONOLOGUE = [
		      # 紅魔郷(対霊夢)
		      "やっぱり、人間って使えないわね",
		      "しょうがないわね",
		      "今、お腹いっぱいだけど・・・",
		      "咲夜は優秀な掃除係・・・ おかげで、首一つ落ちていないわ",
		      "楽しい夜になりそうね",
		      # 紅魔郷(対魔理沙)
		      "能ある鷹は尻尾隠さず...よ",
		      "人間だけよ。脳なんて単純で化学的な思考中枢が必要なのは",
		      "そうよ、病弱っ娘なのよ",
		      "あなたは今まで食べてきたパンの枚数を覚えているの？",
		      "で、何しに来たの？",
		      "もう、私お腹いっぱいだけど・・",
		      "・・・食べてもいいのよ",
		      "人間って楽しいわね",
		      #"暑い夜になりそうね", # 季節調整
		      # 永夜抄
		      "良いのよ、私は人間以外には興味が無いから。",
		      "悪いけど急いでるんだ。さっさとやられてくれないか？",
		      "私が本当の闇夜の恐怖を教えてあげる！",
		      "家の知識人は本ばっかり読んでて、あんまり役に立っていない気が……",
		      "もう、私は夜の王なんだからその位許してよぉ。",
		      "なめられたお返しをしないと、幻想郷での威厳が保てないじゃないの ＞＜",
		      "ほれ私を敬いなさい、咲夜。存分に。",
		      "今の紅茶を飲む毎日の方が楽しいんだよ。それが何が悪い。",
		      "妖怪なんかと一緒にするな。私は誇り高き……。",

		      # 二次創作
		      #"DHMO? ああ、確かに危険だわね…",
		      #"昔は幻想郷の妖怪をほぼ屈服させるほどだったんだが、膝に矢を受けてしまってな…", # Skyrim
		      #"日傘にシュバルツゼクスプロトタイプマークIIIという名前をつけようかしら…", # 中二病でも恋がしたい!
		      #"爆ぜろリアル！弾けろシナプス！ヴァリュシュメント ディス ワールド！", # 中二病でも恋がしたい!
		      #"スペカ名が中二病くさいとか大きなお世話よ… http://www.nicovideo.jp/watch/sm19136977", # 中二病でも恋がしたい!
		      "吹 っ 切 れ た http://www.nicovideo.jp/watch/sm19136977 #sm19136977", # 中二病でも恋がしたい!

		      # グッズの宣伝(商業)
		      "もうちょっとカリスマを出せないものかしら… まあかわいいからいいけどさあ" +
		      " http://www.gift-gift.jp/nui/nui005.html",
		      #"あら、なかなかかわいらしくできているじゃない？" +
		      #" http://www.gift-gift.jp/nui/nui105.html",
		      "う、なんか一部みっともないのが混じっているわね///" +
		      " http://www.liquidstone.jp/mameshiki/mameshiki002.html",
		     ]

    REMY_MONOLOGUE_FULLMOON = [
			       # 紅魔郷(対霊夢)
			       "こんなに月も紅いから本気で殺すわよ",
			       # 紅魔郷(対魔理沙)
			       "ふふふ、こんなに月も紅いから？",
			       "紅い悪魔の恐怖、その身をもって知るがよい！",
			      ]

    REMY_MONOLOGUE_NEWMOON = [
			      # 二次創作
			      "うっうー☆",
			      "こんなに月も紅いから… って今日は月は出てないわね",
			      "(」・ω・)」うー！(」・ω・)」うー！",
			      "フラン！フラン！フラン！フランぅぅうううわぁあああああああああああああああああん！！！あぁああああ…ああ…あっあっー！あぁああああああ！！！フランフランフランぅううぁわぁああああ！！！あぁクンカクンカ！クンカクンカ！スーハースーハー！スーハースーハー！いい匂いだなぁ…くんくん",
			     ]

    REMY_HENTAI_BATOU = ["咲夜〜 こいつつまみ出して！",
			 "変態ね…",
			 "どうしようもない変態ね…",
			 "救いようもない変態ね…",
			 "下劣な人間ね…",
			 "なんだこいつ… フランのおもちゃにでもしようかしら",
			 "なんだこいつ… パチェの実験材料にでもしてやろうかしら",
			]

    #----------------------------------------------------------------------
    # ひとり言発生処理
    # 600秒に1回(144回/日)、ひとり言を話すかのチェックを行なう。
    def generate_remy_monologue()
	# 一定時間経過していない場合はそのままリターン
	curr = Time.now
	return if @last_monologue[:remy_subc] != nil &&
	    curr.to_i < @last_monologue[:remy_subc] + 550 + rand(100)
	@last_monologue[:remy_subc] = curr.to_i

	# 確率調整
	return if rand(100) >= RANDOM_TALK_FRAC

	case rand(100)
	when 0 ... 50
	    # 月相によってひとり言候補を調整
	    case @moon_shape
	    when :new_moon
		cand = REMY_MONOLOGUE + REMY_MONOLOGUE_NEWMOON * 2
	    when :full_moon
		cand = REMY_MONOLOGUE + REMY_MONOLOGUE_FULLMOON * 2
	    else
		cand = REMY_MONOLOGUE
	    end

	    res = rnd(cand)
	    post(res)
	when 50 ... 80
	    pixiv_random_introduction(:remilia, '東方 レミリア')

	when 80 ... 81
	    bosyu = rnd(["非想天則募集", "天則対戦募集", "【非想天則】対戦募集"])
	    ip = "127.#{rand(255)}.#{rand(255)}.#{rand(255)}:10800"
	    char = rnd(["おぜう様", "レミリア", "れみりゃ使い"])
	    count = rnd(["相互切断自由", "3戦", "5戦"])
	    msg = rnd(["かかってきなさい", "相手してあげるわよ"])
	    post("#{bosyu} #{ip} #{char} #{count} #{msg}")
	end
    end

    #----------------------------------------------------------------------
    def generate_remy_resp(c)
	return false if c.only_to_others

	case c.text
	when /うっうー|うー\s*うー|れみりあ(☆|★)?うー/
	    if c.charisma > +20
		r = rnd(["…", "…定命の者よ、", "…人間、"])
		r += rnd(["私をバカにするなら容赦しない",
			  "私をバカにしているのか？"])
	    elsif c.charisma < -20
		r = rnd(["うっうー★", "うっうー♪", "れみりあ☆うー"])
	    else
		r = rnd(["そのうーうー言うのをやめなさい！",
			 "そのうーうー言うのをやめなさい！",
			 "な、なによ…",
			 "な、なんのことかしら//",
			 "なっ、なんのことかしらっ！"])
	    end
	    reply("#{c.to} #{r}", c)
	    c.uconf[:base_charisma] -= dice(2, 6)
	    return true
	end

	#----------------------------------------------------------------------
	return false if !(c.to_us || c.only_to_us)

	if c.text =~ /^\s*$/
	    if c.affection > 60
		if c.charisma < -20
		    reply("#{c.to} な、なに…？", c) # 空リプ
		elsif c.charisma < +20
		    reply("#{c.to}", c) # 空リプ
		end
	    end
	    c.uconf[:base_charisma] -= dice(2, 4)
	    return true
	end

	case c.text
	when /ごめん|ゴメン|すいません|許してくだ/
	    if c.charisma > +20
		reply("#{c.to} …ふん", c)
		return true
	    end

	when /ありがと/
	    if c.charisma > -20
		r = rnd(["どういたしまして、#{c.cn}",
			 "紅魔館の主ともなれば、これくらいの度量は見せないとね",
			 "それくらい構わないわよ？"])
	    else
		r = "うーうー///"
	    end
	    reply("#{c.to} #{r}", c)
	    return true

	when /(カリスマ|ｶﾘｽﾏ)(ブレイク|ブレーク|ﾌﾞﾚｲｸ|ﾌﾞﾚｰｸ)/, /れみりゃ/
	    c.uconf[:base_charisma] -= dice(2, 6)
	    return true

	when /カリスマ|ｶﾘｽﾏ/
	    if c.charisma > +20
		r = "ふっ…"
	    elsif c.charisma < -20
		r = "うーうーうー☆"
	    else
		r = "まあ、私がカリスマなのは当然ね！"
	    end
	    reply("#{c.to} #{r}", c)
	    c.uconf[:base_charisma] += dice(2, 6)
	    return true

	when /ぴちゅーん|ﾋﾟﾁｭｰﾝ/
	    if c.charisma > -20
		r = rnd(["雑魚ね…",
			 "霊夢くらいじゃないと歯ごたえないわね…"])
	    else
		r = "うーうー♪"
	    end
	    reply("#{c.to} #{r}", c)
	    c.uconf[:base_charisma] += dice(1, 6)
	    return true

	end

	#----------------------------------------------------------------------
	return false if !c.only_to_us

	case c.text
	when /(えっ?ち|エッチ|H|セック(ス|ル)|ｾｯｸ(ｽ|ﾙ)|[Ss][Ee][Xx]|(にゃん|ニャン){2}|生殖.*)しよ/,
	    /(もみ|揉み){2}|(ぬっちょ|ぬっぽ|じゅっぽ|じゅぽ){2}/,
	    /(ぺろぺろ|ペロペロ|ﾍﾟﾛﾍﾟﾛ|れろれろ|レロレロ|ﾚﾛﾚﾛ)/,
	    /#{REMY_RE}.*(おいしい|はむはむ|もぐもぐ|モグモグ|ﾓｸﾞﾓｸﾞ|mogmog)/o,
	    /#{REMY_RE}.*(ごくごく|ゴクゴク|ｺﾞｸｺﾞｸ|gokgok)/o,
	    /(さわ|触)(って(いい|良い).*(\?|？)|らせて)/,
	    /(おしり|お尻|おっぱい|ちっぱい|胸|貧乳|ひんぬー).*/,
	    /(ちゅっちゅ|チュッチュ|ﾁｭｯﾁｭ)|(ちゅー*っ?|チュー*ッ?|ﾁｭｰ*ｯ?)(しよう|して)?#{EOL}$/o,
	    /(パンツ|ぱんつ|ドロワーズ|ドロワース|ブラ|下着)/,
	    /(足|脚|靴|ニーソ|ソックス)(で|こき|コキ|ｺｷ)(して|シて)/,
	    /(は|履|穿)いてない/,
	    /(膝|ひざ)(枕|まくら)して/,
	    /くんかくんか|クンカクンカ|ｸﾝｶｸﾝｶ/,
	    # ↑セクハラ ↓イチャイヤ
	    /さわさわ|すりすり|もふもふ|ふにふに|ぷにぷに|つんつん|(ぎゅー?っ?|ギュー?ッ?|ｷﾞｭｰ?ｯ?)#{EOL}$|(なでなで|ナデナデ)|(いちゃいちゃ|イチャイチャ|ｲﾁｬｲﾁｬ)|(こちょこちょ|コチョコチョ|ｺﾁｮｺﾁｮ)/o

	    r = c.charisma + rand(30) - 15
	    if r < -20
		# れみりゃ対応
		r = rnd(["へ、へんたい＞＜",
			 "さくやーたすけてよぉ；；",
			 "や、やめてよぉ…；；",
			 "や、やぁん＞＜",
			 "うー…＞＜",
			 "うーうー" + tere_aa()])
	    elsif r > +20
		# カリスマおぜう対応
		r = rnd(REMY_SPELLS)
	    else
		# 一般の対応
		r = rnd(REMY_HENTAI_BATOU)
	    end
	    reply("#{c.to} #{r}", c)
	    return true

	when /(一緒|いっしょ)に(寝|ねよう|眠|ねむ|スリーピング)/
	    # 50%の確率で無視
	    return :ignored if rand(2) == 0

	    if c.charisma > +20
		r = rnd(["そんなに永遠の眠りにつきたいの？",
			 "永久の眠りにつきたいようね",
			 "あんただけ寝るのよ、永遠にね"])
	    elsif c.charisma < -40
		r = rnd(["うっう〜", "う〜う〜"]) + tere_aa()
	    else
		r = rnd(REMY_HENTAI_BATOU)
	    end
	    reply("#{c.to} #{r}", c)
	    return true

	when /性的な意味で.*(食|喰|たべ)/
	    # 50%の確率で無視
	    return :ignored if rand(2) == 0

	    if c.charisma > +20
		r = rnd(["穢らわしい血はいらないよ",
			 "人間は想像以上に下劣ね"])
	    elsif c.charisma < -40
		r = tere_aa()
	    else
		r = rnd(REMY_HENTAI_BATOU)
	    end
	    reply("#{c.to} #{r}", c)
	    return true

	when /(踏んで|ふんで)(下さい|ください)/
	    if c.charisma > +20
		r = rnd(["靴が汚れるわ",
			 "いやよ。靴が汚れるわ",
			 "人間ごときになんでそんなことしないといけないのよ"])
	    elsif c.charisma < -20
		r = tere_aa()
	    else
		r = rnd(REMY_HENTAI_BATOU)
	    end
	    reply("#{c.to} #{r}", c)
	    return true

	when /占い(して|お願い)|占って|運勢を?(教|おし)えて/
	    if c.charisma > +40
		r = "そのうち死んじゃいそうね。いつかはわからないけど"
	    elsif c.charisma < -40
		r = "恋愛運がいいみたい。だれかお相手はいるのかしら…？"
	    else
		r = rnd(["大吉", "中吉", "小吉", "吉", "末吉", "凶", "大凶",
			 "マジキチ", "ぴょん吉"])
		r += "ってとこね。本当はいろいろあるけど、めんどうくさいからそういう運命に変えておくわ"
	    end
	    reply("#{c.to} #{r}", c)
	    return true

	    #------------------------------------------------------------
	when /(かわいい|カワイイ|ｶﾜ.*ｲｲ|可愛い|きれい|綺麗)(ね|よ|お|なあ|ぜ|ZE)?/, /(かわゆす|ｶﾜﾕｽ)(なあ)?/
	    return :ignored if !c.only_to_us

	    if c.charisma > -20
		r = rnd(["ふふん、まあ当然ね！",
			 "このあふれでるカリスマにひれ伏しなさい",
			 "人間に言われてもそれほど嬉しくないわね"])
	    else
		r = rnd(["うーうー", "うー", "うっう〜"]) + tere_aa()
	    end
	    reply("#{c.to} #{r}", c)
	    return true

	when /(好|す)き(です|だ|だよ|だお|だぜ|だZE|っ)/,
	    /((愛|あい)してる|大切にする)(ね|よ|お|ぜ|ZE)?/,
	    /((つ|付)き(あ|合)って|交際して|彼女になって|彼氏にして)/,
	    /(結婚して|結婚しよう|お嫁に|お婿に)/
	    return :ignored if !c.only_to_us

	    if c.charisma < -40
		r = rnd(["うーうー", "うー", "うっう〜"]) + tere_aa()
	    else
		r = rnd(["は？",
			 "どういう意味か分からないんだけど",
			 "自分が何を言っているのか理解しているの？",
			 "身の程知らずの人間もいたものね…"])
	    end
	    reply("#{c.to} #{r}", c)
	    return true

	end

	return false
    end
end
