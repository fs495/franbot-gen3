# -*- coding: utf-8 -*-

class TohoResgen

    def is_admin(c)
	return c.sn == 'fs495' || c.sn == 'ccs_sf_net' ### TODO
    end

    def bot_admin(c)
	return false if !(c.to_us || c.only_to_us)
	return false if !is_admin(c)

	case c.text
	    # bot主から伝言
	when /^%msg:?\s*(.*)/m
	    res = {
		:flan_subc => "お世話係から伝言だよ♪",
		:remy_subc => "bot主から伝言よ",
		:patche_subc => "bot主から伝言よ",
		:sakuya_subc => "bot主から伝言です",
	    }[c.subc]
	    post("#{res}「#{$1}」")
	    return true

	    # botとして発言
	when /^%fwd:?\s*(.*)/m
	    post("#{$1}")
	    return true

	    # フォロー・ブロック、ふぁぼ・削除
	when /^%(follow|unfollow|block|unblock|fav|unfav|rt|delete)\s+(.*)/
	    cmd = $1
	    targets = $2.split(/\s*(?:\s|,|、)+\s*/)
	    @bot_list.each do |bot|
		next if !c.decoded[:r_to].include?(bot.to_s)
		targets.each do |target|
		    index = @bot_to_index[bot]
		    sendmsg("#{cmd} #{target}", "inout#{index}")
		end
	    end
	    return true

	    # マシンの停止と自動
	    # e.g. "reboot 14:00 300" 14時にシャットダウン開始し、5分後に再起動
	when /%reboot\s*([0-9:]+)\s*([0-9]+)/
	    if c.nm == @admin
		start, duration = $1, $2
		reply("#{c.to} scheduled cycle down at #{start}, wake up after #{duration} seconds", c)
		system("echo /usr/bin/sudo /usr/sbin/rtcwake -l -m no -s #{duration} | at #{start}")
		system("echo /usr/bin/sudo /sbin/poweroff | at #{start}")
		return true
	    end

	    # クラスタ内の規制状態を報告
	when /%banstate/
	    res = ''
	    SUBCLUSTER.each do |subc, bots|
		bots.each do |bot|
		    started = @ban_started_at[bot.to_sym]
		    if started == nil
			res += "#{bot}=OK "
		    else
			time = Time.at(started).strftime("%H:%M")
			res += "#{bot}=#{time} "
		    end
		end
	    end
	    reply("#{c.to} #{res}", c)
	    return true

	when /%rest_birthday_check/
	    @last_birthday_check = Time.at(0)
	    return true

	end
	return false
    end

    #----------------------------------------------------------------------
    # 呼称変更
    def yobina_henkou(c)
	return false if !(c.to_us || c.only_to_us)
	return false if c.decoded[:type] != :reply

	return false if c.text !~ /([^@]+)(と|って)(呼|よ)んで/
	return false if $1.length == 0
	newname = $1

	case c.subc
	when :flan_subc
	    if c.affection > 30
		if c.tsundere < 0
		    reply_targeted("#{c.to} しょうがないなあ… #{newname}って呼べばいいの？", c)
		else
		    reply_targeted("#{c.to} #{newname}、って呼べばいいんだよね？", c)
		end
	    else
		reply_targeted("#{c.to} ごきげんよう、#{c.cn}さん（#{newname}は無理があるわ）", c)
		newname = c.uconf[:nickname]
	    end

	when :remy_subc
	    reply_targeted("#{c.to} 面倒くさいわね… #{newname}と呼べばいいのね？", c)
	when :patche_subc
	    reply_targeted("#{c.to} あなたのことは#{newname}と呼べばいいのね。分かったわ", c)
	when :sakuya_subc
	    reply_targeted("#{c.to} #{newname}とお呼びすればいいのですね", c)
	end
	c.uconf[:nickname] = newname
	return true
    end

    #----------------------------------------------------------------------
    # ステータスの設定・問い合わせなど。
    # 今のところフランサブクラスタしか対応しない

    def status_admin(c)
	case c.subc
	when :flan_subc
	    return status_admin_flan(c)
	when :remy_subc
	    return status_admin_remy(c)
	end
	return false
    end

    def status_admin_flan(c)
	return false if !(c.to_us || c.only_to_us)

	case c.text
	    # ユーザステータス設定
	when /ステータス変更\s*([-+]?\d+)\s*(,|，|、|\s)\s*([-+]?\d+)/x
	    c.uconf[:base_affection] = $1.to_i
	    c.uconf[:base_tsundere] = $3.to_i
	    reply_targeted("#{c.to} 基本好感度を#{c.uconf[:base_affection]}、基本ツンデレ度を#{c.uconf[:base_tsundere]}に変更したよー", c)
	    return true

	    # ユーザステータス報告
	when /#{FIRST_PERSON}の(事|こと).*どう(思|おも).*(\?|\？)/ox
	    if c.affection > 90
		if c.tsundere > +10
		    resp = "#{c.cn}のこと大好きだよ" + tere_aa()
		    ack = "だよね♪"
		elsif c.tsundere < -10
		    resp = "………べっ、別に#{c.cn}のことなんかどうとも思ってないんだからねっ" + tere_aa()
		    ack = "だった気がするけど、たまたま覚えてただけだからね！"
		else
		    resp = rnd(["#{c.cn}は大事なお友達だよ♪",
				"ﾌﾗﾝと#{c.cn}ゎ……ズッ友だょ！！"])
		    ack = "だよね？"
		end
	    elsif c.affection > 60
		if c.tsundere < -10
		    resp = "#{c.cn}は食料として優秀だよね♪"
		    ack = "だっけ？"
		else
		    resp = "#{c.cn}は大事なお友達だよ♪"
		    ack = "だよね？"
		end
	    elsif c.affection > 30
		resp = "え、#{c.cn}はお友達だよね？"
		ack = "だったような…"
	    else
		resp = "#{c.cn}とはなかよしになったばかりだよね"
		ack = "だったような…"
	    end
	    resp += "[好感度#{c.affection}(基本#{c.uconf[:base_affection]}), "
	    resp += "ツンデレ度#{c.tsundere}(基本#{c.uconf[:base_tsundere]})]。"

	    # 誕生日
	    if c.cconf[:birth_month] && c.cconf[:birth_mday]
		resp += "お誕生日は"
		resp += "#{c.cconf[:birth_year]}年" if c.cconf[:birth_year]
		resp += "#{c.cconf[:birth_month]}月#{c.cconf[:birth_mday]}日"
		resp += ack
	    end

	    reply_targeted("#{c.to} #{resp}", c)
	    return true

	    # 遅延状況を調べる
	when /遅延(調べて|しらべて|教えて|おしえて)/x
	    if is_admin(c) || c.affection > 90
		s = Time.now.strftime("%Y-%m-%d %H:%M:%S")
		reply("禁弾「過去を刻む時計」(#{s})", c)
		return true
	    end
	end
	return false
    end

    def status_admin_remy(c)
	return false if !(c.to_us || c.only_to_us)

	case c.text
	when /ステータス変更\s*([-+]?\d+)\s*(,|，|、|\s)\s*([-+]?\d+)/x
	    c.uconf[:base_affection] = $1.to_i
	    c.uconf[:base_charisma] = $3.to_i
	    reply_targeted("#{c.to} 基本好感度を#{c.uconf[:base_affection]}、基本カリスマ度を#{c.uconf[:base_charisma]}に変更しておいてあげたわよ", c)
	    return true

	    # ユーザステータス報告
	when /#{FIRST_PERSON}の(事|こと).*どう(思|おも).*(\?|\？)/ox
	    if c.affection < 60
		h = rnd(['血液タンクかしら… ',
			 'フランのおもちゃってとこね。',
			 '村人Dといったところかな。',
			 'ええと…どちらさん？'])
		t = 'かしら'
	    elsif c.charisma > -20
		h = rnd(['変わった人間ね。',
			 '物好きな人間ね。'])
		t = 'かしら'
	    elsif
		h = rnd(['ま、まあ人間にしてはおもしろいやつかな// ',
			 'うー…なんでもないってば/// '])
		t = 'ね'
	    end

	    reply_targeted("#{c.to} #{h}基本好感度は#{c.uconf[:base_affection]}、基本カリスマ度は#{c.uconf[:base_charisma]}といったところ#{t}", c)
	    return true
	end

	return false
    end
end
