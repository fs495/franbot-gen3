#!/usr/bin/env ruby
# -*- coding: euc-jp -*-
# 
# Usage:
# 	$0 -a
# 	$0 var/in_done/5??/
# 	$0 var/out_done/1304????
# 	$0 var/out_failed/1304????
#
# - 動作
#   - dir/* をまとめて dir_archive/dir.tar.bz2 に作成する
#   - textを抜き出す(in_doneモード時)
#   - fail_messageを抜き出す(out_failモード時)

SUMMARIZE_THRE = 86400 * 3

require 'yaml/store'

def summarize(src_dir)
    # ディレクトリ名から動作フラグを決める
    case src_dir
    when /in_done/
	ext_text, ext_fail = true, false
    when /out_done/
	ext_text, ext_fail = false, false
    when /out_failed/
	ext_text, ext_fail = false, true
    end

    # ターゲットのファイル名を決定する
    a = File::split(src_dir)
    parent, prefix = a[0], a[1]
    backup_base = "#{parent}_backup"
    arch_base = "#{parent}_archive"
    arch_name = File::join(arch_base, prefix)
    system('mkdir', '-p', arch_base, backup_base)

    # テキスト抽出処理
    if ext_text
	STDERR.puts "Extracting text from #{src_dir}/*..."
	fout = open("#{arch_name}.txt", "w")
	Dir::entries(src_dir).sort.each do |name|
	    next if name !~ /^\d+$/
	    STDERR.print " Reading #{name}...\r"

	    store = YAML::Store.new(File::join(src_dir, name))
	    store.transaction(true) do
		break if !store.root?(:resgen_hint) # 古い
		break if !store[:resgen_hint].has_key?(:handled) # 処理対象外

		handled = store[:resgen_hint][:handled]
		text = store[:status][:text]
		fout.puts "#{name}: #{handled}: #{text}"
	    end
	end
	fout.close()
	system('bzip2', '--best', "#{arch_name}.txt")
    end

    # fail_message抽出処理
    if ext_fail
	STDERR.puts "Extracting failure message from #{src_dir}/*..."
	fout = open("#{arch_name}.txt", "w")
	Dir::entries(src_dir).sort.each do |name|
	    next if name !~ /^\d+$/
	    STDERR.print " Reading #{name}...\r"

	    store = YAML::Store.new(File::join(src_dir, name))
	    store.transaction(true) do
		break if !store.root?(:fail_message)
		fout.puts "#{src_dir}/#{name}::fail_message: #{store[:fail_message]}"
	    end
	end
	fout.close()
	system('bzip2', '--best', "#{arch_name}.txt")
    end

    # tar.bz2
    STDERR.puts "Creating #{arch_name}.tar.bz2..."
    system('tar', 'cf', "#{arch_name}.tar", '-C', parent, prefix)
    system('rm', '-f', "#{arch_name}.tar.bz2")
    system('bzip2', '--best', "#{arch_name}.tar")

    # dir_workへの移動
    dir = File::join(backup_base, prefix)
    raise "#{dir} already exists" if File::directory?(dir)
    system('mv', '-vi', src_dir, backup_base)
end

def summarize_all_old(base_dir)
    curr = Time.now
    Dir.entries(base_dir).each do |dir|
	src_dir = File::join(base_dir, dir)
	# 条件に合わないものをスキップ
	# カレントディレクトリ・ペアレントディレクトリではないディレクトリで
	# あって、修正してからSUMMERIZE_THRE以上経過したもののみ処理する
	next if dir == '.' || dir == '..'
	next if !File::directory?(src_dir)
	next if File::mtime(src_dir) + SUMMARIZE_THRE > curr
	#p [dir, curr, File::mtime(src_dir)]

	summarize(src_dir)
    end
end

### メインループ

ARGV.each do |src_dir|
    if src_dir == '-a'
	['var/in_done', 'var/out_done', 'var/out_failed'].each do |base_dir|
	    summarize_all_old(base_dir)
	end
    else
	summarize(src_dir)
    end
end
