#!/usr/bin/ruby
# -*- coding: utf-8 -*-

require 'yaml/store'

class InterCluster
    CLUESTERS = ['twibot1', 'twibot2']

    def initialize()
	@yamls = {}
	CLUESTERS.each do |cl|
	    path = File::join('..', cl, 'var', 'user_info.yaml')
	    @yamls[cl] = YAML::Store.new(path)
	end
    end

    def report_ff()
	friends_of, followers_of = {}, {}
	uniq_friends, uniq_followers = [], []
	total_friends, total_followers = 0, 0

	@yamls.each do |cl, yaml|
	    yaml.transaction(true) do
		yaml[:bots].each do |bot|
		    friends = yaml[:friends][bot]
		    followers = yaml[:followers][bot]

		    friends_of[bot] = friends
		    followers_of[bot] = followers
		    uniq_friends |= friends
		    uniq_followers |= followers
		    total_friends += friends.size
		    total_followers += followers.size
		    printf "%18s %6d %6d\n", bot, friends.size, followers.size
		end
	    end
	end

	printf "%18s %6d %6d\n", 'Total', total_friends, total_followers
	printf "%18s %6d %6d\n", 'Uniq.', uniq_friends.size, uniq_followers.size
    end
end

ic = InterCluster.new()
ic.report_ff()
