# -*- coding: utf-8 -*-

class TohoResgen

    SAKUYA_MONOLOGUE = [
			# 紅魔郷
			"あー、お掃除が進まない！",
			"お嬢様は暗いところが好きなのよ",
			"あんたじゃ掃除も出来そうに無いわね",
			# 妖々夢
			"なんか、無駄に時間を過ごしてるような気がする．．．",
			"うちのお嬢様は大丈夫かしら？",
			"ああ、心配だわ。自分。",
			"あなたは悩みが少なそうでいいわね",
			"で、こんなところで時間を潰してる暇はないんだけど",
			"死人に口無し",
			"見た感じで物を言うな",
			# 永夜抄
			"お嬢様！少々お戯れが過ぎますよ。",
			"お嬢様がそうおっしゃるのなら……。",
			"私のナイフから逃げれると思って？",
			"私は掃除が得意なのよ。",
			"お嬢様は夜型、ですものねぇ。",
			"年長者は敬わないといけませんよ。",
			"あいにく、私は人間よ。",

			# 二次創作
			#"スカーレッツ！スカーレッツ！",
			#"忠誠心っていったい何のことかしら？",
			#"美鈴がうらやましいわ…",
			#"時間を止める魔法少女ですって？ 私だってまじかるさくやちゃんに… いえ、なんでもありませんわ",
			"お嬢様方のお召し物をし○むらで買って済ませるなんてできないわ…",
			#"白玉楼の庭師、自機化して調子にのってるわね… 同じ従者ポジションで獲物を使うところもキャラ被ってて忌々しい…",
		       ]

    #----------------------------------------------------------------------
    # ひとり言発生処理
    # 600秒に1回(144回/日)、ひとり言を話すかのチェックを行なう。
    def generate_sakuya_monologue()
	# 一定時間経過していない場合はそのままリターン
	curr = Time.now
	return if @last_monologue[:sakuya_subc] != nil &&
	    curr.to_i < @last_monologue[:sakuya_subc] + 550 + rand(100)
	@last_monologue[:sakuya_subc] = curr.to_i

	# 確率調整
	return if rand(100) >= RANDOM_TALK_FRAC

	case rand(100)
	when 0 ... 5
	    res = rnd(SAKUYA_MONOLOGUE)
	    post(res)
	end
    end

    #----------------------------------------------------------------------
    PAD_RE = 'PAD|Pad|pad|ＰＡＤ|Ｐａｄ|ｐａｄ|パッド|ﾊﾟｯﾄﾞ'

    def generate_sakuya_resp(c)
	return false if c.only_to_others

	case c.text
	when /iPad/o
	    # 頻出単語につき、空中リプでは反応しないようにする
	when /#{PAD_RE}/o
	    if !(c.to_us || c.only_to_us) && rand(4) == 0
		# 空中リプのときだけ25%の確率で反応
		# 「ゲームPad」「Pad長」なども対象
		res = rnd(['ﾋﾟｸｯ', 'ｷﾞｸｯ', 'ﾄﾞｷｯ'])
		reply("#{c.to} #{res}", c)
		return true
	    end
	end

	#------------------------------------------------------------
	return false if !(c.to_us || c.only_to_us)

	if c.text =~ /^\s*$/
	    reply("#{c.to}", c) # 空リプ
	    return true
	end

	case c.text
	when /なまちち/
	    reply("#{c.to} なまちちです！", c)
	    return true
	when /生乳/
	    reply("#{c.to} 確認ですが、それは「せいにゅう」と読むのですよね？つまり、未加工の牛乳という意味ですが…", c)
	    return true
	when /(違う|違います)/
	    reply("#{c.to} そ、そうですか… ええと、お嬢様に呼ばれてしまったのでこれにて…", c)
	    return true

	when /#{PAD_RE}|(偽|にせ)(乳|ちち|おっぱい)|ヌーブラ|(寄|よ)せて(上|あ)げる/o
	    spell = rnd(SAKUYA_SPELLS)
	    res = rnd(["な、何をおっしゃっているのかさっぱり…",
		       "な、何のことしょうか？",
		       "生乳ですっ！＞＜",
		       "ナイフの切れ味を知りたいようですね… #{spell}！",
		       "…#{spell}！",
		       "2時間前に出直してきな",
		      ])
	    reply("#{c.to} #{res}", c)
	    return true

	when /規制(状態)?教えて/
	    res = ''
	    [[:remy_subc, "お嬢様"],
	     [:patche_subc, "パチュリー様"],
	     [:flan_subc, "フランドールお嬢様"]].each do |data|
		subc, name = data[0], data[1]
		res += "#{name}は"
		SUBCLUSTER[subc].each do |bot|
		    started = @ban_started_at[bot]
		    if started == nil
			res += "○@#{bot}、"
		    else
			time = Time.at(started).strftime("%H時%M分")
			res += "☓(#{time})@#{bot}、"
		    end
		end
	    end
	    reply("#{c.to} #{res}ですわ", c)
	    return true

	    # when /遅延教えて/ TODO
	    # セクハラ TODO
	    #"2時間前に出直してきな",
	end

	#------------------------------------------------------------
	return false if !c.only_to_us

	case c.text
	when /#{SAKUYA_RE}.*#{TIYA_ALL}/o,
	    /#{TIYA_ALL}.*#{SAKUYA_RE}/o,
	    /#{TIYA_ALL}#{EOL}$/o
	    res = rnd(["あ、ありがとうございます//",
		       "て、照れますわ…//",
		       "じょ、冗談はおやめ下さい//",])
	    reply("#{c.to} #{res}", c)
	    return true
	end

	return false
    end
end
