# -*- coding: utf-8 -*-

# IDベースのYAMLファイルの操作
# YAMLファイルは「var/#{base}/#{middir}/#{id}」という形式で保存する。
# middirはidの先頭数文字からなり、dir_size()で返される文字より多い部分が
# あてられる。
#
# - 入力ID: 16桁 in_*/13/16桁
# - 出力ID: 12桁 out_*/8/12桁
# - ユーザID: 8〜9桁
class BaseStore
    include Commondef

    def self.path(base, id)
	if id.length <= dir_size()
	    middir = "."
	else
	    middir = id[0, id.length - dir_size()]
	end
	return File::join(VAR_DIR, base, middir, id)
    end

    def self.open_store(base, id)
	path = path(base, id)
	basedir = File::dirname(path)
	FileUtils::mkdir_p(basedir) if !File::directory?(basedir)
	return YAML::Store.new(path)
    end

    def self.load_hash(store)
	hash = {}
	store.transaction(true) do
	    store.roots().each do |key|
		hash[key] = store[key]
	    end
	end
	return hash
    end

    def self.move(oldbase, newbase, id)
	opath, npath = path(oldbase, id), path(newbase, id)
	odir, ndir = File::dirname(opath), File::dirname(npath)

	FileUtils::mkdir_p(ndir) if !File::directory?(ndir)
	FileUtils::mv(opath, npath)
	begin
	    FileUtils::rmdir(odir) if Dir::entries(odir).size <= 2
	rescue Errno::ENOENT
	    # 複数プロセスで同一ディレクトリを削除しようとした場合はOK
	end
    end

    def self.remove(base, id)
	path = path(base, id)
	dir = File::dirname(path)

	FileUtils::rm_f(path)
	begin
	    FileUtils::rmdir(dir) if Dir::entries(dir).size <= 2
	rescue Errno::ENOENT
	    # 複数プロセスで同一ディレクトリを削除しようとした場合はOK
	end
    end

    def self.traverse(base)
	dir = File::join(VAR_DIR, base)
	return [] unless File::directory?(dir)

	list = []
	Dir::entries(dir).each do |middir|
	    next if middir == '.' || middir == '..'
	    Dir::entries(File::join(VAR_DIR, base, middir)).each do |entry|
		next if entry == '.' || entry == '..'
		list << entry if entry =~ /^\d+$/
	    end
	end
	return list.sort()
    end
end

class InputStore < BaseStore
    def self.dir_size()
	return 14
    end
end

class OutputStore < BaseStore
    def self.dir_size()
	return 4
    end
end

class UserStore < BaseStore
    def self.dir_size()
	return 99
    end
end

#----------------------------------------------------------------------

class VarStore
    include Commondef
    def self.open_store(path)
	return YAML::Store.new(File::join(VAR_DIR, path))
    end
end

class ConfigStore
    include Commondef
    def self.open_store(path)
	return YAML::Store.new(File::join(CONFIG_DIR, path))
    end
end


#----------------------------------------------------------------------

class Cluster
    include Commondef

    # クラスタ設定ファイルをロードする
    def self.load_config()
	cstore = YAML::Store.new(File::join(CONFIG_DIR, CLUSTER_CONFIG_FILE))
	cstore.transaction(true) do
	    return cstore[:config]
	end
    end

    # クラスタ設定ファイルの雛形を出力する
    def self.save_sample_config()
	path = File::join(CONFIG_DIR, CLUSTER_CONFIG_FILE + ".sample")
	cstore = YAML::Store.new(path)
	cstore.transaction do
	    config = Config.new()
	    config.bot_list = [:replace_me_1, :replace_me_2]
	    config.script = {:resgen => 'sample-resgen.rb'}
	    config.port_number = 49500
	    config.subcluster = {:subc1 => [:bot1, :bot2], :subc2 => [:bot3]}
	    config.ext[:key1] = :value1
	    config.ext[:key2] = :value2

	    cstore[:config] = config
	    cstore.commit()
	end
	puts "Saved sample cluster configuration file as #{path}"
	puts "Please rename it to #{CONFIG_DIR}/#{CLUSTER_CONFIG_FILE}"
    end

    class Config
	attr_accessor :bot_list
	attr_accessor :script
	attr_accessor :port_number
	attr_accessor :subcluster
	attr_accessor :ext
	def initialize()
	    @bot_list = []
	    @script = {}
	    @port_number = 0
	    @subcluster = {}
	    @ext = {}
	end
    end
end
